
<a href="#" class="danger p-0 playvideo-button" data-id="{{ $dt->id }}" data-iprogramme="{{ $dt->iprogramme }}" data-channel="{{ $dt->channel }}" data-iproduct="{{ $dt->iproduct }}" data-toggle="modal" data-target="#playvideo-modal" data-quality="HD">
        <img src="{{asset('images/icon_hd2.png')}}" alt="player-hd" width="20px" class="mr-2"  style="padding-top:-10px;margin-top:-10px">
    </a>

<a href="#" class="success p-0 playvideo-button" data-id="{{ $dt->id }}" data-iprogramme="{{ $dt->iprogramme }}" data-channel="{{ $dt->channel }}" data-iproduct="{{ $dt->iproduct }}" data-toggle="modal" data-target="#playvideo-modal" data-quality="SD">
        <i class="ft-play-circle font-medium-3 mr-2"></i>
    </a>