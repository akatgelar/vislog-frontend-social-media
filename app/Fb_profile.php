<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Fb_profile extends Eloquent
{
    protected $guarded = ['id'];
    protected $table = 'fb_profile';

}
