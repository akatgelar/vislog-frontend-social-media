<?php $__env->startSection('pagetitle'); ?>
    <title>Video Data</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
        <!-- BEGIN : Main Content-->
        <div class="main-content">
          <div class="content-wrapper"><!-- DOM - jQuery events table -->
<section id="browse-table">
  <div class="row">
    <div class="col-12">
      <div class="card">
      <?php if($errors->any()): ?>
              <p class="alert alert-danger">
              <?php echo ucfirst(implode('<br/>', $errors->all(':message'))); ?>

              </p>
      <?php endif; ?>
        <div class="card-header">
          <h4 class="card-title">Video Data</h4>
        </div>
        <div class="card-content">
          <div class="px-3">
          <?php if(isset($item)): ?>
              <?php echo e(Form::model($item, ['url' => 'admin/videodataupdate/'.$item->id, 'method' => 'patch'])); ?>

          <?php else: ?>
              <?php echo e(Form::open(['url' => 'admin/videodatastore'])); ?>

          <?php endif; ?>
              <div class="form-body">
                <div class="form-group row">
                  <label class="col-md-3 label-control" for="date">Date: </label>
                  <div class="col-md-9">
                  <?php echo e(Form::text('date', old('date',$item->date ?? null), array('class' => 'form-control datepicker-here','required','autocomplete'=>'off', 'data-language'=>'id'))); ?>

                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 label-control" for="channel">Channel: </label>
                  <div class="col-md-9">
                  <?php echo e(Form::select('channel', 
                    \App\Channel::pluck('channel','channel'), 
                    old('channel',$item->channel ?? null), 
                    array('class' => 'form-control'))); ?>

                  </div>
                </div>    
                <div class="form-group row">
                  <label class="col-md-3 label-control" for="count">No. of File: </label>
                  <div class="col-md-9">
                  <?php echo e(Form::number('count', old('count',$item->count ?? null), array('class' => 'form-control'))); ?>

                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 label-control" for="remarks">Remarks: </label>
                  <div class="col-md-9">
                  <?php echo e(Form::textarea('remarks', old('remarks',$item->remarks ?? null), array('class' => 'form-control','rows'=>4))); ?>

                  </div>
                </div>
              </div>
              <div class="form-actions">
                <a class="pull-right" href="<?php echo e(url('admin/videodata')); ?>"><button type="button" class="btn btn-raised btn-warning mr-1">
                  <i class="ft-x"></i> Cancel
                </button></a>
                <button type="submit" class="pull-left btn btn-raised btn-primary mr-3">
                  <i class="fa fa-check-square-o"></i> Save
                </button>         
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- File export table -->

          </div>
        </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('pagecss'); ?>
<link href="<?php echo e(asset('css')); ?>/datepicker.min.css" rel="stylesheet" type="text/css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('pagejs'); ?>
<script src="<?php echo e(asset('js')); ?>/datepicker.min.js"></script>
<script src="<?php echo e(asset('js')); ?>/i18n/datepicker.id.js"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\vislog\resources\views/admin/videodata/createupdate.blade.php ENDPATH**/ ?>