<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Notifications\Notifiable;
use App\Socialmedia;
use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;
use Session;
use Validator;
use Hash;
use \Carbon\Carbon;
use Auth;

class SocialmediaController extends Controller
{
    use Notifiable;

    public function __construct()
    {
        //
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.socialmedia.index');
    }

    public function indexjson()
    {
        return datatables(Socialmedia::where([['created_by', '=', Auth::id()]])->get())
        ->addColumn('action', function ($dt) {
            return view('admin.socialmedia.action',compact('dt'));
        })
        ->addColumn('last_updated', function ($socialmedia)
        {
            if($socialmedia->last_updated_profile) {
                return $socialmedia->last_updated_profile;
            }
            else if($socialmedia->last_updated_post) {
                return $socialmedia->last_updated_post;
            }
            else {
                return '';
            }
        })
        ->editColumn('is_active', function ($socialmedia)
        {
            if($socialmedia->is_active) {
                return 'Ya';
            } else {
                return 'Tidak';
            }
        })
        ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.socialmedia.createupdate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'category' => 'required|unique:category',
            'name' => 'required|unique:name',
            'username' => 'required|unique:username',
            'userid' => 'required|unique:userid',
        ]);

        $requestData = $request->all();
        if(isset($requestData['is_active'])) {
            $requestData['is_active'] = true;
        }else{
            $requestData['is_active'] = false;
        }
        $requestData['created_by'] = Auth::id();
        $requestData['updated_by'] = Auth::id();
        Socialmedia::create($requestData);
        Session::flash('message', 'Social media ditambahkan');
        Session::flash('alert-class', 'alert-success');
        return redirect('admin/socialmedia');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Socialmedia $socialmedia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($socialmediaid)
    {
        $item = Socialmedia::find($socialmediaid);
        return view('admin.socialmedia.createupdate',compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($socialmediaid, Request $request)
    {
        $socialmedia = Socialmedia::find($socialmediaid);

        $requestData = $request->all();
        if(isset($requestData['is_active'])) {
            $requestData['is_active'] = true;
        }else{
            $requestData['is_active'] = false;
        }
        $requestData['updated_by'] = Auth::id();
        // dd($requestData);
        Socialmedia::where('_id',$socialmediaid)->update($requestData);
        Session::flash('message', 'Social media diubah');
        Session::flash('alert-class', 'alert-success');
        return redirect('admin/socialmedia');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($socialmediaid)
    {
        Socialmedia::destroy($socialmediaid);
        Session::flash('message', 'Social media dihapus');
        Session::flash('alert-class', 'alert-success');
        return redirect('admin/socialmedia');
    }


}
