<?php $__env->startSection('pagetitle'); ?>
    <title>Target Audience</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
        <!-- BEGIN : Main Content-->
        <div class="main-content">
          <div class="content-wrapper"><!-- DOM - jQuery events table -->
<section id="browse-table">
  <div class="row">
    <div class="col-12">
      <?php if($errors->any()): ?>
      <p class="alert alert-danger">
        <?php echo ucfirst(implode('<br/>', $errors->all(':message'))); ?>

      </p>
      <?php endif; ?>
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Target Audience</h4>
        </div>
        <div class="card-content">
          <div class="px-3">
            <?php echo e(Form::model($item, ['url' => ['admin/targetaudience/update'], 'method' => 'patch'])); ?>

              <?php echo e(Form::hidden('id', $item->id)); ?>

              <div class="form-body">
                <div class="form-group row">
                  <label class="col-md-3 label-control" for="code"><?php echo app('translator')->get('Code'); ?>: </label>
                  <div class="col-md-9">
                  <?php echo e($item->code ?? null); ?>

                  </div>
                </div><div class="form-group row">
                  <label class="col-md-3 label-control" for="targetaudience"><?php echo app('translator')->get('Target Audience'); ?>: </label>
                  <div class="col-md-9">
                  <?php echo e(Form::text('targetaudience', old('targetaudience',$item->targetaudience ?? null), array('class' => 'form-control'))); ?>

                  </div>
                </div>
                <div class="form-actions">
                  <a class="pull-right" href="admin/targetaudience"><button type="button" class="btn btn-raised btn-warning mr-1">
                    <i class="ft-x"></i> Cancel
                  </button></a>
                  <button type="submit" class="pull-left btn btn-raised btn-primary mr-3">
                    <i class="fa fa-check-square-o"></i> Save
                  </button>     
                </div>
              </div>
            </form>                    
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- File export table -->

          </div>
        </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('pagecss'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('pagejs'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\vislog\resources\views/admin/targetaudience/createupdate.blade.php ENDPATH**/ ?>