<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Notifications\Notifiable;
use App\Yt_post;
use App\Yt_profile;
use App\Socialmedia;
use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;
use Session;
use Validator;
use Hash;
use \Carbon\Carbon;
use Auth;
use DateTime;
use DateInterval;
use DatePeriod;
use MongoDB\BSON\UTCDateTime as MongoDate;
use PhpParser\Node\Stmt\Foreach_;
use Illuminate\Support\Facades\DB;

class YoutubeController extends Controller
{
    use Notifiable;

    public function __construct()
    {
        //
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.youtube.index');
    }

    public function indexjson()
    {
        $data_id = Socialmedia::where([['is_active', '=', true],['category', '=', 'youtube'],['created_by', '=', Auth::id()]])->get()->toArray();
        $data_full = [];
        foreach($data_id as $du) {
            $last_update = Yt_profile::where([['id', '=', $du['userid']]])->orderBy('date', 'desc')->limit(1)->get()->toArray();
            if ($last_update) {
                $du['last_update'] = $last_update['0']['date'];
                $du['profile_pic'] = $last_update['0']['thumbnails_default'];
            } else {
                $du['last_update'] = '';
                $du['profile_pic'] = '';
            }
            array_push($data_full, $du);
        }
        return datatables($data_full)
        ->addColumn('profile_pic', function ($dt) {
            return view('admin.youtube.profilepic',compact('dt'));
        })
        ->addColumn('action', function ($dt) {
            return view('admin.youtube.action',compact('dt'));
        })
        ->toJson();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function dashboard($socialmediaid)
    {
        $first_profile = Yt_profile::where([['id', '=', $socialmediaid]])->orderBy('date', 'asc')->limit(1)->get()->toArray();
        $last_profile = Yt_profile::where([['id', '=', $socialmediaid]])->orderBy('date', 'desc')->limit(1)->get()->toArray();
        $first_profile_date = DateTime::createFromFormat('Y-m-d H:i:s', $first_profile[0]['date']);
        $last_profile_date = DateTime::createFromFormat('Y-m-d H:i:s', $last_profile[0]['date']);

        // $first_post = Yt_post::where([['channelId', '=', $socialmediaid]])->orderBy('publishedAt', 'asc')->limit(1)->get(['publishedAt'])->toArray();
        // $last_post = Yt_post::where([['channelId', '=', $socialmediaid]])->orderBy('publishedAt', 'desc')->limit(1)->get(['publishedAt'])->toArray();
        // $first_post_date = DateTime::createFromFormat('Y-m-d H:i:s', $first_post[0]['publishedAt']);
        // $last_post_date = DateTime::createFromFormat('Y-m-d H:i:s', $last_post[0]['publishedAt']);

        $item = [];
        $item['profile'] = Yt_profile::where([['id', '=', $socialmediaid]])->orderBy('date', 'desc')->limit(1)->get()->toArray()[0];
        $item['post'] = [];
        $item['date']['profile']['first'] = $first_profile_date->format('Y-m-d');
        $item['date']['profile']['last'] = $last_profile_date->format('Y-m-d');
        $item['date']['post']['first'] = $first_profile_date->format('Y-m-d');
        $item['date']['post']['last'] = $last_profile_date->format('Y-m-d');
        // $item['date']['post']['first'] = $first_post_date->format('Y-m-d');
        // $item['date']['post']['last'] = $last_post_date->format('Y-m-d');

        return view('admin.youtube.dashboard', compact('item'));
    }


    public function get_limit_post($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $result = [];
        $post = Yt_post::where([['channelId', '=', $socialmediaid], ['date', '>=', $startdate], ['date', '<=', $enddate]]);
        $post = $post->orderBy('media_preview_like', 'desc')->limit(6)->get()->toArray();
        foreach ($post as $p) {
            // $p['datetimes'] = Carbon::createFromTimestamp($p['publishedAt'])->toFormattedDateString();
            $p['datetimes'] = $p['publishedAt'];
            array_push($result, $p);
        }
        return $result;
    }

    public function get_stat($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $profile_start = Yt_profile::where([['id', '=', $socialmediaid], ['date', '>=', $startdate]])->orderBy('date', 'asc')->limit(1)->get()->toArray();
        $profile_end = Yt_profile::where([['id', '=', $socialmediaid], ['date', '<=', $enddate]])->orderBy('date', 'desc')->limit(1)->get()->toArray();
        $like_start = Yt_post::where([['channelId', '=', $socialmediaid], ['publishedAt', '<=', $startdate]])->sum('likeCount');
        $like_end = Yt_post::where([['channelId', '=', $socialmediaid], ['publishedAt', '<=', $enddate]])->sum('likeCount');

        $result = [];

        if (count($profile_start) >= 1) {
            $result['subscriber_start'] = $profile_start[0]['subscriberCount'];
            $result['video_start'] = $profile_start[0]['videoCount'];
            $result['view_start'] = (int)$profile_start[0]['viewCount'];
        } else {
            $result['subscriber_start'] = 0;
            $result['video_start'] = 0;
            $result['view_start'] = 0;
        }
        if (count($profile_end) >= 1) {
            $result['subscriber_end'] = $profile_end[0]['subscriberCount'];
            $result['video_end'] = $profile_end[0]['videoCount'];
            $result['view_end'] = (int)$profile_end[0]['viewCount'];
        } else {
            $result['subscriber_end'] = 0;
            $result['video_end'] = 0;
            $result['view_end'] = 0;
        }

        if ($like_start) {
            $result['like_start'] = $like_start;
        } else {
            $result['like_start'] = 0;
        }
        if ($like_end) {
            $result['like_end'] = $like_end;
        } else {
            $result['like_end'] = 0;
        }

        $result['subscriber_total'] = $result['subscriber_end'];
        $result['subscriber_growth'] = $result['subscriber_end'] - $result['subscriber_start'];
        $result['video_total'] = $result['video_end'];
        $result['video_growth'] = $result['video_end'] - $result['video_start'];
        $result['view_total'] = $result['view_end'];
        $result['view_growth'] = $result['view_end'] - $result['view_start'];
        $result['like_total'] = $result['like_end'];
        $result['like_growth'] = $result['like_end'] - $result['like_start'];
        return $result;
    }

    public function get_total_followers($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $total = [];

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($startdate, $interval, $enddate);

        $temp_number = 0;
        foreach ($period as $dt) {
            $now = $dt;
            $tomorrow = DateTime::createFromFormat('Y-m-d',  $now->format('Y-m-d'))->modify('1 day');

            $followed_by = Yt_profile::where([['id', '=', $socialmediaid], ['date', '>=', $now], ['date', '<=', $tomorrow]])->limit(1)->get(['subscriberCount'])->toArray();
            if($followed_by) {
                $temp_number = $followed_by[0]['subscriberCount'];
            }

            if($temp_number > 0) {
                $temp = [];
                $temp['date'] = $now->format('Y-m-d');
                $temp['followed_by'] = $temp_number;
                array_push($total, $temp);
            }
        }

        return $total;
    }

    public function get_growth_followers($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $growth = [];
        $growth_final = [];
        $result = [];
        $result['data'] = [];
        $result['total'] = 0;
        $result['change'] = 0;
        $result['max'] = 0;
        $result['max_date'] = 0;
        $result['average'] = 0;

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($startdate, $interval, $enddate);

        $temp_number = 0;
        foreach ($period as $dt) {
            $now = $dt;
            $tomorrow = DateTime::createFromFormat('Y-m-d',  $now->format('Y-m-d'))->modify('1 day');

            $followed_by = Yt_profile::where([['id', '=', $socialmediaid], ['date', '>=', $now], ['date', '<=', $tomorrow]])->limit(1)->get(['subscriberCount'])->toArray();
            if($followed_by) {
                $temp_number = $followed_by[0]['subscriberCount'];
            }

            if($temp_number > 0) {
                $temp = [];
                $temp['date'] = $now->format('Y-m-d');
                $temp['followed_by'] = $temp_number;
                array_push($growth, $temp);
            }
        }
        $result['total'] = $growth[count($growth)-1]['followed_by'];
        $result['change'] = $growth[count($growth)-1]['followed_by'] - $growth[0]['followed_by'];
        $result['average'] = $result['change'] / count($growth);

        for ($i=1; $i<count($growth); $i++){
            $temp = [];
            $temp['date'] = $growth[$i]['date'];
            $temp['followed_by'] = $growth[$i]['followed_by'] - $growth[$i-1]['followed_by'];
            array_push($growth_final, $temp);

            if ($temp['followed_by'] > $result['max']) {
                $result['max'] = $temp['followed_by'];
                $result['max_date'] = $temp['date'];
            }
        }
        $result['data'] = $growth_final;

        return $result;
    }

    public function get_number_posts($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $number = [];
        $number['data'] = [];
        $number['total'] = 0;
        $number['average'] = 0;

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($startdate, $interval, $enddate);

        $number_post_total = 0;
        foreach ($period as $dt) {
            $now = $dt;
            $tomorrow = DateTime::createFromFormat('Y-m-d',  $now->format('Y-m-d'))->modify('1 day');

            $number_post = Yt_post::where([['channelId', '=', $socialmediaid], ['publishedAt', '>=', $now], ['publishedAt', '<', $tomorrow]])->count();

            $number_post_total = $number_post_total + $number_post;
            $temp = [];
            $temp['date'] = $now->format('Y-m-d');
            $temp['number_post'] = $number_post;
            $temp['number_post_total'] = $number_post_total;
            array_push($number['data'], $temp);
            $number['total'] = $number['total'] + $temp['number_post'];
        }
        $number['average'] = $number['total'] / count($number['data']);

        return $number;
    }

    public function get_distribution_interactions($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate);

        $like = Yt_post::where([['channelId', '=', $socialmediaid], ['publishedAt', '>=', $startdate], ['publishedAt', '<=', $enddate]])->sum('likeCount');
        $dislike = Yt_post::where([['channelId', '=', $socialmediaid], ['publishedAt', '>=', $startdate], ['publishedAt', '<=', $enddate]])->sum('dislikeCount');
        $comment = Yt_post::where([['channelId', '=', $socialmediaid], ['publishedAt', '>=', $startdate], ['publishedAt', '<=', $enddate]])->sum('commentCount');

        $result = [];
        $result['like'] = $like;
        $result['dislike'] = $dislike;
        $result['comment'] = $comment;

        return $result;
    }

    public function get_evolution_interactions($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $result = [];
        $result['data'] = [];
        $result['max'] = 0;
        $result['max_date'] = 0;
        $result['total'] = 0;
        $result['average'] = 0;

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($startdate, $interval, $enddate);

        foreach ($period as $dt) {
            $now = $dt;
            $tomorrow = DateTime::createFromFormat('Y-m-d',  $now->format('Y-m-d'))->modify('1 day');

            $like = Yt_post::where([['channelId', '=', $socialmediaid], ['publishedAt', '>=', $now], ['publishedAt', '<=', $tomorrow]])->sum('likeCount');
            $dislike = Yt_post::where([['channelId', '=', $socialmediaid], ['publishedAt', '>=', $now], ['publishedAt', '<=', $tomorrow]])->sum('dislikeCount');
            $comment = Yt_post::where([['channelId', '=', $socialmediaid], ['publishedAt', '>=', $now], ['publishedAt', '<=', $tomorrow]])->sum('commentCount');

            $temp = [];
            $temp['date'] = $now->format('Y-m-d');
            $temp['like'] = $like;
            $temp['dislike'] = $dislike;
            $temp['comment'] = $comment;

            array_push($result['data'], $temp);
        }

        foreach ($result['data'] as $dt) {
            if ($dt['like'] + $dt['comment'] > $result['max']) {
                $result['max'] = $dt['like'] + $dt['dislike'] + $dt['comment'];
                $result['max_date'] = $dt['date'];
            }
            $result['total'] = $result['total'] + $dt['like'] + $dt['dislike'] + $dt['comment'];
        }
        $result['average'] = $result['total'] / count($result['data']);

        return $result;
    }

    public function get_number_views($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $number = [];
        $number['data'] = [];
        $number['total'] = 0;
        $number['average'] = 0;

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($startdate, $interval, $enddate);

        $number_view_total = 0;
        foreach ($period as $dt) {
            $now = $dt;
            $tomorrow = DateTime::createFromFormat('Y-m-d',  $now->format('Y-m-d'))->modify('1 day');

            $number_view = Yt_post::where([['channelId', '=', $socialmediaid], ['publishedAt', '>=', $now], ['publishedAt', '<', $tomorrow]])->sum('viewCount');

            $number_view_total = $number_view_total + $number_view;
            $temp = [];
            $temp['date'] = $now->format('Y-m-d');
            $temp['number_view'] = $number_view;
            $temp['number_view_total'] = $number_view_total;
            array_push($number['data'], $temp);
            $number['total'] = $number['total'] + $temp['number_view'];
        }
        $number['average'] = $number['total'] / count($number['data']);

        return $number;
    }

}
