<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Notifications\Notifiable;
use App\Fb_post;
use App\Fb_profile;
use App\Socialmedia;
use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;
use Session;
use Validator;
use Hash;
use \Carbon\Carbon;
use Auth;
use DateTime;
use DateInterval;
use DatePeriod;
use MongoDB\BSON\UTCDateTime as MongoDate;
use PhpParser\Node\Stmt\Foreach_;
use Illuminate\Support\Facades\DB;


class FacebookController extends Controller
{
    use Notifiable;

    public function __construct()
    {
        //
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.facebook.index');
    }

    public function indexjson()
    {
        $data_username = Socialmedia::where([['is_active', '=', true],['category', '=', 'facebook'],['created_by', '=', Auth::id()]])->get()->toArray();
        $data_full = [];
        foreach($data_username as $du) {
            $last_update = Fb_post::where([['username', '=', $du['username']]])->orderBy('date', 'desc')->limit(1)->get()->toArray();
            $datetime = $last_update['0']['date']->toDateTime();
            $du['last_update'] = $datetime->format('Y-m-d H:i:s');
            $du['profile_pic'] = 'https://graph.facebook.com/' . $du['username'] . '/picture';
            array_push($data_full, $du);
        }
        return datatables($data_full)
        ->addColumn('profile_pic', function ($dt) {
            return view('admin.facebook.profilepic',compact('dt'));
        })
        ->addColumn('action', function ($dt) {
            return view('admin.facebook.action',compact('dt'));
        })
        ->toJson();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function dashboard($socialmediaid)
    {
        // $first_profile = Fb_profile::where([['username', '=', $socialmediaid]])->orderBy('date', 'asc')->limit(1)->get()->toArray();
        // $last_profile = Fb_profile::where([['username', '=', $socialmediaid]])->orderBy('date', 'desc')->limit(1)->get()->toArray();
        // $first_profile_date = DateTime::createFromFormat('Y-m-d H:i:s', $first_profile[0]['date']);
        // $last_profile_date = DateTime::createFromFormat('Y-m-d H:i:s', $last_profile[0]['date']);

        $last_post = Fb_post::where([['username', '=', $socialmediaid]])->orderBy('time', 'desc')->limit(1)->get()->toArray();
        $first_post = Fb_post::where([['username', '=', $socialmediaid]])->orderBy('time', 'asc')->limit(1)->get()->toArray();
        $last_post_date = $last_post[0]['time']->toDateTime();
        $first_post_date = $last_post[0]['time']->toDateTime()->modify('-90 day');

        $item = [];
        // $item['profile'] = Fb_profile::where([['username', '=', $socialmediaid]])->orderBy('date', 'desc')->limit(1)->get()->toArray()[0];
        $item['profile'] = [];
        $item['profile']['username'] = $socialmediaid;
        $item['profile']['full_name'] = $socialmediaid;
        $item['profile']['biography'] = '';
        $item['profile']['profile_pic_url'] = 'https://graph.facebook.com/' . $socialmediaid . '/picture';
        $item['profile']['is_private'] = false;
        $item['profile']['is_verified'] = false;
        $item['profile']['fans'] = '-';
        $item['profile']['post'] = Fb_post::where([['username', '=', $socialmediaid]])->count();
        $item['profile']['like'] = Fb_post::where([['username', '=', $socialmediaid]])->sum('likes');
        $item['profile']['date'] = $last_post_date->format('Y-m-d');
        $item['post'] = [];
        $item['date']['profile']['first'] = $first_post_date->format('Y-m-d');
        $item['date']['profile']['last'] = $last_post_date->format('Y-m-d');
        $item['date']['post']['first'] = $first_post_date->format('Y-m-d');
        $item['date']['post']['last'] = $last_post_date->format('Y-m-d');

        return view('admin.facebook.dashboard', compact('item'));
    }

    public function get_limit_post($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $result = [];
        $post = Fb_post::where([['username', '=', $socialmediaid], ['time', '>=', $startdate], ['time', '<=', $enddate]]);
        $post = $post->orderBy('likes', 'desc')->limit(6)->get()->toArray();
        foreach ($post as $p) {
            $p['datetimes'] =$p['time']->toDateTime()->format('Y-m-d');
            array_push($result, $p);
        }
        return $result;
    }

    public function get_stat($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        // $profile_start = Fb_profile::where([['username', '=', $socialmediaid], ['date', '>=', $startdate]])->orderBy('date', 'asc')->limit(1)->get()->toArray();
        // $profile_end = Fb_profile::where([['username', '=', $socialmediaid], ['date', '<=', $enddate]])->orderBy('date', 'desc')->limit(1)->get()->toArray();
        $post_start = Fb_post::where([['username', '=', $socialmediaid], ['time', '<=', $startdate]])->count();
        $post_end = Fb_post::where([['username', '=', $socialmediaid], ['time', '<=', $enddate]])->count();
        $like_start = Fb_post::where([['username', '=', $socialmediaid], ['time', '<=', $startdate]])->sum('likes');
        $like_end = Fb_post::where([['username', '=', $socialmediaid], ['time', '<=', $enddate]])->sum('likes');
        $comment_start = Fb_post::where([['username', '=', $socialmediaid], ['time', '<=', $startdate]])->sum('comments');
        $comment_end = Fb_post::where([['username', '=', $socialmediaid], ['time', '<=', $enddate]])->sum('comments');

        if ($post_start) {
            $result['post_start'] = $post_start;
        } else {
            $result['post_start'] = 0;
        }
        if ($post_end) {
            $result['post_end'] = $post_end;
        } else {
            $result['post_end'] = 0;
        }

        if ($like_start) {
            $result['like_start'] = $like_start;
        } else {
            $result['like_start'] = 0;
        }
        if ($like_end) {
            $result['like_end'] = $like_end;
        } else {
            $result['like_end'] = 0;
        }

        if ($comment_start) {
            $result['comment_start'] = $comment_start;
        } else {
            $result['comment_start'] = 0;
        }
        if ($comment_end) {
            $result['comment_end'] = $comment_end;
        } else {
            $result['comment_end'] = 0;
        }

        $result['fans_total'] = '-';
        $result['fans_growth'] = '-';
        $result['post_total'] = $result['post_end'];
        $result['post_growth'] = $result['post_end'] - $result['post_start'];
        $result['like_total'] = $result['like_end'];
        $result['like_growth'] = $result['like_end'] - $result['like_start'];
        $result['comment_total'] = $result['comment_end'];
        $result['comment_growth'] = $result['comment_end'] - $result['comment_start'];
        return $result;
    }

    public function get_total_followers($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $total = [];

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($startdate, $interval, $enddate);

        $temp_number = 0;
        foreach ($period as $dt) {
            $now = $dt;
            $tomorrow = DateTime::createFromFormat('Y-m-d',  $now->format('Y-m-d'))->modify('1 day');

            $followed_by = Fb_profile::where([['username', '=', $socialmediaid], ['time', '>=', $now], ['time', '<=', $tomorrow]])->limit(1)->get(['followed_by'])->toArray();
            if($followed_by) {
                $temp_number = $followed_by[0]['followed_by'];
            }

            if($temp_number > 0) {
                $temp = [];
                $temp['date'] = $now->format('Y-m-d');
                $temp['followed_by'] = $temp_number;
                array_push($total, $temp);
            }
        }

        return $total;
    }

    public function get_growth_followers($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $growth = [];
        $growth_final = [];
        $result = [];
        $result['data'] = [];
        $result['total'] = 0;
        $result['change'] = 0;
        $result['max'] = 0;
        $result['max_date'] = 0;
        $result['average'] = 0;

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($startdate, $interval, $enddate);

        $temp_number = 0;
        foreach ($period as $dt) {
            $now = $dt;
            $tomorrow = DateTime::createFromFormat('Y-m-d',  $now->format('Y-m-d'))->modify('1 day');

            $followed_by = Fb_profile::where([['username', '=', $socialmediaid], ['time', '>=', $now], ['time', '<=', $tomorrow]])->limit(1)->get(['followed_by'])->toArray();
            if($followed_by) {
                $temp_number = $followed_by[0]['followed_by'];
            }

            if($temp_number > 0) {
                $temp = [];
                $temp['date'] = $now->format('Y-m-d');
                $temp['followed_by'] = $temp_number;
                array_push($growth, $temp);
            }
        }
        if (count($growth) > 1) {
            $result['total'] = $growth[count($growth)-1]['followed_by'];
            $result['change'] = $growth[count($growth)-1]['followed_by'] - $growth[0]['followed_by'];
            $result['average'] = $result['change'] / count($growth);
        } else {
            $result['total'] = 0;
            $result['change'] = 0;
            $result['average'] = 0;
        }

        for ($i=1; $i<count($growth); $i++){
            $temp = [];
            $temp['date'] = $growth[$i]['date'];
            $temp['followed_by'] = $growth[$i]['followed_by'] - $growth[$i-1]['followed_by'];
            array_push($growth_final, $temp);

            if ($temp['followed_by'] > $result['max']) {
                $result['max'] = $temp['followed_by'];
                $result['max_date'] = $temp['date'];
            }
        }
        $result['data'] = $growth_final;

        return $result;
    }

    public function get_number_posts($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $number = [];
        $number['data'] = [];
        $number['total'] = 0;
        $number['average'] = 0;

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($startdate, $interval, $enddate);

        foreach ($period as $dt) {
            $now = $dt;
            $tomorrow = DateTime::createFromFormat('Y-m-d',  $now->format('Y-m-d'))->modify('1 day');

            $number_post = Fb_post::where([['username', '=', $socialmediaid], ['time', '>=', $now], ['time', '<', $tomorrow]])->count();

            $temp = [];
            $temp['date'] = $now->format('Y-m-d');
            $temp['number_post'] = $number_post;
            array_push($number['data'], $temp);
            $number['total'] = $number['total'] + $temp['number_post'];
        }
        $number['average'] = $number['total'] / count($number['data']);

        return $number;
    }

    public function get_distribution_posts($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate);

        $types_image = Fb_post::where([['username', '=', $socialmediaid], ['image', '!=', null], ['time', '>=', $startdate], ['time', '<=', $enddate]])->count();
        $types_video = Fb_post::where([['username', '=', $socialmediaid], ['video', '!=', null], ['time', '>=', $startdate], ['time', '<=', $enddate]])->count();
        $types_text = Fb_post::where([['username', '=', $socialmediaid], ['image', '=', null], ['video', '=', null], ['time', '>=', $startdate], ['time', '<=', $enddate]])->count();

        $result = [];
        $result['types_image'] = $types_image;
        $result['types_video'] = $types_video;
        $result['types_text'] = $types_text;

        return $result;
    }

    public function get_number_posts_type($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $result = [];

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($startdate, $interval, $enddate);

        foreach ($period as $dt) {
            $now = $dt;
            $tomorrow = DateTime::createFromFormat('Y-m-d',  $now->format('Y-m-d'))->modify('1 day');

            $types_image = Fb_post::where([['username', '=', $socialmediaid], ['image', '!=', null], ['time', '>=', $now], ['time', '<', $tomorrow]])->count();
            $types_video = Fb_post::where([['username', '=', $socialmediaid], ['video', '!=', null], ['time', '>=', $now], ['time', '<', $tomorrow]])->count();
            $types_text = Fb_post::where([['username', '=', $socialmediaid], ['image', '=', null], ['video', '=', null], ['time', '>=', $now], ['time', '<', $tomorrow]])->count();

            $temp = [];
            $temp['date'] = $now->format('Y-m-d');
            $temp['types_image'] = $types_image;
            $temp['types_video'] = $types_video;
            $temp['types_text'] = $types_text;

            array_push($result, $temp);
        }

        return $result;
    }

    public function get_average_interaction_post($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $result = [];
        $result['data'] = [];
        $result['max'] = 0;
        $result['max_date'] = 0;
        $result['min'] = 0;
        $result['min_date'] = 0;
        $result['average'] = 0;
        $result['count'] = 0;
        $result['total'] = 0;

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($startdate, $interval, $enddate);

        foreach ($period as $dt) {
            $now = $dt;
            $tomorrow = DateTime::createFromFormat('Y-m-d',  $now->format('Y-m-d'))->modify('1 day');

            $like = Fb_post::where([['username', '=', $socialmediaid], ['time', '>=', $now], ['time', '<', $tomorrow]])->sum('likes');
            $comment = Fb_post::where([['username', '=', $socialmediaid], ['time', '>=', $now], ['time', '<', $tomorrow]])->sum('comments');
            $share = Fb_post::where([['username', '=', $socialmediaid], ['time', '>=', $now], ['time', '<', $tomorrow]])->sum('shares');
            $count = Fb_post::where([['username', '=', $socialmediaid], ['time', '>=', $now], ['time', '<', $tomorrow]])->count();

            $temp = [];
            $temp['date'] = $now->format('Y-m-d');
            $temp['interaction'] = $like + $comment + $share;
            $temp['count'] = $count;
            array_push($result['data'], $temp);
        }

        foreach ($result['data'] as $dt) {
            if ($dt['interaction'] > 0) {
                $result['min'] = $dt['interaction'];
                $result['min_date'] = $dt['date'];
            }
        }

        foreach ($result['data'] as $dt) {
            $result['count'] = $result['count'] + $dt['count'];
            $result['total'] = $result['total'] + $dt['interaction'];
            if ($dt['interaction'] > $result['max']) {
                $result['max'] = $dt['interaction'];
                $result['max_date'] = $dt['date'];
            }
            if (($dt['interaction'] > 0) && ($dt['interaction'] < $result['min'])) {
                $result['min'] = $dt['interaction'];
                $result['min_date'] = $dt['date'];
            }
        }


        $result['average'] = $result['total'] / $result['count'];
        return $result;
    }

    public function get_distribution_interactions($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate);

        $like = Fb_post::where([['username', '=', $socialmediaid], ['time', '>=', $startdate], ['time', '<=', $enddate]])->sum('likes');
        $comment = Fb_post::where([['username', '=', $socialmediaid], ['time', '>=', $startdate], ['time', '<=', $enddate]])->sum('comments');
        $share = Fb_post::where([['username', '=', $socialmediaid], ['time', '>=', $startdate], ['time', '<=', $enddate]])->sum('shares');

        $result = [];
        $result['like'] = $like;
        $result['comment'] = $comment;
        $result['share'] = $share;

        return $result;
    }

    public function get_evolution_interactions($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $result = [];
        $result['data'] = [];
        $result['max'] = 0;
        $result['max_date'] = 0;
        $result['total'] = 0;
        $result['average'] = 0;

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($startdate, $interval, $enddate);

        foreach ($period as $dt) {
            $now = $dt;
            $tomorrow = DateTime::createFromFormat('Y-m-d',  $now->format('Y-m-d'))->modify('1 day');

            $like = Fb_post::where([['username', '=', $socialmediaid], ['time', '>=', $now], ['time', '<', $tomorrow]])->sum('likes');
            $comment = Fb_post::where([['username', '=', $socialmediaid], ['time', '>=', $now], ['time', '<', $tomorrow]])->sum('comments');
            $share = Fb_post::where([['username', '=', $socialmediaid], ['time', '>=', $now], ['time', '<', $tomorrow]])->sum('shares');

            $temp = [];
            $temp['date'] = $now->format('Y-m-d');
            $temp['like'] = $like;
            $temp['comment'] = $comment;
            $temp['share'] = $share;

            array_push($result['data'], $temp);
        }

        foreach ($result['data'] as $dt) {
            if ($dt['like'] + $dt['comment'] > $result['max']) {
                $result['max'] = $dt['like'] + $dt['comment'] + $dt['share'];
                $result['max_date'] = $dt['date'];
            }
            $result['total'] = $result['total'] + $dt['like'] + $dt['comment']+ $dt['share'];
        }
        $result['average'] = $result['total'] / count($result['data']);

        return $result;
    }

    public function get_number_reactions($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $result = [];
        $result['data'] = [];

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($startdate, $interval, $enddate);

        foreach ($period as $dt) {
            $now = $dt;
            $tomorrow = DateTime::createFromFormat('Y-m-d',  $now->format('Y-m-d'))->modify('1 day');

            $like = Fb_post::where([['username', '=', $socialmediaid], ['time', '>=', $now], ['time', '<', $tomorrow]])->sum('reactions_like');
            $haha = Fb_post::where([['username', '=', $socialmediaid], ['time', '>=', $now], ['time', '<', $tomorrow]])->sum('reactions_haha');
            $love = Fb_post::where([['username', '=', $socialmediaid], ['time', '>=', $now], ['time', '<', $tomorrow]])->sum('reactions_love');
            $sorry = Fb_post::where([['username', '=', $socialmediaid], ['time', '>=', $now], ['time', '<', $tomorrow]])->sum('reactions_sorry');
            $support = Fb_post::where([['username', '=', $socialmediaid], ['time', '>=', $now], ['time', '<', $tomorrow]])->sum('reactions_support');
            $wow = Fb_post::where([['username', '=', $socialmediaid], ['time', '>=', $now], ['time', '<', $tomorrow]])->sum('reactions_wow');

            $temp = [];
            $temp['date'] = $now->format('Y-m-d');
            $temp['like'] = $like;
            $temp['haha'] = $haha;
            $temp['love'] = $love;
            $temp['sorry'] = $sorry;
            $temp['support'] = $support;
            $temp['wow'] = $wow;

            array_push($result['data'], $temp);
        }

        return $result;
    }

    public function get_engaging_posts_type($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $image = Fb_post::where([['username', '=', $socialmediaid], ['image', '!=', null], ['time', '>=', $startdate], ['time', '<', $enddate]])->sum('likes');
        $video = Fb_post::where([['username', '=', $socialmediaid], ['video', '!=', null], ['time', '>=', $startdate], ['time', '<', $enddate]])->sum('likes');
        $text = Fb_post::where([['username', '=', $socialmediaid], ['image', '=', null], ['video', '=', null], ['time', '>=', $startdate], ['time', '<', $enddate]])->sum('likes');

        $result = [];
        $result['image'] = $image;
        $result['video'] = $video;
        $result['text'] = $text;

        return $result;
    }

    public function get_user_activity($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $result = Fb_post::where([['username', '=', $socialmediaid], ['time', '>=', $startdate], ['time', '<', $enddate]]);
        $result = $result->orderBy('time', 'asc');
        $result = $result->get(['time']);
        $result = $result->groupBy(function($item) {
            return $item->time->toDateTime()->format('Y-m-d H');
        });
        $result = $result->toArray();

        $results = [];
        foreach($result as $key => $value) {
            $arr = explode(" ", $key);
            $temp = [];
            $temp['x'] = $arr[0];
            $temp['y'] = (int)$arr[1];
            $temp['r'] = count($value);
            array_push($results, $temp);
        }

        return $results;
    }

}
