<?php

namespace App\Http\Controllers\Admin\Uploadsearch;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;
use App\Commercialsearch;
use \Carbon\Carbon;
use Rap2hpoutre\FastExcel\FastExcel;
use MongoDB\Client;

use DB;

class CommercialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.uploadsearch.commercial');
    }

    public function indexjson(Request $request)
    {
        $draw = $request->draw;
        $start = (isset($request->start) ? (int)$request->start : 0);
        $length = (isset($request->length) ? (int)$request->length : 10);

        $search = (isset($request->search['value']))? $request->search['value'] : false;

        $orderColumn = (isset($request->order[0]['column']))? $request->order[0]['column'] : 0;
        $order = (isset($request->order[0]['dir']))? $request->order[0]['dir'] : 'asc';

        $mappingColumn = [
            '_id', 'nsector', 'ncategory', 'nadvertiser', 'nproduct', 
            'ncopy', 'isector','icategory', 'iadvertiser', 
            'iadvertiser_group', 'iproduct', 'icopy'
        ];

        #start
        $mainQuery = [];
        $allQuery = [];

        #search
        if($search){
            array_push($mainQuery, [
                '$match' => [
                    '$or' => [
                        [
                            '_id' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ],
                        [
                            'nsector' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ],
                        [
                            'ncategory' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ],
                        [
                            'nadvertiser' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ],
                        [
                            'nproduct' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ],
                        [
                            'ncopy' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ],
                        [
                            'isector' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ],
                        [
                            'icategory' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ],
                        [
                            'iadvertiser' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ],
                        [
                            'iadvertiser_group' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ],
                        [
                            'iproduct' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ],
                        [
                            'icopy' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ]
                    ]
                ]
            ]);
        }
        $allQuery = $mainQuery; #all data

        #orderby
        if($order == 'asc'){
            $order = 1;
        }else{
            $order = -1;
        }
        array_push($mainQuery, [
            '$sort' => [
                $mappingColumn[$orderColumn] => $order
            ]
        ]);

        #limit offset
        if(isset($start)) array_push($mainQuery, ['$skip' => $start]);
        if(isset($length)) array_push($mainQuery, ['$limit' => $length]);

        #query data
        // DB::connection('mongodb')->enableQueryLog();
        $query = Commercialsearch::raw(function($collection) use($mainQuery)
        {
            // dd($mainQuery);
            return $collection->aggregate(array_merge(
                [], $mainQuery), ['allowDiskUse' => true, 'maxTimeMS' => 600000] #max 5 minutes
            );
        });
        // dd(DB::connection('mongodb')->getQueryLog());
        // dd($query);
        $response['draw'] = $draw;
        $response['data'] = $query;
        $response['recordsTotal'] = $response['data']->count();

        #query all data
        array_push($allQuery, ['$count' => 'count']);
        $queryAll = Commercialsearch::raw(function($collection) use($allQuery)
        {
            // dd($allQuery);
            return $collection->aggregate(array_merge(
                [], $allQuery), ['allowDiskUse' => true, 'maxTimeMS' => 600000] #max 5 minutes
            );
        });
        // dd($queryAll);
        $response['recordsFiltered'] = (isset($queryAll[0]['count']) ? $queryAll[0]['count'] : 0);

        return response()->json($response);
    }

    public function upload(Request $request)
    {             
        $upload_path = 'uploads/temp';
        if ($request->file('file')->isValid()) {
            $file = $request->file('file');
            // File Details 
            $filename = $file->getClientOriginalName();
            $upload_filename = Carbon::now()->format('Ymd').$filename;
            $extension = $file->getClientOriginalExtension();
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize();
            $mimeType = $file->getMimeType();

            $data['filename'] = $filename;
            $data['mimeType'] = $mimeType;
            $data['fileSize'] = $fileSize;

            // store file to temp folder
            $file->move($upload_path,$upload_filename);

            // import to database
            // $imp = (new FastExcel)->configureCsv(';', '}', '\n', 'gbk')->import($upload_path.'/'.$upload_filename, function ($line) {
            //     $insertData = [];
            //     foreach($line as $key=>$val){
            //         $colname = strtolower($key);
            //         $colname = str_replace(' ','_',$colname);
            //         $colname = str_replace('.','',$colname);
            //         $insertData[$colname] = str_replace('�',' ',$val);
            //     }
            //     return Commercialsearch::create($insertData);
            // });

            // $data['rowCount'] = $imp->count();
            
            $header = [];
            $handle = fopen($upload_path.'/'.$upload_filename, 'r');
            if ($handle) {
                $i = 0;
                $insert = [];
                while ($line = fgetcsv($handle, null, '|')) {
                    $insertData = [];
                    if($i == 0){
                        //header, save as column name
                        $header = explode(";",$line[0]);
                        $header = array_map('strtolower', $header);
                        $header = str_replace(' ', '_', $header);
                        $header = str_replace('.', '', $header);
                    }else{
                        $content = explode(";",$line[0]);// split line into columns
                        foreach( $content as $key => $value ){                            
                            $insertData[$header[$key]] = str_replace('�',' ',$value);
                        }
                        array_push($insert, $insertData);
                        if(count($insert) == 500){
                            // Commercial::insertMany($insertData);// insert after 1000
                            $mongoClient=new Client();
                            $mongodata=$mongoClient->vislog->commercialsearches;
                            $mongodata->insertMany($insert);
                            $insert = [];// reset
                        }
                    }
                    $i = $i+1; //increment = no longer header
                }
                if(count($insert) > 0){                
                    $mongoClient=new Client();
                    $mongodata=$mongoClient->vislog->commercialsearches;                            
                    $mongodata->insertMany($insert);
                }
            }
            unset($handle);
            unlink($upload_path.'/'.$upload_filename);

            $return = [
                'message' => 'Success',
                'data' => $data
            ];
            return response($return,200);
        }else{
            return response(['message'=>'Upload failed'],400);
        }
    }

    public function destroymulti(Request $request)
    {
        $ids = explode(';',htmlentities($request->id));
        foreach($ids as $id){
            Commercialsearch::where('_id',$id)->delete();
        }
        Session::flash('message', 'Search Data Commercial dihapus'); 
        Session::flash('alert-class', 'alert-success'); 
        return redirect('admin/uploadsearch/commercial');
    }

    public function csvall()
    {
        $export = Commercialsearch::all();
        $filename = 'vislog-commercial-search-uploaded.csv';
        $temp = 'uploads/temp/'.$filename;
        (new FastExcel($export))->export($temp);
        $headers = [
            'Content-Type: text/csv',
            ];
        return response()->download($temp, $filename, $headers)->deleteFileAfterSend(true);
    }
}
