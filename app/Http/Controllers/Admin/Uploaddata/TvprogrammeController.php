<?php

namespace App\Http\Controllers\Admin\Uploaddata;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Tvprogramme;
use App\Log;
use Auth;
use \Carbon\Carbon;
use Rap2hpoutre\FastExcel\FastExcel;

use DB;

class TvprogrammeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $data['startdate'] = (isset($request->startdate) ? Carbon::parse($request->startdate)->format('Y-m-d') : Carbon::now()->addMonths(-1)->format('Y-m-d'));
        $data['enddate'] = (isset($request->enddate) ? Carbon::parse($request->enddate)->format('Y-m-d') : Carbon::now()->format('Y-m-d'));

        return view('admin.uploaddata.tvprogramme', $data);
    }

    public function indexjson(Request $request)
    {
        $greaterThan = (isset($request->gte) ? Carbon::parse($request->gte)->format('Y-m-d') : Carbon::now()->addMonths(-1)->format('Y-m-d'));
        $lessThan = (isset($request->lte) ? Carbon::parse($request->lte)->format('Y-m-d') : Carbon::now()->format('Y-m-d'));

        $draw = $request->draw;
        $start = (isset($request->start) ? (int)$request->start : 0);
        $length = (isset($request->length) ? (int)$request->length : 10);

        $search = (isset($request->search['value']))? $request->search['value'] : false;

        $orderColumn = (isset($request->order[0]['column']))? $request->order[0]['column'] : 0;
        $order = (isset($request->order[0]['dir']))? $request->order[0]['dir'] : 'asc';

        $mappingColumn = [
            '_id.date', '_id.year', '_id.month', '_id.date', 'count'
        ];

        #start
        $mainQuery = [];
        $allQuery = [];

        #filter range
        array_push($mainQuery, [
            '$match' => [
                'date' => [
                    '$gte' => $greaterThan,
                    '$lte' => $lessThan
                ]
            ]
        ]);

        #group and count
        array_push($mainQuery, [
            '$group'    => [
                '_id'   => [
                    'year'=>'$year',
                    'month'=>'$month',
                    'date'=>'$date',
                ],
                'count' => [
                    '$sum'  => 1
                ]
            ]
        ]);
        $allQuery = $mainQuery; #all data

        #search
        if($search){
            array_push($mainQuery, [
                '$match' => [
                    '$or' => [
                        [
                            '_id.date' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ],
                        [
                            '_id.year' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ],
                        [
                            '_id.month' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ],
                        [
                            'count' => [
                                '$gte' => (int)$search,
                                '$lte' => (int)$search
                            ]
                        ]
                    ]
                ]
            ]);
        }
        $allQuery = $mainQuery; #all data

        #orderby
        if($order == 'asc'){
            $order = 1;
        }else{
            $order = -1;
        }
        array_push($mainQuery, [
            '$sort' => [
                $mappingColumn[$orderColumn] => $order
            ]
        ]);

        #limit offset
        if(isset($start)) array_push($mainQuery, ['$skip' => $start]);
        if(isset($length)) array_push($mainQuery, ['$limit' => $length]);

        #query data
        // DB::connection('mongodb')->enableQueryLog();
        $query = Tvprogramme::raw(function($collection) use($mainQuery)
        {
            // dd($mainQuery);
            return $collection->aggregate(array_merge(
                [], $mainQuery)
            );
        });
        // dd(DB::connection('mongodb')->getQueryLog());
        // dd($query);
        $response['draw'] = $draw;
        $response['data'] = $query;
        $response['recordsTotal'] = $response['data']->count();

        #query all data
        array_push($allQuery, ['$count' => 'count']);
        $queryAll = Tvprogramme::raw(function($collection) use($allQuery)
        {
            // dd($allQuery);
            return $collection->aggregate(array_merge(
                [], $allQuery)
            );
        });
        // dd($queryAll);
        $response['recordsFiltered'] = (isset($queryAll[0]['count']) ? $queryAll[0]['count'] : 0);

        return response()->json($response);
    }

    public function upload(Request $request)
    {             
        Log::create(['user_id'=>Auth::user()->id,'action'=>'data update - tv programme','date'=>date('Y-m-d')]);
        $upload_path = 'uploads/temp';
        if ($request->file('file')->isValid()) {
            $file = $request->file('file');
            // File Details 
            $filename = $file->getClientOriginalName();
            $upload_filename = Carbon::now()->format('Ymd').$filename;
            $extension = $file->getClientOriginalExtension();
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize();
            $mimeType = $file->getMimeType();

            $data['filename'] = $filename;
            $data['mimeType'] = $mimeType;
            $data['fileSize'] = $fileSize;

            // store file to temp folder
            $file->move($upload_path,$upload_filename);

            // import to database
            $imp = (new FastExcel)->configureCsv(';', '}', '\n', 'gbk')->import($upload_path.'/'.$upload_filename, function ($line) {
                $insertData = [];
                foreach($line as $key=>$val){
                    $colname = strtolower($key);
                    $colname = str_replace(' ','_',$colname);
                    $colname = str_replace('.','',$colname);
                    switch($colname){
                    case 'date':
                        $date = Carbon::createFromFormat('Y-m-d H:i:s',$val.' 00:00:00')->toDateTimeString();
                        $insertData[$colname] = str_replace('�',' ',$val);
                        $insertData['isodate'] = new \MongoDB\BSON\UTCDateTime(new \DateTime($date));
                        break;
                    default:
                        $insertData[$colname] = str_replace('�',' ',$val);
                    }
                }
                return Tvprogramme::create($insertData);
            });

            $data['rowCount'] = $imp->count();
            
            unlink($upload_path.'/'.$upload_filename);

            $return = [
                'message' => 'Success',
                'data' => $data
            ];
            return response($return,200);
        }else{
            return response(['message'=>'Upload failed'],400);
        }
    }
    
    public function destroymulti(Request $request)
    {
        $dates = explode(",",htmlentities($request->date));
        foreach($dates as $date){
            Tvprogramme::where('date',$date)->delete();
        }
        return redirect('admin/uploaddata/tvprogramme');
    }

    public function csvall()
    {
        $exp = [];
        $export = Tvprogramme::raw(function($collection)
        {
            return $collection->aggregate([
                [
                    '$group'    => [
                        '_id'   => [
                            'year'=>'$year',
                            'month'=>'$month',
                            'date'=>'$date',
                        ],
                        'count' => [
                            '$sum'  => 1
                        ]
                    ]
                ]
            ]);
        });
        foreach($export as $key=>$val){
            $exp[$key]['year'] = $val->_id->year;
            $exp[$key]['month'] = $val->_id->month;
            $exp[$key]['date'] = $val->_id->date;
            $exp[$key]['count'] = $val->count;
        }
        $filename = 'vislog-tvprogramme-uploaded.csv';
        $temp = 'uploads/temp/'.$filename;
        (new FastExcel($exp))->export($temp);
        $headers = [
            'Content-Type: text/csv',
            ];
        return response()->download($temp, $filename, $headers)->deleteFileAfterSend(true);
    }
}
