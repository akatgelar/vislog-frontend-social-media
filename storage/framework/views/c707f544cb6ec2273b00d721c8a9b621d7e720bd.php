<?php $__env->startSection('pagetitle'); ?>
    <title>Upload Data - Commercial</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
        <!-- BEGIN : Main Content-->
        <div class="main-content">
          <div class="content-wrapper">
            <section id="page">
              <div class="row">
                <div class="col-sm-12">
                  <div class="content-header">Upload Data</div>                
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="card">
                    <?php if(Session::has('message')): ?>
                    <p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(ucfirst(Session::get('message'))); ?></p>
                    <?php endif; ?>
                    <div class="card-header col-sm-9">
                      <h4 class="card-title">Commercial</h4>       
                    </div>
                    <form role="form" method="POST" action="<?php echo e(route('admin.uploaddata.commercial-post')); ?>">
                      <?php echo csrf_field(); ?>
                      <div class="card-header col-sm-9">
                        <div id="daterange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
                          <i class="fa fa-calendar"></i>&nbsp;
                          <span></span> <i class="fa fa-caret-down"></i>
                          <input id="startdate" name="startdate" type="hidden" value="<?php echo e($startdate ?? ''); ?>">
                          <input id="enddate" name="enddate" type="hidden" value="<?php echo e($enddate ?? ''); ?>">
                        </div>
                        <br>
                        <div class="dt-buttons btn-group pull-right">
                          <button type="submit" id="filterersubmit" class="btn btn-warning buttons-csv buttons-html5 buttons-upload btn-outline-primary mr-1"><i class="ft-filter"></i> Process</button>
                          <a class="btn btn-danger buttons-csv buttons-html5 buttons-upload btn-outline-danger mr-1" href="<?php echo e(route('admin.uploaddata.commercial')); ?>"><span><i class="ft-x"></i></span> Reset</a>
                        </div>
                      </div>
                    </form>
                    <div class="card-content">
                      <div class="card-body card-dashboard table-responsive" style="height:100%;overflow-y: scroll;">
                        <table class="table browse-table">
                          <thead>
                            <tr>
                              <th></th>                  
                              <th class="year"><?php echo app('translator')->get('Year'); ?></th>
                              <th class="month"><?php echo app('translator')->get('Month'); ?></th>
                              <th class="date"><?php echo app('translator')->get('Date'); ?></th>
                              <th class="total_records"><?php echo app('translator')->get('Total Records'); ?></th>
                            </tr>
                          </thead>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <!--Calendar Ends-->
          </div>
        </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('modal'); ?>
        <div class="modal fade" id="uploadmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Upload File</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form method="POST" action="<?php echo e(url('admin/uploaddata/commercial/upload')); ?>" class="dropzone" id="upload-dropzone">
                  <?php echo csrf_field(); ?>
                </form>
              </div>
              <div class="modal-footer">
                <div class="modal-footer-processing-info" style="display:none">
                Inserting data in background... You can close this window once the upload progress bar is full.<i class="ft-refresh-cw font-medium-4 fa fa-spin align-middle"></i>
                </div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('pagecss'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets')); ?>/vendors/css/tables/datatable/datatables.min.css">
<link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets')); ?>/vendors/css/dropzone.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets')); ?>/css/daterangepicker.css" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('pagejs'); ?>
<script src="<?php echo e(asset('app-assets')); ?>/vendors/js/datatable/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo e(asset('app-assets')); ?>/vendors/js/datatable/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<?php echo e(asset('app-assets')); ?>/vendors/js/datatable/buttons.flash.min.js" type="text/javascript"></script>
<script src="<?php echo e(asset('app-assets')); ?>/vendors/js/datatable/jszip.min.js" type="text/javascript"></script>
<script src="<?php echo e(asset('app-assets')); ?>/vendors/js/datatable/pdfmake.min.js" type="text/javascript"></script>
<script src="<?php echo e(asset('app-assets')); ?>/vendors/js/datatable/vfs_fonts.js" type="text/javascript"></script>
<script src="<?php echo e(asset('app-assets')); ?>/vendors/js/datatable/buttons.html5.min.js" type="text/javascript"></script>
<script src="<?php echo e(asset('app-assets')); ?>/vendors/js/datatable/buttons.print.min.js" type="text/javascript"></script>
<script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>    
<script src="<?php echo e(asset('app-assets')); ?>/vendors/js/dropzone.min.js" type="text/javascript"></script>
<script src="<?php echo e(asset('app-assets')); ?>/js/dropzone.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo e(asset('app-assets')); ?>/vendors/js/moment.min.js"></script>
<script type="text/javascript" src="<?php echo e(asset('app-assets')); ?>/vendors/js/daterangepicker.min.js"></script>
<script>
$(document).ready(function() {
    var resp = false;
    if(window.innerWidth <= 800) resp=true;

    var table = $('.browse-table').DataTable({
        responsive: resp,
        processing: true,
        serverSide: true,
        ajax: "<?php echo e(route('admin.uploaddata.commercial.indexjson')); ?>?gte=<?php echo e($startdate); ?>&lte=<?php echo e($enddate); ?>",
        columns: [
          { data: '_id.date', name: 'checkbox' },
          { data: '_id.year', name: '_id.year' },
          { data: '_id.month', name: '_id.month' },
          { data: '_id.date', name: '_id.date' },
          { data: 'count', name: 'count' },
        ],
        dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'B>>"+
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        buttons: [            
            {
              extend: 'csv',
              text: '<i class="ft-upload-cloud"></i>',
              className: 'buttons-upload',
              action: function (e, node, config){
                $('#uploadmodal').modal('show')
                }
            },
            {
              extend: 'csv',
              text: '<i class="ft-download"></i>',
              className: 'buttons-csvall',
              action: function ( e, dt, node, config ) {
                  window.location = '<?php echo e(url('admin/uploaddata/commercial/csvall')); ?>'
              }
            },
            {
              text: '<i class="ft-trash"></i>', className: 'buttons-deletemulti',
              action: function ( e, dt, node, config ) {

              }
            },  
        ],
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        columnDefs: [ {
            targets: 0,
            data: null,
            defaultContent: '',
            orderable: false,
            searchable: false,
            checkboxes: {
                'selectRow': true
            }
        }],
        order: [[3, 'desc']],
        select: {
            style:    'multi',
            selector: 'td:first-child'
        },
    });
    $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel, .buttons-colvis, .buttons-csvall').addClass('btn btn-outline-primary mr-1');
    $('.buttons-add').addClass('btn mr-1');
    $('.buttons-deletemulti').addClass('btn-danger mr-1');

    $('.buttons-deletemulti').click(function(){
      var deletedates_arr = [];
      var rows_selected = table.column(0).checkboxes.selected();
      $.each(rows_selected, function(index, rowId){
         deletedates_arr.push(rowId);
      });
      var deletedates_str = encodeURIComponent(deletedates_arr);

      // Check any checkbox checked or not
      if(deletedates_arr.length > 0){
        var confirmdelete = confirm("Hapus seluruh data terpilih?");
        if (confirmdelete == true) {
          window.location = '<?php echo e(url('admin/uploaddata/commercial/destroymulti?date=')); ?>'+deletedates_str
        } 
      }
    });

    <?php if(!empty($startdate)): ?>
    var start = moment("<?php echo e($startdate); ?>");
    $('input[name=startdate]').val(start.format('YYYY-MM-DD'));
    <?php else: ?>
    var start = moment().subtract(1, 'month');
    $('input[name=startdate]').val(start.format('YYYY-MM-DD'));
    <?php endif; ?>    
    <?php if(!empty($enddate)): ?>
    var end = moment("<?php echo e($enddate); ?>");
    $('input[name=enddate]').val(end.format('YYYY-MM-DD'));
    <?php else: ?>
    var end = moment();
    $('input[name=enddate]').val(end.format('YYYY-MM-DD'));
    <?php endif; ?>

    function cb(start, end) {
        $('#daterange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#daterange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Hari ini': [moment(), moment()],
           'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           '7 Hari': [moment().subtract(6, 'days'), moment()],
           '30 Hari': [moment().subtract(29, 'days'), moment()],
           'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
           'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

    $('#daterange').on('apply.daterangepicker', function(ev, picker) {
      daterange = $('#daterange').data('daterangepicker');
      $('#startdate').val(daterange.startDate.format('YYYY-MM-DD'));
      $('#enddate').val(daterange.endDate.format('YYYY-MM-DD'));
    });
});
</script>
<script>
  // reload page after upload finishes
  Dropzone.options.uploadDropzone = {
    maxFilesize: 900, // Mb
    timeout: 900000, //ms
    init: function () {
        this.on("processing", function(file) { 
          $(".modal-footer-processing-info").show();
        });
        this.on("success", function(file) {           
          alert("Upload and insert finished."); 
          location.reload();
        });
        this.on("error", function(file,response) { 
          alert("Error: "+response.message); 
        });
      }
  };
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\vislog\resources\views/admin/uploaddata/commercial.blade.php ENDPATH**/ ?>