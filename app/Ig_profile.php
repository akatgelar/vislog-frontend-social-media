<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Ig_profile extends Eloquent
{
    protected $guarded = ['id'];
    protected $table = 'ig_profile';

    protected $casts = [
        'date' => 'datetime',
    ];
}
