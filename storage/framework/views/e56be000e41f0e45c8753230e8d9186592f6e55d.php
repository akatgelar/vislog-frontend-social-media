<?php $__env->startSection('pagetitle'); ?>
    <title>Daypart Setting</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
        <!-- BEGIN : Main Content-->
        <div class="main-content">
          <div class="content-wrapper"><!-- DOM - jQuery events table -->
<section id="browse-table">
  <div class="row">
    <div class="col-12">
      <div class="card">
      <?php if($errors->any()): ?>
              <p class="alert alert-danger">
              <?php echo ucfirst(implode('<br/>', $errors->all(':message'))); ?>

              </p>
      <?php endif; ?>
        <div class="card-header">
          <h4 class="card-title">Daypart Setting</h4>
        </div>
        <div class="card-content">
          <div class="px-3">
          <?php if(isset($item)): ?>
              <?php echo e(Form::model($item, ['url' => 'admin/daypartsetting/'.$item->id, 'method' => 'patch'])); ?>

          <?php else: ?>
              <?php echo e(Form::open(['url' => 'admin/daypartsetting'])); ?>

          <?php endif; ?>
              <div class="form-body">
                <div class="form-group row">
                  <label class="col-md-3 label-control" for="date">Daypart: </label>
                  <div class="col-md-9">
                  <?php echo e(Form::text('daypart', old('daypart',$item->daypart ?? null), array('class' => 'form-control','required','autocomplete'=>'off'))); ?>

                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 label-control" for="channel">Start Time: </label>
                  <div class="col-md-9">
                  <?php echo e(Form::text('start_time', old('start_time',$item->start_time ?? null), array('id'=>'start_time','class' => 'form-control','required','autocomplete'=>'off'))); ?>

                  </div>
                </div>    
                <div class="form-group row">
                  <label class="col-md-3 label-control" for="count">End Time: </label>
                  <div class="col-md-9">
                  <?php echo e(Form::text('end_time', old('end_time',$item->end_time ?? null), array('id'=>'end_time','class' => 'form-control','required','autocomplete'=>'off'))); ?>

                  </div>
                </div>                
              </div>
              <div class="form-actions">
                <a class="pull-right" href="<?php echo e(url('admin/daypartsetting')); ?>"><button type="button" class="btn btn-raised btn-warning mr-1">
                  <i class="ft-x"></i> Cancel
                </button></a>
                <button type="submit" class="pull-left btn btn-raised btn-primary mr-3">
                  <i class="fa fa-check-square-o"></i> Save
                </button>         
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- File export table -->

          </div>
        </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('pagecss'); ?>
<link href="<?php echo e(asset('css')); ?>/jquery.timepicker.min.css" rel="stylesheet" type="text/css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('pagejs'); ?>
<script src="<?php echo e(asset('js')); ?>/jquery.timepicker.min.js"></script>
<script>
  $(document).ready(function(){
    var options = { 'timeFormat': 'H:i','step':30 };
    $('#start_time').timepicker(options);
    $('#end_time').timepicker(options);
  });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\vislog\resources\views/admin/daypartsetting/createupdate.blade.php ENDPATH**/ ?>