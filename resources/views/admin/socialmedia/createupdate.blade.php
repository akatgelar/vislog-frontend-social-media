@extends('admin.layouts.app')

@section('pagetitle')
    <title>Social Media</title>
@endsection

@section('content')
<!-- BEGIN : Main Content-->
<div class="main-content">
    <div class="content-wrapper"><!-- DOM - jQuery events table -->
        <section id="browse-table">
            <div class="row">
                <div class="col-12">
                @if ($errors->any())
                <p class="alert alert-danger">
                    {!! ucfirst(implode('<br/>', $errors->all(':message'))) !!}
                </p>
                @endif
                <div class="card">
                    <div class="card-content">
                    <div class="px-3 form">
                    @if(isset($item))
                        {{ Form::model($item, ['url' => 'admin/socialmedia/'.$item->id.'/update', 'method' => 'patch']) }}
                    @else
                        {{ Form::open(['url' => 'admin/socialmedia/store']) }}
                    @endif
                        <h4 class="form-section"><i class="ft-message-circle"></i> Detail Social Media</h4>
                        <div class="form-body">
                            <div class="form-group row">
                            <label class="col-md-3 label-control" for="category">Category: </label>
                            <div class="col-md-9">
                            {{ Form::select('category',
                                ['facebook'=>'Facebook','instagram'=>'Instagram','twitter'=>'Twitter','youtube'=>'Youtube'],
                                old('category',$item->category ?? null),
                                array('class' => 'form-control')) }}
                            </div>
                            </div>
                            <div class="form-group row">
                            <label class="col-md-3 label-control" for="name">Name: </label>
                            <div class="col-md-9">
                            {{ Form::text('name', old('name',$item->name ?? null), array('class' => 'form-control','required')) }}
                            </div>
                            </div>
                            <div class="form-group row">
                            <label class="col-md-3 label-control" for="username">Username: </label>
                            <div class="col-md-9">
                            {{ Form::text('username', old('username',$item->username ?? null), array('class' => 'form-control','required')) }}
                            </div>
                            </div>
                            <div class="form-group row">
                            <label class="col-md-3 label-control" for="userid">User Id: </label>
                            <div class="col-md-9">
                            {{ Form::text('userid', old('userid',$item->userid ?? null), array('class' => 'form-control')) }}
                            </div>
                            </div>
                            <div class="form-group row">
                            <label class="col-md-3 label-control" for="description">Description: </label>
                            <div class="col-md-9">
                            {{ Form::text('description', old('description',$item->description ?? null), array('class' => 'form-control')) }}
                            </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 label-control" for="date">Active: </label>
                                <div class="col-md-9">
                                <div class="custom-control custom-switch">
                                    @if(isset($item->is_active))
                                    <input type="checkbox" class="custom-control-input" id="is_active" name="is_active" {{ ($item->is_active)? "checked":"" }}>
                                    @else
                                    <input type="checkbox" class="custom-control-input" id="is_active" name="is_active">
                                    @endif
                                    <label class="custom-control-label" for="is_active" id="status_text">Active</label>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <a class="pull-right" href="{{ url('admin/socialmedia') }}"><button type="button" class="btn btn-raised btn-warning mr-1">
                            <i class="ft-x"></i> Cancel
                            </button></a>
                            <button type="submit" class="pull-left btn btn-raised btn-primary mr-3">
                            <i class="fa fa-check-square-o"></i> Save
                            </button>
                        </div>
                        </form>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        </section>
        <!-- File export table -->
    </div>
</div>
@endsection
@section('pagecss')
<link href="{{ asset('css') }}/datepicker.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/daterangepicker.css" />
@endsection
@section('pagejs')
<script src="{{ asset('js') }}/datepicker.min.js"></script>
<script src="{{ asset('js') }}/i18n/datepicker.id.js"></script>
<script type="text/javascript" src="{{ asset('app-assets') }}/vendors/js/moment.min.js"></script>
<script type="text/javascript" src="{{ asset('app-assets') }}/vendors/js/daterangepicker.min.js"></script>
<script type="text/javascript">
$(function() {
    @if(isset($item->privileges['startdate']))
      var start = moment('{{$item->privileges['startdate']}}');
      $('input[name="privileges[startdate]"]').val(start.format('YYYY-MM-DD'));
      var end = moment('{{$item->privileges['enddate']}}');
      $('input[name="privileges[enddate]"]').val(end.format('YYYY-MM-DD'));
    @else
      var start = moment('2016-01-01');
      $('input[name="privileges[startdate]"]').val(start.format('YYYY-MM-DD'));
      var end = moment('2099-12-31');
      $('input[name="privileges[enddate]"]').val(end.format('YYYY-MM-DD'));
    @endif

    function cb(start, end) {
        $('#daterange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#daterange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Hari ini': [moment(), moment()],
           'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           '7 Hari': [moment().subtract(6, 'days'), moment()],
           '30 Hari': [moment().subtract(29, 'days'), moment()],
           'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
           'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

    $('#daterange').on('apply.daterangepicker', function(ev, picker) {
      daterange = $('#daterange').data('daterangepicker');
      $('#startdate').val(daterange.startDate.format('YYYY-MM-DD'));
      $('#enddate').val(daterange.endDate.format('YYYY-MM-DD'));
    });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $(".filter-button").click(function(){
    var filter = $(this).data("filter");
    $("#filter-title").html($(this).data("title"));
    $("input[name=filter-active]").val(filter);
    $("#filter-modal-search-result").empty();
    $('#search-term').val('');
    $("#filter-modal").modal();
    // show existing filter in modal footer
    $("#filter-selected").html('');
    $("#filter-selected").html($('input[name="privileges['+filter+']"]').val());
    // load directly
      $("#search-button").click();
  });
  $('#search-term').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
          $('#search-button').click();
        }
  });
  $("#search-button").click(function(){
    var filter = $("input[name=filter-active]").val();
    $("#filter-modal-search-result").empty();
    var term = $('#search-term').val();
    // get and show search result
    $.ajax({
      url: "{{ url('/admin/adsperformance') }}/search-"+filter+"-json",
      data: {term: term},
      success: function(result){
        if(result.length){
          $.each(result, function(k,v) {
            var curfil = $("#filter-selected").html().replace(/&amp;/g, '&');;
            curfil = curfil.split(";");
            if(!$("#filter-"+filter).is(':empty') && (jQuery.inArray(v[filter], curfil) != -1) ){
              // previously selected, set style
              $('#filter-modal-search-result').append( '<button class="btn search-result btn-primary mr-1" value="'+v[filter]+'">'+v[filter]+'</button>' );
            }else{
              $('#filter-modal-search-result').append( '<button class="btn search-result btn-outline-primary mr-1" value="'+v[filter]+'">'+v[filter]+'</button>' );
            }
          });
        }else{
          $('#filter-modal-search-result').html('<p>No result found</p>');
        }
    }});
  });
  $("#filter-modal-search-result").on('click', '.search-result', function() {
    var filter = $("input[name=filter-active]").val();
    count = $("#filter-"+filter+"-count").html(); // get initial button count
    $(this).toggleClass("btn-outline-primary");
    $(this).toggleClass("btn-primary");
    var filt = $(this).val();
    if($(this).hasClass('btn-primary')){
      count++; // selected, increment button count
      $("#filter-selected").html(function(i,html) {
        html = html + filt + ';';
        return html;
      });
    }else{
      count--; // unselected, decrement button count
      newval = $("#filter-selected").html().replace(/&amp;/g, '&');
      $("#filter-selected").html(newval.replace(filt+';',''));
    }
    $('input[name="privileges['+filter+']"]').val($("#filter-selected").html());
    $("#filter-"+filter+"-count").html(count); // set count at button
  });
  $("#filter-reset-selected").click(function(){
      var filter = $("input[name=filter-active]").val();
      $(".search-result.btn-primary").addClass("btn-outline-primary");
      $(".search-result.btn-primary").removeClass("btn-primary");
      $("#filter-selected").html('');
      $('input[name="privileges['+filter+']"]').val('');
      $("#filter-"+filter+"-count").html(''); // set count at button
    });
});
</script>
@endsection
