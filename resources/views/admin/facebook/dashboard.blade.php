@extends('admin.layouts.app')

@section('pagetitle')
    <title>Facebook Dashboard</title>
@endsection

@section('content')
    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-2">
                    <img src="{{ $item['profile']['profile_pic_url'] }}" style="margin-bottom: 5px; width: 80%;"/>
                    <br>
                    @if($item['profile']['is_private'] === false)<span class="badge" style="background-color: #6768fc; color: #FFFFFF;">Public</span>@else<span class="badge badge-danger">Private</span>@endif
                    @if($item['profile']['is_verified'] === false)<span class="badge" style="background-color: #03ae51; color: #FFFFFF;">Verified</span>@else<span class="badge badge-warning">Not Verified</span>@endif
                </div>
                <div class="col-10">
                    <table width="100%">
                        <tr>
                            <th width="100%">
                                <h2 style="font-weight: bold">{{$item['profile']['full_name']}}</h2>
                                <h6>{{$item['profile']['biography']}}</h6>
                            </th>
                        </tr>
                        <tr>
                            <th width="100%">
                                <h6 style="margin-top: 5px; margin-bottom: 5px;">
                                    <span class="badge" style="background-color: #193B83; color: #FFFFFF;">Facebook</span>
                                    / {{$item['profile']['username']}}
                                </h6>
                            </th>
                        </tr>
                        <tr>
                            <th width="100%">
                                <h6 style="margin-top: 5px; margin-bottom: 5px;">
                                    <span style="font-weight: bold">{{$item['profile']['fans']}}</span> Fans,
                                    <span style="font-weight: bold">{{number_format($item['profile']['post'], 0, ',', '.')}}</span> Post,
                                    <span style="font-weight: bold">{{number_format($item['profile']['like'], 0, ',', '.')}}</span> Likes
                                    (update: {{$item['profile']['date']}})
                                </h6>
                            </th>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Periode</h4>
                </div>
                <div class="col-sm-4">
                    <label class="label-control" for="start_date">Start Date: </label>
                    <input type="date" id="start_date" name="start_date" class="form-control" value="{{$item['date']['post']['first']}}">
                </div>
                <div class="col-sm-4">
                    <label class="label-control" for="end_date">End Date: </label>
                    <input type="date" id="end_date" name="end_date" class="form-control" value="{{$item['date']['post']['last']}}">
                </div>
                <div class="col-sm-4">
                    <label class="label-control" for="end_date">&nbsp;</label>
                    <button class="btn search-result btn-primary m-1" style="width: 100%" onclick="filterDate()"><i class="ft-filter"></i> Filter</button>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-3">
                    <div class="card gradient-blackberry">
                        <div class="card-content">
                          <div class="card-body pt-2 pb-2">
                                <div class="media">
                                    <div class="media-body white text-left">
                                        <h5 class="font-medium-4 mb-0" id="stat_fans"></h5>
                                        <span>Fans</span>
                                    </div>
                                    <div class="media-right white text-right">
                                        <i class="ft-user-plus font-medium-4"></i>
                                    </div>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card gradient-ibiza-sunset">
                        <div class="card-content">
                          <div class="card-body pt-2 pb-2">
                                <div class="media">
                                    <div class="media-body white text-left">
                                        <h5 class="font-medium-4 mb-0" id="stat_posts"></h5>
                                        <span>Posts</span>
                                    </div>
                                    <div class="media-right white text-right">
                                        <i class="ft-file font-medium-4"></i>
                                    </div>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card gradient-green-tea">
                        <div class="card-content">
                          <div class="card-body pt-2 pb-2">
                                <div class="media">
                                    <div class="media-body white text-left">
                                        <h5 class="font-medium-4 mb-0" id="stat_likes"></h5>
                                        <span>Likes</span>
                                    </div>
                                    <div class="media-right white text-right">
                                        <i class="ft-thumbs-up font-medium-4"></i>
                                    </div>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card gradient-pomegranate">
                        <div class="card-content">
                          <div class="card-body pt-2 pb-2">
                                <div class="media">
                                    <div class="media-body white text-left">
                                        <h5 class="font-medium-4 mb-0" id="stat_comments"></h5>
                                        <span>Comments</span>
                                    </div>
                                    <div class="media-right white text-right">
                                        <i class="ft-message-square font-medium-4"></i>
                                    </div>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Total Fans</h4>
                </div>
                <div class="col-sm-12 mt-2 p-2" style="display: none;">
                    <div class="card-content" style="text-align: center;">
                        <canvas id="chart-total-followers" height="150"></canvas>
                    </div>
                </div>
                <div class="col-sm-12">No Data</div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Growth of Total Fans</h4>
                </div>
                <div class="col-sm-9 mt-2 p-2" style="display: none;">
                    <canvas id="chart-growth-followers" height="150"></canvas>
                </div>
                <div class="col-sm-3 mt-2 p-2" style="display: none;">
                    <h5 style="font-weight: bolder;">Total Fans</h5>
                    <h3 style="color: #193B83;" id="growth_followers_total"></h3>
                    <h5 style="font-weight: bolder;" class="mt-3">Total Change in Fans</h5>
                    <h3 style="color: #193B83;" id="growth_followers_change"></h3>
                    <h5 style="font-weight: bolder;" class="mt-3">Max Change of Fans on</h5>
                    <h3 style="color: #193B83;" id="growth_followers_max"></h3>
                    <span id="growth_followers_max_date"></span>
                    <h5 style="font-weight: bolder;" class="mt-3">Average Change per Day</h5>
                    <h3 style="color: #193B83;" id="growth_followers_average"></h3>
                </div>
                <div class="col-sm-12">No Data</div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Number of Page Posts</h4>
                </div>
                <div class="col-sm-9 mt-2 p-2">
                    <canvas id="chart-number-posts" height="150"></canvas>
                </div>
                <div class="col-sm-3 mt-2 p-2">
                    <h5 style="font-weight: bolder;">Sum of Profile Posts</h5>
                    <h3 style="color: #193B83;" id="number_post_total"></h3>
                    <h5 style="font-weight: bolder;" class="mt-3">Average Profile Post per Day</h5>
                    <h3 style="color: #193B83;" id="number_post_average"></h3>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Distribution of Post Types</h4>
                </div>
                <div class="col-sm-4 mt-2 p-2">
                    <canvas id="chart-distribution-posts" height="200"></canvas>
                </div>
                <div class="col-sm-8 mt-2 p-2">
                    <table class="table" style="border: solid 2px darkgray;">
                        <tr style="background-color: lightgrey;">
                            <th></th>
                            <th>Count</th>
                            <th>Share</th>
                        </tr>
                        <tr>
                            <td><span style="background-color: #193B83;">&nbsp;&nbsp;&nbsp;</span> <i class="ft-camera mr-2 ml-2"></i> Photos</td>
                            <td id="distribution_post_photos_number">0</td>
                            <td id="distribution_post_photos_percen">0%</td>
                        </tr>
                        <tr>
                            <td><span style="background-color: #036AE3;">&nbsp;&nbsp;&nbsp;</span> <i class="ft-video mr-2 ml-2"></i> Videos</td>
                            <td id="distribution_post_videos_number">0</td>
                            <td id="distribution_post_videos_percen">0%</td>
                        </tr>
                        <tr>
                            <td><span style="background-color: #6eb2ff;">&nbsp;&nbsp;&nbsp;</span> <i class="ft-image mr-2 ml-2"></i>Statuses</td>
                            <td id="distribution_post_text_number">0</td>
                            <td id="distribution_post_text_percen">0%</td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Number of Page Post with Post Types</h4>
                </div>
                <canvas id="chart-number-posts-type" height="150"></canvas>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Average Interaction per Post</h4>
                </div>
                <div class="col-sm-9 mt-2 p-2">
                    <canvas id="chart-average-interaction-post" height="150"></canvas>
                </div>
                <div class="col-sm-3 mt-2 p-2">
                    <h6 style="font-weight: bolder;" class="mt-3">Max Interactions per Post on</h6>
                    <h3 style="color: #193B83;" id="average_interaction_post_max"></h3>
                    <span id="average_interaction_post_max_date"></span>
                    <h6 style="font-weight: bolder;" class="mt-3">Min Interactions per Post on</h6>
                    <h3 style="color: #193B83;" id="average_interaction_post_min"></h3>
                    <span id="average_interaction_post_min_date"></span>
                    <h6 style="font-weight: bolder;" class="mt-3">Average Interactions per Post on</h6>
                    <h3 style="color: #193B83;" id="average_interaction_post_average"></h3>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Distribution of Interactions</h4>
                </div>
                <div class="col-sm-4 mt-2 p-2">
                    <canvas id="chart-distribution-interactions" height="200"></canvas>
                </div>
                <div class="col-sm-8 mt-2 p-2">
                    <table class="table" style="border: solid 2px darkgray;">
                        <tr style="background-color: lightgrey;">
                            <th></th>
                            <th>Count</th>
                            <th>Share</th>
                        </tr>
                        <tr>
                            <td><span style="background-color: #193B83;">&nbsp;&nbsp;&nbsp;</span> <i class="ft-thumbs-up mr-2 ml-2"></i> Reactions</td>
                            <td id="distribution_interactions_likes_number">0</td>
                            <td id="distribution_interactions_likes_percen">0%</td>
                        </tr>
                        <tr>
                            <td><span style="background-color: #036AE3;">&nbsp;&nbsp;&nbsp;</span> <i class="ft-message-square mr-2 ml-2"></i> Comments</td>
                            <td id="distribution_interactions_comments_number">0</td>
                            <td id="distribution_interactions_comments_percen">0%</td>
                        </tr>
                        <tr>
                            <td><span style="background-color: #6eb2ff;">&nbsp;&nbsp;&nbsp;</span> <i class="ft-message-square mr-2 ml-2"></i> Shares</td>
                            <td id="distribution_interactions_shares_number">0</td>
                            <td id="distribution_interactions_shares_percen">0%</td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Evolution of Interactions</h4>
                </div>
                <div class="col-sm-9 mt-2 p-2">
                    <canvas id="chart-evolution-interactions" height="200"></canvas>
                </div>
                <div class="col-sm-3 mt-2 p-2">
                    <h5 style="font-weight: bolder;" class="mt-3">Max Interactions on</h5>
                    <h3 style="color: #193B83;" id="evolution_interaction_max"></h3>
                    <span id="evolution_interaction_max_date"></span>
                    <h5 style="font-weight: bolder;" class="mt-3">Average Interactions per Day</h5>
                    <h3 style="color: #193B83;" id="evolution_interaction_average"></h3>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Number of Reactions</h4>
                </div>
                <canvas id="chart-number-reactions" height="150" style="display: none;"></canvas>
                <div class="col-sm-12">No Data</div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Most Engaging Post Types</h4>
                </div>
                <canvas id="chart-engaging-posts-type" height="150"></canvas>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">User Activity</h4>
                </div>
                <canvas id="chart-user-activity" height="150"></canvas>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Promoted Post Detection Overview</h4>
                </div>
                <div class="col-sm-12">No Data</div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Most Engaging Post Overview</h4>
                </div>
                <div class="col-sm-12 mt-2 p-2">
                    <div class="row m-0 p-0" id="limitpost">
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('pagecss')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.3/css/bootstrap-select.min.css">
@endsection
@section('pagejs')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script>

    // var element_total_followers = document.getElementById('chart-total-followers').getContext('2d');
    // var element_growth_followers = document.getElementById('chart-growth-followers').getContext('2d');
    var element_number_posts = document.getElementById('chart-number-posts').getContext('2d');
    var element_distribution_posts = document.getElementById('chart-distribution-posts').getContext('2d');
    var element_number_posts_type = document.getElementById('chart-number-posts-type').getContext('2d');
    var element_average_interaction_post = document.getElementById('chart-average-interaction-post').getContext('2d');
    var element_distribution_interactions = document.getElementById('chart-distribution-interactions').getContext('2d');
    var element_evolution_interactions = document.getElementById('chart-evolution-interactions').getContext('2d');
    // var element_number_reactions = document.getElementById('chart-number-reactions').getContext('2d');
    var element_engaging_posts_type = document.getElementById('chart-engaging-posts-type').getContext('2d');
    var element_user_activity = document.getElementById('chart-user-activity').getContext('2d');

    // var ctx_total_followers = new Chart(element_total_followers, {});
    // var ctx_growth_followers = new Chart(element_growth_followers, {});
    var ctx_number_posts = new Chart(element_number_posts, {});
    var ctx_distribution_posts = new Chart(element_distribution_posts, {});
    var ctx_number_posts_type = new Chart(element_number_posts_type, {});
    var ctx_average_interaction_post = new Chart(element_average_interaction_post, {});
    var ctx_distribution_interactions = new Chart(element_distribution_interactions, {});
    var ctx_evolution_interactions = new Chart(element_evolution_interactions, {});
    // var ctx_number_reactions = new Chart(element_number_reactions, {});
    var ctx_engaging_posts_type= new Chart(element_engaging_posts_type, {});
    var ctx_user_activity = new Chart(element_user_activity, {});

    $(document).ready(function() {
        get_stat();
        // get_total_followers();
        // get_growth_followers();
        get_number_posts();
        get_distribution_posts();
        get_number_posts_type();
        get_average_interaction_post();
        get_distribution_interactions();
        get_evolution_interactions();
        // get_number_reactions();
        get_engaging_posts_type();
        get_user_activity();
        get_limit_post();
    });

    function filterDate() {
        var start_date = document.getElementById("start_date").value;
        var end_date = document.getElementById("end_date").value;
        // ctx_total_followers.destroy();
        // ctx_growth_followers.destroy();
        ctx_number_posts.destroy();
        ctx_distribution_posts.destroy();
        ctx_number_posts_type.destroy();
        ctx_average_interaction_post.destroy();
        ctx_distribution_interactions.destroy();
        ctx_evolution_interactions.destroy();
        // ctx_number_reactions.destroy();
        ctx_engaging_posts_type.destroy();
        ctx_user_activity.destroy();
        get_stat(start_date, end_date);
        // get_total_followers(start_date, end_date);
        // get_growth_followers(start_date, end_date);
        get_number_posts(start_date, end_date);
        get_distribution_posts(start_date, end_date);
        get_number_posts_type(start_date, end_date);
        get_average_interaction_post(start_date, end_date);
        get_distribution_interactions(start_date, end_date);
        get_evolution_interactions(start_date, end_date);
        // get_number_reactions(start_date, end_date);
        get_engaging_posts_type(start_date, end_date);
        get_user_activity(start_date, end_date);
        get_limit_post(start_date, end_date);
    }

    function get_stat(start_date="{{ $item['date']['profile']['first'] }}", end_date="{{ $item['date']['profile']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/facebook/get_stat/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                document.getElementById("stat_fans").innerHTML = "- <span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(-)" +"</span>";
                if (result['post_growth'] > 0) {
                    document.getElementById("stat_posts").innerHTML = result['post_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(+" + result['post_growth'].toLocaleString('id') + ")" +"</span>";
                } else {
                    document.getElementById("stat_posts").innerHTML = result['post_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(" + result['post_growth'].toLocaleString('id') + ")" +"</span>";
                }
                if (result['like_growth'] > 0) {
                    document.getElementById("stat_likes").innerHTML = result['like_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(+" + result['like_growth'].toLocaleString('id') + ")" +"</span>";
                } else {
                    document.getElementById("stat_likes").innerHTML = result['like_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(" + result['like_growth'].toLocaleString('id') + ")" +"</span>";
                }
                if (result['comment_growth'] > 0) {
                    document.getElementById("stat_comments").innerHTML = result['comment_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(+" + result['comment_growth'].toLocaleString('id') + ")" +"</span>";
                } else {
                    document.getElementById("stat_comments").innerHTML = result['comment_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(" + result['comment_growth'].toLocaleString('id') + ")" +"</span>";
                }
            }
        });
    }

    function get_total_followers(start_date="{{ $item['date']['profile']['first'] }}", end_date="{{ $item['date']['profile']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/facebook/get_total_followers/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_total_followers = [];
                var data_total_followers = [];
                result.forEach(element => {
                    label_total_followers.push(element.date)
                    data_total_followers.push(element.followed_by)
                });

                ctx_total_followers = new Chart(element_total_followers, {
                    type: 'line',
                    data: {
                        labels: label_total_followers,
                        datasets: [
                            {
                                label: 'Followers',
                                backgroundColor: '#9132b7',
                                borderColor: '#193B83',
                                fill: false,
                                data: data_total_followers,
                            },
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: false
                        },
                        bezierCurve : false,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: false
                                }
                            }]
                        },
                        elements: {
                            line: {
                                tension: 0
                            }
                        }
                    }
                });
            }
        });
    }

    function get_growth_followers(start_date="{{ $item['date']['profile']['first'] }}", end_date="{{ $item['date']['profile']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/facebook/get_growth_followers/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_growth_followers = [];
                var data_growth_followers = [];
                result['data'].forEach(element => {
                    label_growth_followers.push(element.date)
                    data_growth_followers.push(element.followed_by)
                });

                document.getElementById("growth_followers_total").innerHTML = result['total'].toLocaleString('id');
                document.getElementById("growth_followers_change").innerHTML = result['change'].toLocaleString('id');
                document.getElementById("growth_followers_max").innerHTML = result['max'].toLocaleString('id');
                document.getElementById("growth_followers_max_date").innerHTML = result['max_date'];
                document.getElementById("growth_followers_average").innerHTML = result['average'].toLocaleString('id');

                ctx_growth_followers = new Chart(element_growth_followers, {
                    type: 'bar',
                    data: {
                        labels: label_growth_followers,
                        datasets: [
                            {
                                label: 'Followers',
                                backgroundColor: '#193B83',
                                borderColor: '#9132b7',
                                fill: false,
                                data: data_growth_followers,
                            },
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: false
                        },
                        bezierCurve : false,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: false
                                }
                            }]
                        },
                        elements: {
                            line: {
                                tension: 0
                            }
                        }
                    }
                });
            }
        });
    }

    function get_number_posts(start_date="{{ $item['date']['post']['first'] }}", end_date="{{ $item['date']['post']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/facebook/get_number_posts/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_number_posts = [];
                var data_number_posts = [];
                result['data'].forEach(element => {
                    label_number_posts.push(element.date)
                    data_number_posts.push(element.number_post)
                });

                document.getElementById("number_post_total").innerHTML = result['total'].toLocaleString('id');
                document.getElementById("number_post_average").innerHTML = result['average'].toLocaleString('id');

                ctx_number_posts = new Chart(element_number_posts, {
                    type: 'bar',
                    data: {
                        labels: label_number_posts,
                        datasets: [
                            {
                                label: 'Post',
                                backgroundColor: '#193B83',
                                data: data_number_posts,
                            },
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: false
                        },
                        bezierCurve : false
                    }
                });
            }
        });
    }

    function get_distribution_posts(start_date="{{ $item['date']['post']['first'] }}", end_date="{{ $item['date']['post']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/facebook/get_distribution_posts/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_distribution_post = [];
                label_distribution_post.push('Image');
                label_distribution_post.push('Video');
                label_distribution_post.push('Statuses');

                var data_distribution_post = [];
                data_distribution_post.push(result['types_image'])
                data_distribution_post.push(result['types_video'])
                data_distribution_post.push(result['types_text'])
                var types_total = result['types_image'] + result['types_video'] + result['types_text'];

                document.getElementById("distribution_post_text_number").innerHTML = result['types_text'].toLocaleString('id');
                document.getElementById("distribution_post_text_percen").innerHTML = ((result['types_text'] / types_total) * 100).toLocaleString('id') + "%";
                document.getElementById("distribution_post_photos_number").innerHTML = result['types_image'].toLocaleString('id');
                document.getElementById("distribution_post_photos_percen").innerHTML = ((result['types_image'] / types_total) * 100).toLocaleString('id') + "%";
                document.getElementById("distribution_post_videos_number").innerHTML = result['types_video'].toLocaleString('id');
                document.getElementById("distribution_post_videos_percen").innerHTML = ((result['types_video'] / types_total) * 100).toLocaleString('id') + "%";

                var ctx = document.getElementById('chart-distribution-posts').getContext('2d');
                var chart = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: label_distribution_post,
                        datasets: [{
                            label: 'Distribution Post Type',
                            backgroundColor: [
                                '#193B83',
                                '#036AE3',
                                '#6eb2ff'
                            ],
                            data: data_distribution_post,
                            name: label_distribution_post,
                        }],
                    },
                    options: {
                        responsive: true,
                        events: false,
                        legend: {
                            display: false,
                            position: 'right'
                        },
                        animation: {
                            // duration: 500,
                            // easing: "easeOutQuart",
                            onComplete: function () {
                            var ctx = this.chart.ctx;
                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function (dataset) {

                                for (var i = 0; i < dataset.data.length; i++) {
                                var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                    total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                    mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
                                    start_angle = model.startAngle,
                                    end_angle = model.endAngle,
                                    mid_angle = start_angle + (end_angle - start_angle)/2;

                                var x = mid_radius * Math.cos(mid_angle);
                                var y = mid_radius * Math.sin(mid_angle);

                                ctx.fillStyle = '#fff';
                                if (i == 3){
                                    ctx.fillStyle = '#444';
                                }
                                var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
                                // if(dataset.data[i] != 0 && dataset._meta[0].data[i].hidden != true) {
                                    ctx.fillText(dataset.name[i] + ' (' + dataset.data[i] + ')', model.x + x, model.y + y);
                                    ctx.fillText(percent, model.x + x, model.y + y + 15);
                                // }
                                }
                            });
                            }
                        }
                    }
                });
            }
        });
    }

    function get_number_posts_type(start_date="{{ $item['date']['post']['first'] }}", end_date="{{ $item['date']['post']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/facebook/get_number_posts_type/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_number_posts_type = [];
                var data_number_posts_type = [];
                data_number_posts_type.push([]);
                data_number_posts_type.push([]);
                data_number_posts_type.push([]);

                result.forEach(element => {
                    label_number_posts_type.push(element.date)
                    data_number_posts_type[0].push(element.types_image)
                    data_number_posts_type[1].push(element.types_video)
                    data_number_posts_type[2].push(element.types_text)
                });

                ctx_number_posts_type = new Chart(element_number_posts_type, {
                    type: 'bar',
                    data: {
                        labels: label_number_posts_type,
                        datasets: [
                            {
                                label: 'Photos',
                                backgroundColor: '#193B83',
                                data: data_number_posts_type[0],
                            },
                            {
                                label: 'Videos',
                                backgroundColor: '#036AE3',
                                data: data_number_posts_type[1],
                            },
                            {
                                label: 'Statuses',
                                backgroundColor: '#6eb2ff',
                                data: data_number_posts_type[2],
                            },
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: true,
                            position: 'bottom'
                        },
                        scales: {
                            xAxes: [{
                                stacked: true,
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        },
                        bezierCurve : false
                    }
                });
            }
        });
    }

    function get_average_interaction_post(start_date="{{ $item['date']['post']['first'] }}", end_date="{{ $item['date']['post']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/facebook/get_average_interaction_post/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_average_interaction_post = [];
                var data_average_interaction_post = [];
                result['data'].forEach(element => {
                    label_average_interaction_post.push(element.date)
                    data_average_interaction_post.push(element.interaction)
                });

                document.getElementById("average_interaction_post_max").innerHTML = result['max'].toLocaleString('id');
                document.getElementById("average_interaction_post_max_date").innerHTML = result['max_date'];
                document.getElementById("average_interaction_post_min").innerHTML = result['min'].toLocaleString('id');
                document.getElementById("average_interaction_post_min_date").innerHTML = result['min_date'];
                document.getElementById("average_interaction_post_average").innerHTML = result['average'].toLocaleString('id');

                ctx_average_interaction_post = new Chart(element_average_interaction_post, {
                    type: 'bar',
                    data: {
                        labels: label_average_interaction_post,
                        datasets: [
                            {
                                label: 'Post',
                                backgroundColor: '#193B83',
                                data: data_average_interaction_post,
                            },
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: false
                        },
                        bezierCurve : false
                    }
                });
            }
        });
    }

    function get_distribution_interactions(start_date="{{ $item['date']['post']['first'] }}", end_date="{{ $item['date']['post']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/facebook/get_distribution_interactions/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_distribution_interactions = [];
                label_distribution_interactions.push('Likes');
                label_distribution_interactions.push('Comments');
                label_distribution_interactions.push('Shares');

                var data_distribution_interactions = [];
                data_distribution_interactions.push(result['like'])
                data_distribution_interactions.push(result['comment'])
                data_distribution_interactions.push(result['share'])
                var interaction_total = result['like'] + result['comment'] + result['share'];

                document.getElementById("distribution_interactions_likes_number").innerHTML = result['like'].toLocaleString('id');
                document.getElementById("distribution_interactions_likes_percen").innerHTML = ((result['like'] / interaction_total) * 100).toLocaleString('id') + "%";
                document.getElementById("distribution_interactions_comments_number").innerHTML = result['comment'].toLocaleString('id');
                document.getElementById("distribution_interactions_comments_percen").innerHTML = ((result['comment'] / interaction_total) * 100).toLocaleString('id') + "%";
                document.getElementById("distribution_interactions_shares_number").innerHTML = result['share'].toLocaleString('id');
                document.getElementById("distribution_interactions_shares_percen").innerHTML = ((result['share'] / interaction_total) * 100).toLocaleString('id') + "%";

                var ctx = document.getElementById('chart-distribution-interactions').getContext('2d');
                var chart = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: label_distribution_interactions,
                        datasets: [{
                            label: 'Distribution Interactions',
                            backgroundColor: [
                                '#193B83',
                                '#036AE3',
                                '#6eb2ff'
                            ],
                            data: data_distribution_interactions,
                            name: label_distribution_interactions,
                        }],
                    },
                    options: {
                        responsive: true,
                        events: false,
                        legend: {
                            display: false,
                            position: 'right'
                        },
                        animation: {
                            // duration: 500,
                            // easing: "easeOutQuart",
                            onComplete: function () {
                            var ctx = this.chart.ctx;
                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function (dataset) {

                                for (var i = 0; i < dataset.data.length; i++) {
                                var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                    total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                    mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
                                    start_angle = model.startAngle,
                                    end_angle = model.endAngle,
                                    mid_angle = start_angle + (end_angle - start_angle)/2;

                                var x = mid_radius * Math.cos(mid_angle);
                                var y = mid_radius * Math.sin(mid_angle);

                                ctx.fillStyle = '#fff';
                                if (i == 3){
                                    ctx.fillStyle = '#444';
                                }
                                var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
                                // if(dataset.data[i] != 0 && dataset._meta[0].data[i].hidden != true) {
                                    ctx.fillText(dataset.name[i] + ' (' + dataset.data[i] + ')', model.x + x, model.y + y);
                                    ctx.fillText(percent, model.x + x, model.y + y + 15);
                                // }
                                }
                            });
                            }
                        }
                    }
                });
            }
        });
    }

    function get_evolution_interactions(start_date="{{ $item['date']['post']['first'] }}", end_date="{{ $item['date']['post']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/facebook/get_evolution_interactions/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_evolution_interactions = [];
                var data_evolution_interactions = [];
                data_evolution_interactions.push([]);
                data_evolution_interactions.push([]);
                data_evolution_interactions.push([]);

                result['data'].forEach(element => {
                    label_evolution_interactions.push(element.date)
                    data_evolution_interactions[0].push(element.like)
                    data_evolution_interactions[1].push(element.comment)
                    data_evolution_interactions[2].push(element.share)
                });

                document.getElementById("evolution_interaction_max").innerHTML = result['max'].toLocaleString('id');
                document.getElementById("evolution_interaction_max_date").innerHTML = result['max_date'];
                document.getElementById("evolution_interaction_average").innerHTML = result['average'].toLocaleString('id');

                ctx_evolution_interactions = new Chart(element_evolution_interactions, {
                    type: 'bar',
                    data: {
                        labels: label_evolution_interactions,
                        datasets: [
                            {
                                label: 'Likes',
                                backgroundColor: '#193B83',
                                data: data_evolution_interactions[0],
                            },
                            {
                                label: 'Comments',
                                backgroundColor: '#036AE3',
                                data: data_evolution_interactions[1],
                            },
                            {
                                label: 'Shares',
                                backgroundColor: '#6eb2ff',
                                data: data_evolution_interactions[1],
                            },
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: true,
                            position: 'bottom'
                        },
                        scales: {
                            xAxes: [{
                                stacked: true,
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        },
                        bezierCurve : false
                    }
                });
            }
        });
    }

    function get_number_reactions(start_date="{{ $item['date']['post']['first'] }}", end_date="{{ $item['date']['post']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/facebook/get_number_reactions/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_evolution_interactions = [];
                var data_evolution_interactions = [];
                data_evolution_interactions.push([]);
                data_evolution_interactions.push([]);
                data_evolution_interactions.push([]);
                data_evolution_interactions.push([]);
                data_evolution_interactions.push([]);
                data_evolution_interactions.push([]);

                result['data'].forEach(element => {
                    label_evolution_interactions.push(element.date)
                    data_evolution_interactions[0].push(element.like)
                    data_evolution_interactions[1].push(element.haha)
                    data_evolution_interactions[2].push(element.love)
                    data_evolution_interactions[3].push(element.sorry)
                    data_evolution_interactions[4].push(element.support)
                    data_evolution_interactions[5].push(element.wow)
                });

                ctx_evolution_interactions = new Chart(element_evolution_interactions, {
                    type: 'bar',
                    data: {
                        labels: label_evolution_interactions,
                        datasets: [
                            {
                                label: 'Like',
                                backgroundColor: '#193B83',
                                data: data_evolution_interactions[0],
                            },
                            {
                                label: 'Haha',
                                backgroundColor: '#0e45b8',
                                data: data_evolution_interactions[1],
                            },
                            {
                                label: 'Love',
                                backgroundColor: '#135af0',
                                data: data_evolution_interactions[2],
                            },
                            {
                                label: 'Sorry',
                                backgroundColor: '#5286f3',
                                data: data_evolution_interactions[3],
                            },
                            {
                                label: 'Support',
                                backgroundColor: '#b3c9f6',
                                data: data_evolution_interactions[4],
                            },
                            {
                                label: 'Wow',
                                backgroundColor: '#9fabc4',
                                data: data_evolution_interactions[5],
                            },
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: true,
                            position: 'bottom'
                        },
                        scales: {
                            xAxes: [{
                                stacked: true,
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        },
                        bezierCurve : false
                    }
                });
            }
        });
    }

    function get_engaging_posts_type(start_date="{{ $item['date']['post']['first'] }}", end_date="{{ $item['date']['post']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/facebook/get_engaging_posts_type/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_engaging_posts_type = [];
                var data_engaging_posts_type = [];

                label_engaging_posts_type.push('Photos');
                label_engaging_posts_type.push('Videos');
                label_engaging_posts_type.push('Statuses');

                data_engaging_posts_type.push(result['image']);
                data_engaging_posts_type.push(result['video']);
                data_engaging_posts_type.push(result['text']);

                ctx_engaging_posts_type = new Chart(element_engaging_posts_type, {
                    type: 'bar',
                    data: {
                        labels: label_engaging_posts_type,
                        datasets: [
                            {
                                label: 'Post Type',
                                backgroundColor: '#193B83',
                                data: data_engaging_posts_type,
                            }
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: true,
                            position: 'bottom'
                        },
                        scales: {
                            xAxes: [{
                                stacked: true,
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        },
                        bezierCurve : false
                    }
                });
            }
        });
    }

    function get_user_activity(start_date="{{ $item['date']['post']['first'] }}", end_date="{{ $item['date']['post']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/facebook/get_user_activity/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_user_activity = 'User Activity';
                var data_user_activity = [];

                result.forEach(element => {
                    var temp = {};
                    temp['x'] = (new Date(element['x'])).getTime();
                    temp['y'] = element['y']
                    temp['r'] = element['r'] * 3;
                    data_user_activity.push(temp)
                });

                ctx_user_activity = new Chart(element_user_activity, {
                    type: 'bubble',
                    data: {
                        labels: label_user_activity,
                        datasets: [
                            {
                                label: label_user_activity,
                                data: data_user_activity,
                                backgroundColor: '#193B83'
                            }
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: true,
                            position: 'bottom'
                        },
                        scales: {
                            xAxes: [{
                                stacked: true,
                                ticks: {
                                    min: (new Date(data_user_activity[0]['x'] - (1 * 86400000))).getTime(),
                                    max: (new Date(data_user_activity[data_user_activity.length - 1]['x'] + (1 * 86400000))).getTime(),
                                    callback: function(value, index, values) {
                                        return new Date(value).toLocaleDateString("en-US")
                                    }
                                },
                            }],
                            yAxes: [{
                                stacked: true,
                                ticks: {
                                    min: 0,
                                    max: 24,
                                    stepSize: 1,
                                    callback: function(value, index, values) {
                                        if(value < 10) {
                                            return '0'+value.toString()+':00';
                                        } else {
                                            return value.toString()+':00';
                                        }
                                    }
                                },
                            }]
                        },
                        tooltips: {
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    var label = data.datasets[tooltipItem.datasetIndex].label;
                                    var val = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                    if(val.y < 10) {
                                        return new Date(val.x).toLocaleDateString("en-US") +  " " + '0'+ val.y.toString()+':00' + " - " + val.r / 3 + " Aktivitas";
                                    } else {
                                        return new Date(val.x).toLocaleDateString("en-US") +  " " + val.y.toString()+':00'  + " - " + val.r / 3 + " Aktivitas";
                                    }
                                }
                            }
                        }
                    }
                });

            }
        });
    }

    function get_limit_post(start_date="{{ $item['date']['post']['first'] }}", end_date="{{ $item['date']['post']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/facebook/get_limit_post/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){

                var html = ``;
                document.getElementById("limitpost").innerHTML = html;
                result.forEach(element => {

                    html += `
                    <div class="col-sm-5 ml-4 mb-2" style="border: solid 2px lightgrey; border-radius: 15px; height: fit-content;">
                        <div class="row" style="margin-top: 5px; margin-bottom: 5px; border-bottom: solid 2px lightgrey;">
                            <div class="col-2">
                                <img src="{{ $item['profile']['profile_pic_url'] }}" style="margin-bottom: 5px; width: 100%;"/>
                            </div>
                            <div class="col-10">
                                <h6 style="font-weight: bold">`;
                                if (element['image'] !== null) {
                                    html += `<i class="ft-camera mr-2 ml-2"></i>`;
                                }
                                else if (element['video'] !== null) {
                                    html += `<i class="ft-video mr-2 ml-2"></i>`;
                                }
                                else {
                                    html += `<i class="ft-file-text mr-2 ml-2"></i>`;
                                }
                                html += `{{ $item['profile']['full_name'] }}
                                <h6>
                                    <span class="badge" style="background-color: #193B83; color: #FFFFFF; padding: 3px;">Facebook</span>
                                    / {{$item['profile']['username']}}
                                    <i class="ml-4">` + element['datetimes'] + `</i>
                                </h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 p-2" style="display: grid;">
                                <span class="p-2">` + element['text'] + `</span>`;
                                if (element['image'] !== null) {
                                    html += `<img class="p-2" src="` + element['image'] + `" style="width: 100%;"/>`;
                                }
                                else if (element['video'] !== null) {
                                    html += `<video class="p-2" controls src="` + element['video'] + `" style="width: 100%;"></video>`;
                                }
                                html += `
                                <span class="ml-2">
                                </span>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 5px; margin-bottom: 5px; border-top: solid 2px lightgrey;">
                            <div class="col-sm-3 p-1" style="border-right: solid 1px lightgrey;">
                                <b>` + (element['likes'] + element['comments'] + element['shares']).toLocaleString('id') + `</b>
                                <br>Interactions
                            </div>
                            <div class="col-sm-2 p-1" style="border-right: solid 1px lightgrey;">
                                <b>` + element['likes'].toLocaleString('id') + `</b>
                                <br>Likes
                            </div>
                            <div class="col-sm-2 p-1" style="border-right: solid 1px lightgrey;">
                                <b>` + element['comments'].toLocaleString('id') + `</b>
                                <br>Comments
                            </div>
                            <div class="col-sm-2 p-1" style="border-right: solid 1px lightgrey;">
                                <b>` + element['shares'].toLocaleString('id') + `</b>
                                <br>Shares
                            </div>
                            <div class="col-sm-3 p-1">
                                <b>` + ((element['likes'] + element['comments'] + element['shares'])/1000).toLocaleString('id') + `</b>
                                <br>Interactions per 1k Followers
                            </div>
                        </div>
                    </div>
                    `;
                });

                document.getElementById("limitpost").innerHTML = html;

            }
        });
    }

</script>
@endsection
