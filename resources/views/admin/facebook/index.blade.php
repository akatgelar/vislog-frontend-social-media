@extends('admin.layouts.app')

@section('pagetitle')
    <title>Facebook Dashboard</title>
@endsection

@section('content')
        <!-- BEGIN : Main Content-->
        <div class="main-content">
          <div class="content-wrapper"><!-- DOM - jQuery events table -->
<section id="browse-table">
  <div class="row">
    <div class="col-12">
      @if(Session::has('message'))
      <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ ucfirst(Session::get('message')) }}</p>
      @endif
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Facebook Dashboard</h4>
        </div>
        <div class="card-content ">
          <div class="card-body card-dashboard table-responsive">
            <table class="table browse-table">
              <thead>
                <tr>
                  <th class="profile_pic">Profile Picture</th>
                  <th class="username">Username</th>
                  <th class="last_update">Last Update</th>
                  <th style="white-space: nowrap;">Action</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- File export table -->

          </div>
        </div>
@endsection
@section('pagecss')
<link rel="stylesheet" type="text/css" href="{{ asset('') }}app-assets/vendors/css/tables/datatable/datatables.min.css">
@endsection
@section('pagejs')
<script src="{{ asset('') }}app-assets/vendors/js/datatable/datatables.min.js" type="text/javascript"></script>
<script src="{{ asset('') }}app-assets/vendors/js/datatable/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="{{ asset('') }}app-assets/vendors/js/datatable/buttons.flash.min.js" type="text/javascript"></script>
<script src="{{ asset('') }}app-assets/vendors/js/datatable/jszip.min.js" type="text/javascript"></script>
<script src="{{ asset('') }}app-assets/vendors/js/datatable/pdfmake.min.js" type="text/javascript"></script>
<script src="{{ asset('') }}app-assets/vendors/js/datatable/vfs_fonts.js" type="text/javascript"></script>
<script src="{{ asset('') }}app-assets/vendors/js/datatable/buttons.html5.min.js" type="text/javascript"></script>
<script src="{{ asset('') }}app-assets/vendors/js/datatable/buttons.print.min.js" type="text/javascript"></script>
<script>
$(document).ready(function() {
    var resp = false;
    if(window.innerWidth <= 800) resp=true;

    $.fn.dataTable.ext.errMode = 'none';

    $('.browse-table').on( 'error.dt', function ( e, settings, techNote, message ) {
    console.log( 'DataTables error: ', message );
    } ) ;

    var table = $('.browse-table').DataTable({
        responsive: resp,
        processing: true,
        serverSide: true,
        ajax: '{!! url('admin/facebook/indexjson') !!}',
        columns: [
          { data: 'profile_pic', name: 'profile_pic' },
          { data: 'username', name: 'username' },
          { data: 'last_update', name: 'last_update' },
          { data: 'action', name: 'action' },
        ],
        dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
            "<'row'<'col-sm-12'B>>"+
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        buttons: [
            {
              text: '<i class="ft-plus"></i> Add New', className: 'buttons-add',
              action: function ( e, dt, node, config ) {
                  window.location = '{{ url('admin/facebook/create') }}'
              }
            },
            // { extend: 'colvis', text: 'Column' },
            // {
            //   extend: 'csv',
            //   text: 'CSV',
            //   className: 'buttons-csvall',
            //   action: function ( e, dt, node, config ) {
            //       window.location = '{{ url('admin/user/csvall') }}'
            //   }
            // }
        ],
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        columnDefs: [{
            targets: ['targetaudience','nsector','isector','ncategory','icategory','nadvertiser','iadvertiser',
            'iadvertiser_group','nproduct','iproduct','ncopy','icopy','nadstype','iadstype','tadstype','channel',
            'nprogramme','iprogramme','nlevel_1','nlevel_2','ilevel_1','ilevel_2','startdate','enddate','phone'],
            visible: false,
        },{
            targets: ['id','created_at','updated_at'],
            visible: false,
            searchable: false,
        } ]
    });
    $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel, .buttons-colvis, .buttons-csvall').addClass('btn btn-outline-primary mr-1');
    $('.buttons-add').addClass('btn mr-1');

});
</script>
@endsection
