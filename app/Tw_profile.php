<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Tw_profile extends Eloquent
{
    protected $guarded = ['id'];
    protected $table = 'tw_profile';

    protected $casts = [
        'date' => 'datetime',
        'created_at' => 'datetime',
    ];

}
