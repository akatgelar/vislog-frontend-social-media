@extends('admin.layouts.app')

@section('pagetitle')
    <title>Twitter Dashboard</title>
@endsection

@section('content')
    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-2">
                    <img src="{{ $item['profile']['profile_image_url'] }}" style="margin-bottom: 5px; width: 80%;"/>

                    @if($item['profile']['protected'] === false)<span class="badge" style="background-color: #6768fc; color: #FFFFFF;">Public</span>@else<span class="badge badge-danger" style="color: #FFFFFF;">Private</span>@endif
                    @if($item['profile']['verified'] === true)<span class="badge" style="background-color: #03ae51; color: #FFFFFF;">Verified</span>@else<span class="badge badge-warning" style="color: #FFFFFF;">Not Verified</span>@endif
                </div>
                <div class="col-10">
                    <table width="100%">
                        <tr>
                            <th width="100%">
                                <h2 style="font-weight: bold">{{$item['profile']['name']}}</h2>
                                <h6>{{$item['profile']['description']}}</h6>
                            </th>
                        </tr>
                        <tr>
                            <th width="100%">
                                <h6 style="margin-top: 5px; margin-bottom: 5px;">
                                    <span class="badge" style="background-color: #1da1f2; color: #FFFFFF;">Twitter</span>
                                    / {{$item['profile']['username']}}
                                </h6>
                            </th>
                        </tr>
                        <tr>
                            <th width="100%">
                                <h6 style="margin-top: 5px; margin-bottom: 5px;">
                                    <span style="font-weight: bold">{{number_format($item['profile']['followers_count'], 0, ',', '.')}}</span> Followers,
                                    <span style="font-weight: bold">{{number_format($item['profile']['following_count'], 0, ',', '.')}}</span> Following,
                                    <span style="font-weight: bold">{{number_format($item['profile']['tweet_count'], 0, ',', '.')}}</span> Tweets
                                    (update: {{$item['profile']['date']}})
                                </h6>
                            </th>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Periode</h4>
                </div>
                <div class="col-sm-4">
                    <label class="label-control" for="start_date">Start Date: </label>
                    <input type="date" id="start_date" name="start_date" class="form-control" value="{{$item['date']['post']['first']}}">
                </div>
                <div class="col-sm-4">
                    <label class="label-control" for="end_date">End Date: </label>
                    <input type="date" id="end_date" name="end_date" class="form-control" value="{{$item['date']['post']['last']}}">
                </div>
                <div class="col-sm-4">
                    <label class="label-control" for="end_date">&nbsp;</label>
                    <button class="btn search-result btn-primary m-1" style="width: 100%" onclick="filterDate()"><i class="ft-filter"></i> Filter</button>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-3">
                    <div class="card gradient-blackberry">
                        <div class="card-content">
                          <div class="card-body pt-2 pb-2">
                                <div class="media">
                                    <div class="media-body white text-left">
                                        <h5 class="font-medium-4 mb-0" id="stat_followers"></h5>
                                        <span>Followers</span>
                                    </div>
                                    <div class="media-right white text-right">
                                        <i class="ft-user-plus font-medium-4"></i>
                                    </div>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card gradient-ibiza-sunset">
                        <div class="card-content">
                          <div class="card-body pt-2 pb-2">
                                <div class="media">
                                    <div class="media-body white text-left">
                                        <h5 class="font-medium-4 mb-0" id="stat_following"></h5>
                                        <span>Following</span>
                                    </div>
                                    <div class="media-right white text-right">
                                        <i class="ft-user-check font-medium-4"></i>
                                    </div>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card gradient-green-tea">
                        <div class="card-content">
                          <div class="card-body pt-2 pb-2">
                                <div class="media">
                                    <div class="media-body white text-left">
                                        <h5 class="font-medium-4 mb-0" id="stat_posts"></h5>
                                        <span>Tweets</span>
                                    </div>
                                    <div class="media-right white text-right">
                                        <i class="ft-message-circle font-medium-4"></i>
                                    </div>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card gradient-pomegranate">
                        <div class="card-content">
                          <div class="card-body pt-2 pb-2">
                                <div class="media">
                                    <div class="media-body white text-left">
                                        <h5 class="font-medium-4 mb-0" id="stat_likes"></h5>
                                        <span>Likes</span>
                                    </div>
                                    <div class="media-right white text-right">
                                        <i class="ft-thumbs-up font-medium-4"></i>
                                    </div>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Total Followers</h4>
                </div>
                <div class="col-sm-12 mt-2 p-2">
                    <div class="card-content" style="text-align: center;">
                        <canvas id="chart-total-followers" height="150"></canvas>
                    </div>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Growth of Total Followers</h4>
                </div>
                <div class="col-sm-9 mt-2 p-2">
                    <canvas id="chart-growth-followers" height="150"></canvas>
                </div>
                <div class="col-sm-3 mt-2 p-2">
                    <h5 style="font-weight: bolder;">Total Followers</h5>
                    <h3 style="color: #1da1f2;" id="growth_followers_total"></h3>
                    <h5 style="font-weight: bolder;" class="mt-3">Total Change in Followers</h5>
                    <h3 style="color: #1da1f2;" id="growth_followers_change"></h3>
                    <h5 style="font-weight: bolder;" class="mt-3">Max Change of Followers on</h5>
                    <h3 style="color: #1da1f2;" id="growth_followers_max"></h3>
                    <span id="growth_followers_max_date"></span>
                    <h5 style="font-weight: bolder;" class="mt-3">Average Change per Day</h5>
                    <h3 style="color: #1da1f2;" id="growth_followers_average"></h3>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Number of Tweet</h4>
                </div>
                <div class="col-sm-9 mt-2 p-2">
                    <canvas id="chart-number-posts" height="150"></canvas>
                </div>
                <div class="col-sm-3 mt-2 p-2">
                    <h5 style="font-weight: bolder;">Sum of Profile Tweets</h5>
                    <h3 style="color: #1da1f2;" id="number_post_total"></h3>
                    <h5 style="font-weight: bolder;" class="mt-3">Average Profile Tweet per Day</h5>
                    <h3 style="color: #1da1f2;" id="number_post_average"></h3>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Average Interaction per Tweet</h4>
                </div>
                <div class="col-sm-9 mt-2 p-2">
                    <canvas id="chart-average-interaction-post" height="150"></canvas>
                </div>
                <div class="col-sm-3 mt-2 p-2">
                    <h6 style="font-weight: bolder;" class="mt-3">Max Interactions per Tweet on</h6>
                    <h3 style="color: #1da1f2;" id="average_interaction_post_max"></h3>
                    <span id="average_interaction_post_max_date"></span>
                    <h6 style="font-weight: bolder;" class="mt-3">Min Interactions per Tweet on</h6>
                    <h3 style="color: #1da1f2;" id="average_interaction_post_min"></h3>
                    <span id="average_interaction_post_min_date"></span>
                    <h6 style="font-weight: bolder;" class="mt-3">Average Interactions per Tweet on</h6>
                    <h3 style="color: #1da1f2;" id="average_interaction_post_average"></h3>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Distribution of Interactions</h4>
                </div>
                <div class="col-sm-4 mt-2 p-2">
                    <canvas id="chart-distribution-interactions" height="200"></canvas>
                </div>
                <div class="col-sm-8 mt-2 p-2">
                    <table class="table" style="border: solid 2px darkgray;">
                        <tr style="background-color: lightgrey;">
                            <th></th>
                            <th>Count</th>
                            <th>Share</th>
                        </tr>
                        <tr>
                            <td><span style="background-color: #1da1f2;">&nbsp;&nbsp;&nbsp;</span> <i class="ft-repeat mr-2 ml-2"></i> Retweets</td>
                            <td id="distribution_interactions_retweet_number">0</td>
                            <td id="distribution_interactions_retweet_percen">0%</td>
                        </tr>
                        <tr>
                            <td><span style="background-color: #0779bf;">&nbsp;&nbsp;&nbsp;</span> <i class="ft-message-square mr-2 ml-2"></i> Replies</td>
                            <td id="distribution_interactions_reply_number">0</td>
                            <td id="distribution_interactions_reply_percen">0%</td>
                        </tr>
                        <tr>
                            <td><span style="background-color: #005183;">&nbsp;&nbsp;&nbsp;</span> <i class="ft-thumbs-up mr-2 ml-2"></i> Likes</td>
                            <td id="distribution_interactions_like_number">0</td>
                            <td id="distribution_interactions_like_percen">0%</td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Evolution of Interactions</h4>
                </div>
                <div class="col-sm-9 mt-2 p-2">
                    <canvas id="chart-evolution-interactions" height="200"></canvas>
                </div>
                <div class="col-sm-3 mt-2 p-2">
                    <h5 style="font-weight: bolder;" class="mt-3">Max Interactions on</h5>
                    <h3 style="color: #1da1f2;" id="evolution_interaction_max"></h3>
                    <span id="evolution_interaction_max_date"></span>
                    <h5 style="font-weight: bolder;" class="mt-3">Average Interactions per Day</h5>
                    <h3 style="color: #1da1f2;" id="evolution_interaction_average"></h3>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Number of Mentions</h4>
                </div>
                <div class="col-sm-12">No Data</div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">User Activity</h4>
                </div>
                <div class="col-sm-12 mt-2 p-2">
                    <canvas id="chart-user-activity" height="200"></canvas>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Most Engaging Tweet Overview</h4>
                </div>
                <div class="col-sm-12 mt-2 p-2">
                    <div class="row m-0 p-0" id="limitpost">
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('pagecss')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.3/css/bootstrap-select.min.css">
@endsection
@section('pagejs')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script>

    var element_total_followers = document.getElementById('chart-total-followers').getContext('2d');
    var element_growth_followers = document.getElementById('chart-growth-followers').getContext('2d');
    var element_number_posts = document.getElementById('chart-number-posts').getContext('2d');
    var element_average_interaction_post = document.getElementById('chart-average-interaction-post').getContext('2d');
    var element_distribution_interactions = document.getElementById('chart-distribution-interactions').getContext('2d');
    var element_evolution_interactions = document.getElementById('chart-evolution-interactions').getContext('2d');
    var element_user_activity = document.getElementById('chart-user-activity').getContext('2d');

    var ctx_total_followers = new Chart(element_total_followers, {});
    var ctx_growth_followers = new Chart(element_growth_followers, {});
    var ctx_number_posts = new Chart(element_number_posts, {});
    var ctx_average_interaction_post = new Chart(element_average_interaction_post, {});
    var ctx_distribution_interactions = new Chart(element_distribution_interactions, {});
    var ctx_evolution_interactions = new Chart(element_evolution_interactions, {});
    var ctx_user_activity = new Chart(element_user_activity, {});

    $(document).ready(function() {
        get_stat();
        get_total_followers();
        get_growth_followers();
        get_number_posts();
        get_average_interaction_post();
        get_distribution_interactions();
        get_evolution_interactions();
        get_user_activity();
        get_limit_post();
    });

    function filterDate() {
        var start_date = document.getElementById("start_date").value;
        var end_date = document.getElementById("end_date").value;
        ctx_total_followers.destroy();
        ctx_growth_followers.destroy();
        ctx_number_posts.destroy();
        ctx_average_interaction_post.destroy();
        ctx_distribution_interactions.destroy();
        ctx_evolution_interactions.destroy();
        ctx_user_activity.destroy();
        get_stat(start_date, end_date);
        get_total_followers(start_date, end_date);
        get_growth_followers(start_date, end_date);
        get_number_posts(start_date, end_date);
        get_average_interaction_post(start_date, end_date);
        get_distribution_interactions(start_date, end_date);
        get_evolution_interactions(start_date, end_date);
        get_user_activity(start_date, end_date);
        get_limit_post(start_date, end_date);
    }

    function get_stat(start_date="{{ $item['date']['profile']['first'] }}", end_date="{{ $item['date']['profile']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/twitter/get_stat/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                if (result['followers_growth'] > 0) {
                    document.getElementById("stat_followers").innerHTML = result['followers_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(+" + result['followers_growth'].toLocaleString('id') + ")" +"</span>";
                } else {
                    document.getElementById("stat_followers").innerHTML = result['followers_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(" + result['followers_growth'].toLocaleString('id') + ")" +"</span>";
                }
                if (result['following_growth'] > 0) {
                    document.getElementById("stat_following").innerHTML = result['following_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(+" + result['following_growth'].toLocaleString('id') + ")" +"</span>";
                } else {
                    document.getElementById("stat_following").innerHTML = result['following_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(" + result['following_growth'].toLocaleString('id') + ")" +"</span>";
                }
                if (result['post_growth'] > 0) {
                    document.getElementById("stat_posts").innerHTML = result['post_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(+" + result['post_growth'].toLocaleString('id') + ")" +"</span>";
                } else {
                    document.getElementById("stat_posts").innerHTML = result['post_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(" + result['post_growth'].toLocaleString('id') + ")" +"</span>";
                }
                if (result['like_growth'] > 0) {
                    document.getElementById("stat_likes").innerHTML = result['like_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(+" + result['like_growth'].toLocaleString('id') + ")" +"</span>";
                } else {
                    document.getElementById("stat_likes").innerHTML = result['like_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(" + result['like_growth'].toLocaleString('id') + ")" +"</span>";
                }
            }
        });
    }

    function get_total_followers(start_date="{{ $item['date']['profile']['first'] }}", end_date="{{ $item['date']['profile']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/twitter/get_total_followers/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_total_followers = [];
                var data_total_followers = [];
                result.forEach(element => {
                    label_total_followers.push(element.date)
                    data_total_followers.push(element.followed_by)
                });

                ctx_total_followers = new Chart(element_total_followers, {
                    type: 'line',
                    data: {
                        labels: label_total_followers,
                        datasets: [
                            {
                                label: 'Followers',
                                backgroundColor: '#1da1f2',
                                borderColor: '#1da1f2',
                                fill: false,
                                data: data_total_followers,
                            },
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: false
                        },
                        bezierCurve : false,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: false
                                }
                            }]
                        },
                        elements: {
                            line: {
                                tension: 0
                            }
                        }
                    }
                });
            }
        });
    }

    function get_growth_followers(start_date="{{ $item['date']['profile']['first'] }}", end_date="{{ $item['date']['profile']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/twitter/get_growth_followers/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_growth_followers = [];
                var data_growth_followers = [];
                result['data'].forEach(element => {
                    label_growth_followers.push(element.date)
                    data_growth_followers.push(element.followed_by)
                });

                document.getElementById("growth_followers_total").innerHTML = result['total'].toLocaleString('id');
                document.getElementById("growth_followers_change").innerHTML = result['change'].toLocaleString('id');
                document.getElementById("growth_followers_max").innerHTML = result['max'].toLocaleString('id');
                document.getElementById("growth_followers_max_date").innerHTML = result['max_date'];
                document.getElementById("growth_followers_average").innerHTML = result['average'].toLocaleString('id');

                ctx_growth_followers = new Chart(element_growth_followers, {
                    type: 'bar',
                    data: {
                        labels: label_growth_followers,
                        datasets: [
                            {
                                label: 'Followers',
                                backgroundColor: '#1da1f2',
                                borderColor: '#1da1f2',
                                fill: false,
                                data: data_growth_followers,
                            },
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: false
                        },
                        bezierCurve : false,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: false
                                }
                            }]
                        },
                        elements: {
                            line: {
                                tension: 0
                            }
                        }
                    }
                });
            }
        });
    }

    function get_number_posts(start_date="{{ $item['date']['post']['first'] }}", end_date="{{ $item['date']['post']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/twitter/get_number_posts/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_number_posts = [];
                var data_number_posts = [];
                result['data'].forEach(element => {
                    label_number_posts.push(element.date)
                    data_number_posts.push(element.number_post)
                });

                document.getElementById("number_post_total").innerHTML = result['total'].toLocaleString('id');
                document.getElementById("number_post_average").innerHTML = result['average'].toLocaleString('id');

                ctx_number_posts = new Chart(element_number_posts, {
                    type: 'bar',
                    data: {
                        labels: label_number_posts,
                        datasets: [
                            {
                                label: 'Tweet',
                                backgroundColor: '#1da1f2',
                                data: data_number_posts,
                            },
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: false
                        },
                        bezierCurve : false
                    }
                });
            }
        });
    }

    function get_average_interaction_post(start_date="{{ $item['date']['post']['first'] }}", end_date="{{ $item['date']['post']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/twitter/get_average_interaction_post/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_average_interaction_post = [];
                var data_average_interaction_post = [];
                result['data'].forEach(element => {
                    label_average_interaction_post.push(element.date)
                    data_average_interaction_post.push(element.interaction)
                });

                document.getElementById("average_interaction_post_max").innerHTML = result['max'].toLocaleString('id');
                document.getElementById("average_interaction_post_max_date").innerHTML = result['max_date'];
                document.getElementById("average_interaction_post_min").innerHTML = result['min'].toLocaleString('id');
                document.getElementById("average_interaction_post_min_date").innerHTML = result['min_date'];
                document.getElementById("average_interaction_post_average").innerHTML = result['average'].toLocaleString('id');

                ctx_average_interaction_post = new Chart(element_average_interaction_post, {
                    type: 'bar',
                    data: {
                        labels: label_average_interaction_post,
                        datasets: [
                            {
                                label: 'Tweet',
                                backgroundColor: '#1da1f2',
                                data: data_average_interaction_post,
                            },
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: false
                        },
                        bezierCurve : false
                    }
                });
            }
        });
    }

    function get_distribution_interactions(start_date="{{ $item['date']['post']['first'] }}", end_date="{{ $item['date']['post']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/twitter/get_distribution_interactions/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_distribution_interactions = [];
                label_distribution_interactions.push('Retweets');
                label_distribution_interactions.push('Replies');
                label_distribution_interactions.push('Likes');

                var data_distribution_interactions = [];
                data_distribution_interactions.push(result['retweet'])
                data_distribution_interactions.push(result['reply'])
                data_distribution_interactions.push(result['like'])
                var interaction_total = result['retweet'] + result['reply'] + result['like'];

                document.getElementById("distribution_interactions_retweet_number").innerHTML = result['retweet'].toLocaleString('id');
                document.getElementById("distribution_interactions_retweet_percen").innerHTML = ((result['retweet'] / interaction_total) * 100).toLocaleString('id') + "%";
                document.getElementById("distribution_interactions_reply_number").innerHTML = result['reply'].toLocaleString('id');
                document.getElementById("distribution_interactions_reply_percen").innerHTML = ((result['reply'] / interaction_total) * 100).toLocaleString('id') + "%";
                document.getElementById("distribution_interactions_like_number").innerHTML = result['like'].toLocaleString('id');
                document.getElementById("distribution_interactions_like_percen").innerHTML = ((result['like'] / interaction_total) * 100).toLocaleString('id') + "%";

                var ctx = document.getElementById('chart-distribution-interactions').getContext('2d');
                var chart = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: label_distribution_interactions,
                        datasets: [{
                            label: 'Distribution Interactions',
                            backgroundColor: [
                                '#1da1f2',
                                '#0779bf',
                                '#005183'
                            ],
                            data: data_distribution_interactions,
                            name: label_distribution_interactions,
                        }],
                    },
                    options: {
                        responsive: true,
                        events: false,
                        legend: {
                            display: false,
                            position: 'right'
                        },
                        animation: {
                            // duration: 500,
                            // easing: "easeOutQuart",
                            onComplete: function () {
                            var ctx = this.chart.ctx;
                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function (dataset) {

                                for (var i = 0; i < dataset.data.length; i++) {
                                var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                    total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                    mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
                                    start_angle = model.startAngle,
                                    end_angle = model.endAngle,
                                    mid_angle = start_angle + (end_angle - start_angle)/2;

                                var x = mid_radius * Math.cos(mid_angle);
                                var y = mid_radius * Math.sin(mid_angle);

                                ctx.fillStyle = '#fff';
                                if (i == 3){
                                    ctx.fillStyle = '#444';
                                }
                                var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
                                // if(dataset.data[i] != 0 && dataset._meta[0].data[i].hidden != true) {
                                    ctx.fillText(dataset.name[i] + ' (' + dataset.data[i] + ')', model.x + x, model.y + y);
                                    ctx.fillText(percent, model.x + x, model.y + y + 15);
                                // }
                                }
                            });
                            }
                        }
                    }
                });
            }
        });
    }

    function get_evolution_interactions(start_date="{{ $item['date']['post']['first'] }}", end_date="{{ $item['date']['post']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/twitter/get_evolution_interactions/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_evolution_interactions = [];
                var data_evolution_interactions = [];
                data_evolution_interactions.push([]);
                data_evolution_interactions.push([]);
                data_evolution_interactions.push([]);

                result['data'].forEach(element => {
                    label_evolution_interactions.push(element.date)
                    data_evolution_interactions[0].push(element.retweet)
                    data_evolution_interactions[1].push(element.reply)
                    data_evolution_interactions[2].push(element.like)
                });

                document.getElementById("evolution_interaction_max").innerHTML = result['max'].toLocaleString('id');
                document.getElementById("evolution_interaction_max_date").innerHTML = result['max_date'];
                document.getElementById("evolution_interaction_average").innerHTML = result['average'].toLocaleString('id');

                ctx_evolution_interactions = new Chart(element_evolution_interactions, {
                    type: 'bar',
                    data: {
                        labels: label_evolution_interactions,
                        datasets: [
                            {
                                label: 'Retweets',
                                backgroundColor: '#1da1f2',
                                data: data_evolution_interactions[0],
                            },
                            {
                                label: 'Replies',
                                backgroundColor: '#0779bf',
                                data: data_evolution_interactions[1],
                            },
                            {
                                label: 'Likes',
                                backgroundColor: '#005183',
                                data: data_evolution_interactions[2],
                            },
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: true,
                            position: 'bottom'
                        },
                        scales: {
                            xAxes: [{
                                stacked: true,
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        },
                        bezierCurve : false
                    }
                });
            }
        });
    }

    function get_user_activity(start_date="{{ $item['date']['post']['first'] }}", end_date="{{ $item['date']['post']['last'] }}") {

        $.ajax({
            url: "{{ url('/admin/twitter/get_user_activity/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_evolution_interactions = 'User Activity';
                var data_evolution_interactions = [];

                result.forEach(element => {
                    var temp = {};
                    temp['x'] = (new Date(element['x'])).getTime();
                    temp['y'] = element['y']
                    temp['r'] = element['r'] * 3;
                    data_evolution_interactions.push(temp)
                });

                ctx_user_activity = new Chart(element_user_activity, {
                    type: 'bubble',
                    data: {
                        labels: label_evolution_interactions,
                        datasets: [
                            {
                                label: label_evolution_interactions,
                                data: data_evolution_interactions,
                                backgroundColor: '#1da1f2'
                            }
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: true,
                            position: 'bottom'
                        },
                        scales: {
                            xAxes: [{
                                stacked: true,
                                ticks: {
                                    min: (new Date(data_evolution_interactions[0]['x'] - (1 * 86400000))).getTime(),
                                    max: (new Date(data_evolution_interactions[data_evolution_interactions.length - 1]['x'] + (1 * 86400000))).getTime(),
                                    callback: function(value, index, values) {
                                        return new Date(value).toLocaleDateString("en-US")
                                    }
                                },
                            }],
                            yAxes: [{
                                stacked: true,
                                ticks: {
                                    min: 0,
                                    max: 24,
                                    stepSize: 1,
                                    callback: function(value, index, values) {
                                        if(value < 10) {
                                            return '0'+value.toString()+':00';
                                        } else {
                                            return value.toString()+':00';
                                        }
                                    }
                                },
                            }]
                        },
                        tooltips: {
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    var label = data.datasets[tooltipItem.datasetIndex].label;
                                    var val = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                    if(val.y < 10) {
                                        return new Date(val.x).toLocaleDateString("en-US") +  " " + '0'+ val.y.toString()+':00' + " - " + val.r / 3 + " Aktivitas";
                                    } else {
                                        return new Date(val.x).toLocaleDateString("en-US") +  " " + val.y.toString()+':00'  + " - " + val.r / 3 + " Aktivitas";
                                    }
                                }
                            }
                        }
                    }
                });

            }
        });
    }

    function get_limit_post(start_date="{{ $item['date']['post']['first'] }}", end_date="{{ $item['date']['post']['last'] }}") {

        $.ajax({
            url: "{{ url('/admin/twitter/get_limit_post/'.$item['profile']['username'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){

                var html = ``;
                document.getElementById("limitpost").innerHTML = html;
                result.forEach(element => {
                    html += `
                    <div class="col-sm-5 ml-4 mb-2" style="border: solid 2px lightgrey; border-radius: 15px; height: fit-content;">
                        <div class="row" style="margin-top: 5px; margin-bottom: 5px; border-bottom: solid 2px lightgrey;">
                            <div class="col-2">
                                <img src="{{ $item['profile']['profile_image_url'] }}" style="margin-bottom: 5px; width: 100%;"/>
                            </div>
                            <div class="col-10">
                                <h6 style="font-weight: bold">
                                    {{$item['profile']['name']}}
                                </h6>
                                <h6>
                                    <span class="badge" style="background-color: #1da1f2; color: #FFFFFF; padding: 3px;">Twitter</span>
                                    / {{$item['profile']['username']}}
                                    <i class="ml-4">` + element['created_at'] + `</i>
                                </h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 p-2" style="display: grid;">
                                <span class="p-2">` + element['text'] + `</span>
                                <span class="ml-2">
                                </span>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 5px; margin-bottom: 5px; border-top: solid 2px lightgrey;">
                            <div class="col-sm-3 p-1" style="border-right: solid 1px lightgrey;">
                                <b>` + (element['like_count'] + element['reply_count'] + element['retweet_count']).toLocaleString('id') + `</b>
                                <br>Interactions
                            </div>
                            <div class="col-sm-2 p-1" style="border-right: solid 1px lightgrey;">
                                <b>` + element['like_count'].toLocaleString('id') + `</b>
                                <br>Likes
                            </div>
                            <div class="col-sm-2 p-1" style="border-right: solid 1px lightgrey;">
                                <b>` + element['reply_count'].toLocaleString('id') + `</b>
                                <br>Replies
                            </div>
                            <div class="col-sm-2 p-1" style="border-right: solid 1px lightgrey;">
                                <b>` + element['retweet_count'].toLocaleString('id') + `</b>
                                <br>Retweets
                            </div>
                            <div class="col-sm-3 p-1">
                                <b>` + (((element['like_count'] + element['reply_count'] + element['retweet_count']) / + `<?php echo $item['profile']['followers_count']; ?>` ) * 1000).toLocaleString('id') + `</b>
                                <br>Interactions per 1k Followers
                            </div>
                        </div>
                    </div>
                    `;
                });

                document.getElementById("limitpost").innerHTML = html;
            }
        });
    }

</script>
@endsection
