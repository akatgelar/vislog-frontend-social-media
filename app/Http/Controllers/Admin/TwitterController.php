<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Notifications\Notifiable;
use App\Tw_post;
use App\Tw_profile;
use App\Socialmedia;
use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;
use Session;
use Validator;
use Hash;
use \Carbon\Carbon;
use Auth;
use DateTime;
use DateInterval;
use DatePeriod;
use MongoDB\BSON\UTCDateTime as MongoDate;
use PhpParser\Node\Stmt\Foreach_;
use Illuminate\Support\Facades\DB;

class TwitterController extends Controller
{
    use Notifiable;

    public function __construct()
    {
        //
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.twitter.index');
    }

    public function indexjson()
    {
        $data_username = Socialmedia::where([['is_active', '=', true],['category', '=', 'twitter'],['created_by', '=', Auth::id()]])->get()->toArray();
        $data_full = [];
        foreach($data_username as $du) {
            $last_update = Tw_profile::where([['username', '=', $du['username']]])->orderBy('date', 'desc')->limit(1)->get(['date', 'profile_image_url'])->toArray();
            if ($last_update) {
                $du['last_update'] = $last_update['0']['date'];
                $du['profile_pic'] = $last_update['0']['profile_image_url'];
            } else {
                $du['last_update'] = '';
                $du['profile_pic'] = '';
            }
            array_push($data_full, $du);
        }
        return datatables($data_full)
        ->addColumn('profile_pic', function ($dt) {
            return view('admin.twitter.profilepic',compact('dt'));
        })
        ->addColumn('action', function ($dt) {
            return view('admin.twitter.action',compact('dt'));
        })
        ->toJson();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function dashboard($socialmediaid)
    {
        $profile = Tw_profile::where([['username', '=', $socialmediaid]])->orderBy('date', 'desc')->limit(1)->get(['date', 'id', 'username', 'name', 'description', 'url', 'profile_image_url', 'location', 'protected', 'verified', 'followers_count', 'following_count', 'tweet_count', 'listed_count'])->toArray()[0];

        $first_profile = Tw_profile::where([['username', '=', $socialmediaid]])->orderBy('date', 'asc')->limit(1)->get(['date'])->toArray();
        $last_profile = Tw_profile::where([['username', '=', $socialmediaid]])->orderBy('date', 'desc')->limit(1)->get(['date'])->toArray();
        $first_profile_date = DateTime::createFromFormat('Y-m-d H:i:s', $first_profile[0]['date']);
        $last_profile_date = DateTime::createFromFormat('Y-m-d H:i:s', $last_profile[0]['date']);

        // $first_post = Tw_post::where([['author_id', '=', $profile['id']]])->orderBy('created_at', 'asc')->limit(1)->get(['created_at'])->toArray();
        // $last_post = Tw_post::where([['author_id', '=', $profile['id']]])->orderBy('created_at', 'desc')->limit(1)->get(['created_at'])->toArray();
        // $first_post_date = DateTime::createFromFormat('Y-m-d H:i:s', $first_post[0]['created_at']);
        // $last_post_date = DateTime::createFromFormat('Y-m-d H:i:s', $last_post[0]['created_at']);

        $item = [];
        $item['profile'] = $profile;
        $item['post'] = [];
        $item['date']['profile']['first'] = $first_profile_date->format('Y-m-d');
        $item['date']['profile']['last'] = $last_profile_date->format('Y-m-d');
        $item['date']['post']['first'] = $first_profile_date->format('Y-m-d');
        $item['date']['post']['last'] = $last_profile_date->format('Y-m-d');
        // $item['date']['post']['first'] = $first_post_date->format('Y-m-d');
        // $item['date']['post']['last'] = $last_post_date->format('Y-m-d');

        return view('admin.twitter.dashboard', compact('item'));
    }

    public function get_limit_post($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');
        $profile = Tw_profile::where([['username', '=', $socialmediaid]])->orderBy('date', 'desc')->limit(1)->get(['date', 'id', 'username', 'name', 'description', 'url', 'profile_image_url', 'location', 'protected', 'verified', 'followers_count', 'following_count', 'tweet_count', 'listed_count'])->toArray()[0];

        $result = [];
        $post = Tw_post::where([['author_id', '=', $profile['id']], ['date', '>=', $startdate], ['date', '<=', $enddate]]);
        $post = $post->orderBy('media_preview_like', 'desc')->limit(6)->get()->toArray();
        foreach ($post as $p) {
            $p['datetimes'] = Carbon::createFromTimestamp($p['created_at'])->toFormattedDateString();
            array_push($result, $p);
        }
        return $result;
    }

    public function get_stat($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');
        $profile = Tw_profile::where([['username', '=', $socialmediaid]])->orderBy('date', 'desc')->limit(1)->get(['date', 'id', 'username', 'name', 'description', 'url', 'profile_image_url', 'location', 'protected', 'verified', 'followers_count', 'following_count', 'tweet_count', 'listed_count'])->toArray()[0];

        $profile_start = Tw_profile::where([['username', '=', $socialmediaid], ['date', '>=', $startdate]])->orderBy('date', 'asc')->limit(1)->get(['followers_count', 'following_count', 'tweet_count'])->toArray();
        $profile_end = Tw_profile::where([['username', '=', $socialmediaid], ['date', '<=', $enddate]])->orderBy('date', 'desc')->limit(1)->get(['followers_count', 'following_count', 'tweet_count'])->toArray();
        $like_start = Tw_post::where([['author_id', '=', $profile['id']], ['created_at', '<=', $startdate]])->sum('like_count');
        $like_end = Tw_post::where([['author_id', '=', $profile['id']], ['created_at', '<=', $enddate]])->sum('like_count');

        $result = [];

        if (count($profile_start) >= 1) {
            $result['followers_start'] = $profile_start[0]['followers_count'];
            $result['following_start'] = $profile_start[0]['following_count'];
            $result['post_start'] = $profile_start[0]['tweet_count'];
        } else {
            $result['followers_start'] = 0;
            $result['following_start'] = 0;
            $result['post_start'] = 0;
        }
        if (count($profile_end) >= 1) {
            $result['followers_end'] = $profile_end[0]['followers_count'];
            $result['following_end'] = $profile_end[0]['following_count'];
            $result['post_end'] = $profile_end[0]['tweet_count'];
        } else {
            $result['followers_end'] = 0;
            $result['following_end'] = 0;
            $result['post_end'] = 0;
        }

        if ($like_start) {
            $result['like_start'] = $like_start;
        } else {
            $result['like_start'] = 0;
        }
        if ($like_end) {
            $result['like_end'] = $like_end;
        } else {
            $result['like_end'] = 0;
        }

        $result['followers_total'] = $result['followers_end'];
        $result['followers_growth'] = $result['followers_end'] - $result['followers_start'];
        $result['following_total'] = $result['following_end'];
        $result['following_growth'] = $result['following_end'] - $result['following_start'];
        $result['post_total'] = $result['post_end'];
        $result['post_growth'] = $result['post_end'] - $result['post_start'];
        $result['like_total'] = $result['like_end'];
        $result['like_growth'] = $result['like_end'] - $result['like_start'];
        return $result;
    }

    public function get_total_followers($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $total = [];

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($startdate, $interval, $enddate);

        $temp_number = 0;
        foreach ($period as $dt) {
            $now = $dt;
            $tomorrow = DateTime::createFromFormat('Y-m-d',  $now->format('Y-m-d'))->modify('1 day');

            $followed_by = Tw_profile::where([['username', '=', $socialmediaid], ['date', '>=', $now], ['date', '<=', $tomorrow]])->limit(1)->get(['followers_count'])->toArray();
            if($followed_by) {
                $temp_number = $followed_by[0]['followers_count'];
            }

            if($temp_number > 0) {
                $temp = [];
                $temp['date'] = $now->format('Y-m-d');
                $temp['followed_by'] = $temp_number;
                array_push($total, $temp);
            }
        }

        return $total;
    }

    public function get_growth_followers($socialmediaid, $startdate, $enddate)
    {
        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $growth = [];
        $growth_final = [];
        $result = [];
        $result['data'] = [];
        $result['total'] = 0;
        $result['change'] = 0;
        $result['max'] = 0;
        $result['max_date'] = 0;
        $result['average'] = 0;

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($startdate, $interval, $enddate);

        $temp_number = 0;
        foreach ($period as $dt) {
            $now = $dt;
            $tomorrow = DateTime::createFromFormat('Y-m-d',  $now->format('Y-m-d'))->modify('1 day');

            $followed_by = Tw_profile::where([['username', '=', $socialmediaid], ['date', '>=', $now], ['date', '<=', $tomorrow]])->limit(1)->get(['followers_count'])->toArray();
            if($followed_by) {
                $temp_number = $followed_by[0]['followers_count'];
            }

            if($temp_number > 0) {
                $temp = [];
                $temp['date'] = $now->format('Y-m-d');
                $temp['followed_by'] = $temp_number;
                array_push($growth, $temp);
            }
        }
        $result['total'] = $growth[count($growth)-1]['followed_by'];
        $result['change'] = $growth[count($growth)-1]['followed_by'] - $growth[0]['followed_by'];
        $result['average'] = $result['change'] / count($growth);

        for ($i=1; $i<count($growth); $i++){
            $temp = [];
            $temp['date'] = $growth[$i]['date'];
            $temp['followed_by'] = $growth[$i]['followed_by'] - $growth[$i-1]['followed_by'];
            array_push($growth_final, $temp);

            if ($temp['followed_by'] > $result['max']) {
                $result['max'] = $temp['followed_by'];
                $result['max_date'] = $temp['date'];
            }
        }
        $result['data'] = $growth_final;

        return $result;
    }

    public function get_number_posts($socialmediaid, $startdate, $enddate)
    {

        $profile = Tw_profile::where([['username', '=', $socialmediaid]])->orderBy('date', 'desc')->limit(1)->get(['date', 'id', 'username', 'name', 'description', 'url', 'profile_image_url', 'location', 'protected', 'verified', 'followers_count', 'following_count', 'tweet_count', 'listed_count'])->toArray()[0];

        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $number = [];
        $number['data'] = [];
        $number['total'] = 0;
        $number['average'] = 0;

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($startdate, $interval, $enddate);

        foreach ($period as $dt) {
            $now = $dt;
            $tomorrow = DateTime::createFromFormat('Y-m-d',  $now->format('Y-m-d'))->modify('1 day');

            $number_post = Tw_post::where([['author_id', '=', $profile['id']], ['created_at', '>=', $now], ['created_at', '<', $tomorrow]])->count();

            $temp = [];
            $temp['date'] = $now->format('Y-m-d');
            $temp['number_post'] = $number_post;
            array_push($number['data'], $temp);
            $number['total'] = $number['total'] + $temp['number_post'];
        }
        $number['average'] = $number['total'] / count($number['data']);

        return $number;
    }

    public function get_average_interaction_post($socialmediaid, $startdate, $enddate)
    {
        $profile = Tw_profile::where([['username', '=', $socialmediaid]])->orderBy('date', 'desc')->limit(1)->get(['date', 'id', 'username', 'name', 'description', 'url', 'profile_image_url', 'location', 'protected', 'verified', 'followers_count', 'following_count', 'tweet_count', 'listed_count'])->toArray()[0];

        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $result = [];
        $result['data'] = [];
        $result['max'] = 0;
        $result['max_date'] = 0;
        $result['min'] = 0;
        $result['min_date'] = 0;
        $result['average'] = 0;
        $result['count'] = 0;
        $result['total'] = 0;

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($startdate, $interval, $enddate);

        foreach ($period as $dt) {
            $now = $dt;
            $tomorrow = DateTime::createFromFormat('Y-m-d',  $now->format('Y-m-d'))->modify('1 day');

            $like = Tw_post::where([['author_id', '=', $profile['id']], ['created_at', '>=', $now], ['created_at', '<', $tomorrow]])->sum('like_count');
            $retweet = Tw_post::where([['author_id', '=', $profile['id']], ['created_at', '>=', $now], ['created_at', '<', $tomorrow]])->sum('retweet_count');
            $reply = Tw_post::where([['author_id', '=', $profile['id']], ['created_at', '>=', $now], ['created_at', '<', $tomorrow]])->sum('reply_count');
            $count = Tw_post::where([['author_id', '=', $profile['id']], ['created_at', '>=', $now], ['created_at', '<', $tomorrow]])->count();

            $temp = [];
            $temp['date'] = $now->format('Y-m-d');
            $temp['interaction'] = $like + $retweet + $reply;
            $temp['count'] = $count;
            array_push($result['data'], $temp);
        }

        foreach ($result['data'] as $dt) {
            if ($dt['interaction'] > 0) {
                $result['min'] = $dt['interaction'];
                $result['min_date'] = $dt['date'];
            }
        }

        foreach ($result['data'] as $dt) {
            $result['count'] = $result['count'] + $dt['count'];
            $result['total'] = $result['total'] + $dt['interaction'];
            if ($dt['interaction'] > $result['max']) {
                $result['max'] = $dt['interaction'];
                $result['max_date'] = $dt['date'];
            }
            if (($dt['interaction'] > 0) && ($dt['interaction'] < $result['min'])) {
                $result['min'] = $dt['interaction'];
                $result['min_date'] = $dt['date'];
            }
        }


        $result['average'] = $result['total'] / $result['count'];
        return $result;
    }

    public function get_distribution_interactions($socialmediaid, $startdate, $enddate)
    {
        $profile = Tw_profile::where([['username', '=', $socialmediaid]])->orderBy('date', 'desc')->limit(1)->get(['date', 'id', 'username', 'name', 'description', 'url', 'profile_image_url', 'location', 'protected', 'verified', 'followers_count', 'following_count', 'tweet_count', 'listed_count'])->toArray()[0];

        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate);

        $like = Tw_post::where([['author_id', '=', $profile['id']], ['created_at', '>=', $startdate], ['created_at', '<', $enddate]])->sum('like_count');
        $retweet = Tw_post::where([['author_id', '=', $profile['id']], ['created_at', '>=', $startdate], ['created_at', '<', $enddate]])->sum('retweet_count');
        $reply = Tw_post::where([['author_id', '=', $profile['id']], ['created_at', '>=', $startdate], ['created_at', '<', $enddate]])->sum('reply_count');

        $result = [];
        $result['like'] = $like;
        $result['retweet'] = $retweet;
        $result['reply'] = $reply;

        return $result;
    }

    public function get_evolution_interactions($socialmediaid, $startdate, $enddate)
    {
        $profile = Tw_profile::where([['username', '=', $socialmediaid]])->orderBy('date', 'desc')->limit(1)->get(['date', 'id', 'username', 'name', 'description', 'url', 'profile_image_url', 'location', 'protected', 'verified', 'followers_count', 'following_count', 'tweet_count', 'listed_count'])->toArray()[0];

        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $result = [];
        $result['data'] = [];
        $result['max'] = 0;
        $result['max_date'] = 0;
        $result['total'] = 0;
        $result['average'] = 0;

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($startdate, $interval, $enddate);

        foreach ($period as $dt) {
            $now = $dt;
            $tomorrow = DateTime::createFromFormat('Y-m-d',  $now->format('Y-m-d'))->modify('1 day');

            $like = Tw_post::where([['author_id', '=', $profile['id']], ['created_at', '>=', $now], ['created_at', '<', $tomorrow]])->sum('like_count');
            $retweet = Tw_post::where([['author_id', '=', $profile['id']], ['created_at', '>=', $now], ['created_at', '<', $tomorrow]])->sum('retweet_count');
            $reply = Tw_post::where([['author_id', '=', $profile['id']], ['created_at', '>=', $now], ['created_at', '<', $tomorrow]])->sum('reply_count');

            $temp = [];
            $temp['date'] = $now->format('Y-m-d');
            $temp['like'] = $like;
            $temp['retweet'] = $retweet;
            $temp['reply'] = $reply;

            array_push($result['data'], $temp);
        }

        foreach ($result['data'] as $dt) {
            if ($dt['like'] + $dt['retweet'] + $dt['reply'] > $result['max']) {
                $result['max'] = $dt['like'] + $dt['retweet'] + $dt['reply'];
                $result['max_date'] = $dt['date'];
            }
            $result['total'] = $result['total'] + $dt['like'] + $dt['retweet'] + $dt['reply'];
        }
        $result['average'] = $result['total'] / count($result['data']);

        return $result;
    }

    public function get_user_activity($socialmediaid, $startdate, $enddate)
    {
        $profile = Tw_profile::where([['username', '=', $socialmediaid]])->orderBy('date', 'desc')->limit(1)->get(['date', 'id', 'username', 'name', 'description', 'url', 'profile_image_url', 'location', 'protected', 'verified', 'followers_count', 'following_count', 'tweet_count', 'listed_count'])->toArray()[0];

        $startdate = DateTime::createFromFormat('Y-m-d', $startdate);
        $enddate = DateTime::createFromFormat('Y-m-d', $enddate)->modify('1 day');

        $result = Tw_post::where([['author_id', '=', $profile['id']], ['created_at', '>=', $startdate], ['created_at', '<', $enddate]]);
        $result = $result->orderBy('created_at', 'asc');
        $result = $result->get(['created_at']);
        $result = $result->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('Y-m-d H');
        });
        $result = $result->toArray();

        $results = [];
        foreach($result as $key => $value) {
            $arr = explode(" ", $key);
            $temp = [];
            $temp['x'] = $arr[0];
            $temp['y'] = (int)$arr[1];
            $temp['r'] = count($value);
            array_push($results, $temp);
        }

        return $results;
    }
}
