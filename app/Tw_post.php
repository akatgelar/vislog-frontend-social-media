<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Tw_post extends Eloquent
{
    protected $guarded = ['id'];
    protected $table = 'tw_post';

    protected $casts = [
        'date' => 'datetime',
        'created_at' => 'datetime',
    ];
}
