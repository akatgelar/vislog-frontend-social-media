<!doctype html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Vislog | Home</title>
	<meta name="description" content="Vislog">
	<meta name="keywords" content="video commercial">
	<link rel="shortcut icon" href="<?php echo e(asset('')); ?>/favicon.ico">
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('startuply')); ?>/css/custom-animations.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('startuply')); ?>/css/lib/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('startuply')); ?>/css/style.css" />

	<!--[if lt IE 9]>
		<script src="<?php echo e(asset('startuply')); ?>/js/html5shiv.js"></script>
		<script src="<?php echo e(asset('startuply')); ?>/js/respond.min.js"></script>
	<![endif]-->
</head>

<body id="landing-page" class="landing-page">
	<!-- Preloader -->
	<div class="preloader-mask">
		<div class="preloader"><div class="spin base_clr_brd"><div class="clip left"><div class="circle"></div></div><div class="gap"><div class="circle"></div></div><div class="clip right"><div class="circle"></div></div></div></div>
	</div>

	<header>
		<nav class="navigation navigation-header">
			<div class="container">
				<div class="navigation-brand">
					<div class="brand-logo">
						<a href="<?php echo e(url('/')); ?>" class="logo">VISLOG</a><a href="<?php echo e(url('/')); ?>" class="logo logo-alt">VISLOG</a>
					</div>
				</div>
				<button class="navigation-toggle">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="navigation-navbar collapsed">
					<ul class="navigation-bar navigation-bar-left">
						<li><a href="#hero">Home</a></li>
						<li><a href="#about">About</a></li>
						<li><a href="#feature">Features</a></li>
						<li><a href="#pricing">Prices</a></li>
						<li><a href="#feedback">Feedback</a></li>
						<li><a href="#process">Process</a></li>
						<li><a href="#contact">Contact</a></li>
						<li id="menu-item-4742" class="dropdown"><a title="EXTRA" class="dropdown-toggle" href="#">Extra <span class="caret"></span></a>
							<ul role="menu" class=" dropdown-menu" style="display: none;">
								<li id="menu-item-4743" ><a href="https://medin.id/" target="_blank">MedIN</a></li>																					
								<li id="menu-item-4743" ><a href="https://bitplay.id/" target="_blank">Bitplay</a></li>																					
								<li id="menu-item-4743" ><a href="https://hitek.co.id/" target="_blank">Hitek</a></li>																					
								<li id="menu-item-4743" ><a href="http://csigroup.co.id/" target="_blank">CSI</a></li>
								<li id="menu-item-4743" ><a href="https://cginetwork.freshdesk.com/support/login/" target="_blank">E-TICKET</a></li>
							</ul>
							</li>
					</ul>
					<ul class="navigation-bar navigation-bar-right">
						<li><a href="<?php echo e(url('login')); ?>">Login</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>

	<div id="hero" class="bg bg1 static-header window-height light-text hero-section ytp-player-background clearfix" data-video="xDQIfx8bB58" data-property="{videoURL: 'https://youtu.be/4YSOqM2aJbM', containment: '#hero', autoPlay: true, realfullscreen: true, stopMovieOnBlur: false, addRaster: false, showControls: false, mute:true, startAt:0, opacity:1, gaTrack: false}">
		<div class="heading-bdlock align-center centered-block">
			<div class="container">
				<h1 class="editContent">Do not wait &mdash; <span class="highlight">monitor</span> your ads now!</h1>
				<h5 class="editContent">manage and monitor your ad, easy, fast & effectively.</h5>
				<a href="<?php echo e(url('/admin')); ?>" class="btn btn-solid editContent">Login</a>
			</div>
		</div>
	</div>

	<div id="clients" class="clients-section align-center">
		<div class="container">
			<ul class="list-inline logos">
				<li><a href="https://medin.id/" target="_blank"><img class="animated" data-animation="fadeInDown" data-duration="500" height="80px" src="<?php echo e(asset('images/group')); ?>/medin.png" alt="mashable" /></a></li>
				<li><a href="https://bitplay.id/" target="_blank"><img class="animated" data-animation="fadeInUp" data-duration="500" height="80px" data-delay="200" src="<?php echo e(asset('images/group')); ?>/bitplay.png" alt="tnw" /></a></li>
				<li><a href="http://hitek.co.id/" target="_blank"><img class="animated" data-animation="fadeInDown" data-duration="500" height="80px" data-delay="400" src="<?php echo e(asset('images/group')); ?>/hitek.png" alt="virgin" /></a></li>
				<li><a href="http://csigroup.co.id/" target="_blank"><img class="animated" data-animation="fadeInUp" data-duration="500" height="80px" data-delay="600" src="<?php echo e(asset('images/group')); ?>/csi.png" alt="microsoft" /></a></li>
			</ul>
		</div>
	</div>

	<section id="about" class="section about-section align-center dark-text">
		<div class="container">

			<ul class="nav nav-tabs alt">
				<li class="active"><a href="#first-tab-alt" data-toggle="tab">USER</a></li>
				<li><a href="#second-tab-alt" data-toggle="tab">TECHNOLOGY</a></li>
				<li><a href="#third-tab-alt" data-toggle="tab">STEPS</a></li>
			</ul>

			<div class="tab-content alt">
				<div class="tab-pane active" id="first-tab-alt">
					<div class="section-content row">
						<div class="col-sm-6 animated" data-delay="200" data-duration="700" data-animation="fadeInLeft">
							<img src="<?php echo e(asset('startuply')); ?>/img/features/people.jpg" class="img-responsive" alt="process 3" />
						</div>
						<div class="col-sm-6 animated" data-delay="200" data-duration="700" data-animation="fadeInRight">
							<br/>
							<article class="align-center">
								<h3>FOR EVERY <span class="highlight">USER</span></h3>
								<p class="sub-title">Vislog is used to meet the needs of all media users.</p>
								<p>Founded in 2009, Hexa Indonesia Teknologi (HITEK) is a global-minded local company with more than 20 clients in our service. Following the motto of Integrated Media Solutions, we dedicate all our efforts to integrated and comprehensive media solution services. Vislog is very useful to optimize all your media activities, be it Broadcasters, Media Agencies or Advertisers.</p>
								<br/>
								<a href="#newsletter" class="btn btn-outline-color">Get Live Demo</a>
							</article>
						</div>
					</div>
				</div>

				<div class="tab-pane" id="second-tab-alt">
					<div class="section-content row">
						<div class="col-sm-6 pull-right animated" data-delay="200" data-duration="700" data-animation="fadeInRight">
							<img src="<?php echo e(asset('startuply')); ?>/img/features/phone.jpg" class="img-responsive pull-right" alt="process 2" />
						</div>
						<div class="col-sm-6 animated" data-delay="200" data-duration="700" data-animation="fadeInLeft">
							<br/><br/>
							<article class="align-center">
								<h3>LATEST <span class="highlight">TECHNOLOGY</span></h3>
								<p class="sub-title">Vislog is always developing with the latest technology.</p>
								<p>As an IT company, keep following the technology is the main key of all our products. Vislog is always developing by following the latest technology. With a web-based application, it will be easy to access Vislog anywhere and anytime, both desktop or smartphone.</p>
							</article>
						</div>
					</div>
				</div>

				<div class="tab-pane" id="third-tab-alt">
					<div class="section-header align-center">
						<h2>3 EASY STEPS</h2>
						<p class="sub-header animated" data-duration="700" data-animation="zoomIn">
							Discover a new way of monitoring your TV campaign.
							<br />Keep your finger with tools that are designed to give you the information you need to make great decisions.
						</p>
					</div>
					<div class="section-content row animated" data-duration="700" data-delay="200" data-animation="fadeInDown">
						<div class="col-sm-4">
							<article class="align-center">
								<i class="howitworks icon icon-shopping-04 highlight"></i>
								<span class="heading">SUBSCRIBE</span>
								<p class="thin" >Subscribe for more monitoring insight at your fingertips. Define your needs, and let's vislog monitor your campaigns.</p>
							</article>
							<!--<span class="icon icon-arrows-04"></span>-->
						</div>
						<div class="col-sm-4">
							<article class="align-center">
								<i class="howitworks icon icon-seo-icons-03 highlight"></i>
								<span class="heading">OUR SYSTEM RUNS</span>
								<p class="thin" >Running vislog with define easy variable. Monitor and capture online visual logproof about your campaigns.</p>
							</article>
							<!--<span class="icon icon-arrows-04"></span>-->
						</div>
						<div class="col-sm-4">
							<article class="align-center">
								<i class="howitworks icon icon-seo-icons-05 highlight"></i>
								<span class="heading">RECEIVE REPORT</span>
								<p class="thin" >Take an easy to understand report. You can configure a report to generate reports on a daily, weekly, monthly or yearly basis.</p>
							</article>
						</div>
					</div>
					<br/>
					<br/>
				</div>
			</div>
		</div>
	</section>

	<hr class="no-margin" />

	<section id="feature" class="section process-section align-center dark-text">
		<div class="container">
			<div class="section-content row">
				<div class="col-sm-6 pull-right animated" data-duration="500" data-animation="fadeInRight">
					<img src="<?php echo e(asset('startuply')); ?>/img/features/content_image1.png" class="img-responsive" alt="process 2" />
				</div>
				<div class="col-sm-6 align-left animated" data-duration="500" data-animation="fadeInLeft">
					<br/><br/>
					<article>
						<h3>NEW AGE <span class="highlight">TECHNOLOGY</span></h3>
						<p class="sub-title">Vislog uses HD quality recording servers.</p>
						<p>With HD quality TV recording technology, this will strengthen the TV log-proof, in both Loose Spot and Creative Items . Our recording server runs 24/7 automatically and tag Ad data, such as: Virtual Ads, Template, Squeeze Frame, Built-In, and much more. Vislog also uses the latest database that is fast and efficient in processing data. The number of TV broadcasts monitored is always increasing according to user needs.</p>
					</article>
				</div>

				<hr class="clearfix" />

				<div class="col-sm-6 animated" data-duration="500" data-animation="fadeInLeft">
					<img src="<?php echo e(asset('startuply')); ?>/img/features/helmet.jpg" class="img-responsive" alt="process 3" />
				</div>
				<div class="col-sm-6 align-right animated" data-duration="500" data-animation="fadeInRight">
					<br/><br/>
					<article>
						<h3>MORE <span class="highlight">COMPLETE</span> FEATURES</h3>
						<p class="sub-title">A powerful tools for TV monitoring.</p>
						<p>Broadcasters, Media Agencies or Advertisers would need web-based media monitoring platform, available from anywhere, anytime. Vislog is compatible with mobile-used and desktop. Analyse campaign, and compare with competitors and create reports in minutes. Tools can provide visually simple reports with video, as well as detailed reports for further analysis. Vislog is more than monitoring tools. Vislog can be used for an off-air broadcast recording solution, compliance logging, monitoring relevant TV channels, track competitors’ channels for competitive analysis. Vislog define rating analyser form another platform fastly and efficiently, create clips for content re-purposing, sharing and exporting track ads automatically for ads verification.</p>
					</article>
				</div>

			</div>
		</div>
	</section>

	<section id="features" class="section features-section align-center inverted">
		<div class="container">
			<div class="section-content">
				<div class="featured-tab">
					<ul class="list-unstyled">
						<li class="active">
							<a href="#home" data-toggle="tab">
								<div class="tab-info">
									<div class="tab-title">Dashboard</div>
									<div class="tab-desc">Vislog dashboard provides an easy comprehensive summary about your ad.</div>
								</div>
								<!--<div class="tab-icon"><span class="icon icon-multimedia-20"></span></div>-->
							</a>
						</li>
						<li>
							<a href="#profile" data-toggle="tab">
								<div class="tab-info">
									<div class="tab-title">Ads Performance</div>
									<div class="tab-desc">Easier to track ad performance and improve your campaign.</div>
								</div>
								<!--<div class="tab-icon"><span class="icon icon-seo-icons-27"></span></div>-->
							</a>
						</li>
						<li>
							<a href="#messages" data-toggle="tab">
								<div class="tab-info">
									<div class="tab-title">Clip Library</div>
									<div class="tab-desc">Understanding how your campaign is performing with HD clip and programme.</div>
								</div>
								<!-- <div class="tab-icon"><span class="icon icon-seo-icons-28"></span></div>-->
							</a>
						</li>
					</ul>

					<div class="tab-content">
						<div class="tab-pane in active" id="home"><img src="<?php echo e(asset('startuply')); ?>/img/features/rich_features_1.png" class="img-responsive animated" data-duration="900" data-animation="flip3dInTop" alt="macbook" /></div>
						<div class="tab-pane" id="profile"><img src="<?php echo e(asset('startuply')); ?>/img/features/rich_features_2.png" class="img-responsive animated" data-duration="900" data-animation="roll3dInLeft" alt="macbook" /></div>
						<div class="tab-pane" id="messages"><img src="<?php echo e(asset('startuply')); ?>/img/features/rich_features_3.png" class="img-responsive animated" data-duration="900" data-animation="fadeInRight" alt="macbook" /></div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="features-list" class="section features-list-section align-center dark-text">
		<div class="container">
			<div class="clearfix animated" data-duration="500" data-animation="fadeInRight">
				<div class="col-md-3 col-sm-6 col-xs-12">
					<article class="align-center">
						<i class="icon icon-seo-icons-27 highlight"></i>
						<span class="heading">WEB-BASED</span>
						<p class="">One-stop solution that gives you or your customer the freedom to work with any browser or device. </p>
					</article>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<article class="align-center">
						<i class="icon icon-faces-users-04 highlight"></i>
						<span class="heading">MULTI-USER</span>
						<p class="">Allow your team and customers to log in the system and get the information they want. </p>
					</article>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<article class="align-center">
						<i class="icon icon-office-24 highlight"></i>
						<span class="heading">DAILY UPDATE</span>
						<p class="">Update every day to get the most current ad data, anytime, from anywhere. </p>
					</article>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<article class="align-center">
						<i class="icon icon-office-32 highlight"></i>
						<span class="heading">VIDEO</span>
						<p class="">Fast clips creation of relevant media, assuring your customers are getting in no time the best results. </p>
					</article>
				</div>
			</div>
			<div class="clearfix animated" data-duration="500" data-delay="500" data-animation="fadeInLeft">
				<div class="col-md-3 col-sm-6 col-xs-12">
					<article class="align-center">
						<i class="icon icon-graphic-design-13 highlight"></i>
						<span class="heading">USER-FRIENDLY</span>
						<p class="">Interactive application display of the information with the media. </p>
					</article>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<article class="align-center">
						<i class="icon icon-multimedia-20 highlight"></i>
						<span class="heading">FILTERING</span>
						<p class="">Advanced search engine for finding fast the relevant media, anytime, from anywhere. </p>
					</article>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<article class="align-center">
						<i class="icon icon-arrows-37 highlight"></i>
						<span class="heading">ANALYZER</span>
						<p class="">Advanced features for integration of rating data from other platform. </p>
					</article>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<article class="align-center">
						<i class="icon icon-seo-icons-15 highlight"></i>
						<span class="heading">SUPPORT</span>
						<p class="">Instead of scripted replies, we have actual experts standing by 24/7 ready to assist you. </p>
					</article>
				</div>
			</div>
		</div>
	</section>

	<section id="newsletter" class="long-block newsletter-section light-text">
		<div class="container align-center">
			<div class="col-sm-12 col-lg-5 animated" data-animation="fadeInLeft" data-duration="500">
				<article>
					<h2>GET LIVE DEMO</h2>
					 <p class="">No spam promise - only latest news and prices!</p>
				</article>
			</div>
			<div class="col-sm-12 col-lg-7 animated" data-animation="fadeInRight" data-duration="500">
				<form class="form" style="padding-top: 10px;" action="<?php echo e(url('demorequest')); ?>" method="post"><?php echo csrf_field(); ?>
					<div class="form-group form-inline">
						<input size="15" type="text" class="form-control required" name="name" placeholder="Your name" />
						<input size="20" type="email" class="form-control required" name="email" placeholder="your@email.com" />
						<input type="submit" class="btn btn-outline" value="REQUEST DEMO" />
						<?php if(Session::has('message')): ?>
						<p><?php echo e(ucfirst(Session::get('message'))); ?></p>
						<?php endif; ?>
					</div>
				</form>
			</div>
		</div>
	</section>

	<section id="pricing" class="section product-section align-center dark-text animated" data-animation="fadeInUp" data-duration="500">
		<div class="container">
			<div class="section-header">
				<h2>PRODUCT <span class="highlight">PACKAGES</span></h2>
				<p class="sub-header">
					Solutions for everyone. Define yor needs, and call us for more special price.
				</p>
			</div>
			<div class="section-content row">

				<div class="col-sm-4">
					<div class="package-column">
						<div class="package-title">BASIC</div>
						<div class="package-price">
							<div class="price"><span class="currency">$</span>2k</div>
							<div class="period">per month</div>
						</div>
						<div class="package-detail">
							<ul class="list-unstyled">
								<li><strong>1</strong> Product Type</li>
								<li><strong>5</strong> User</li>
								<li><strong>5</strong> HD Channel</li>
								<li><strong>5</strong> SD Channel</li>
							</ul>
							<a href="#contact" class="btn btn-outline-color btn-block">Contact Us</a>
						</div>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="package-column">
						<div class="package-title">STANDARD</div>
						<div class="package-price">
							<div class="price"><span class="currency">$</span>3k</div>
							<div class="period">per month</div>
						</div>
						<div class="package-detail">
							<ul class="list-unstyled">
								<li><strong>5</strong> Product Type</li>
								<li><strong>10</strong> User</li>
								<li><strong>5</strong> HD Channel</li>
								<li><strong>10</strong> SD Channel</li>
							</ul>
							<a href="#contact" class="btn btn-outline-color btn-block">Contact Us</a>
						</div>
					</div>
				</div>

				<div class="col-sm-4">
					<div class="package-column">
						<div class="package-title">PREMIUM</div>
						<div class="package-price">
							<div class="price"><span class="currency">$</span>10k</div>
							<div class="period">per month</div>
						</div>
						<div class="package-detail">
							<ul class="list-unstyled">
								<li><strong>50</strong> Product Type</li>
								<li><strong>50</strong> User</li>
								<li><strong>14</strong> HD Channel</li>
								<li><strong>14</strong> SD Channel</li>
							</ul>
							<a href="#contact" class="btn btn-outline-color btn-block">Contact Us</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="awards" class="section awards-section align-center dark-text animated" data-animation="fadeInDown" data-duration="500">
		<div class="container">
			<div class="section-header">
				<h2><span class="highlight">OUR</span> CLIENTS</h2>				
			</div>
			<div class="section-content">
				<ul class="list-inline logos">
					<li><img height="30px" src="<?php echo e(asset('images/clients')); ?>/rcti.png" /></li>
					<li><img height="30px" src="<?php echo e(asset('images/clients')); ?>/mnctv.png" /></li>
					<li><img height="30px" src="<?php echo e(asset('images/clients')); ?>/gtv.png" /></li>
					<li><img height="43px" src="<?php echo e(asset('images/clients')); ?>/sctv.png" /></li>
					<li><img height="43px" src="<?php echo e(asset('images/clients')); ?>/indosiar.png" /></li>
					<li><img height="30px" src="<?php echo e(asset('images/clients')); ?>/inews.png" /></li>
			</div>
		</div>
	</section>

	<section id="feedback" class="section feedback-section align-center light-text">
		<div class="container animated" data-animation="fadeInDown" data-duration="500">
			<div class="section-header">
				<h2>WHAT <span class="highlight">CLIENTS</span> SAY</h2>
			</div>
			<div class="section-content">
				<!-- BEGIN SLIDER CONTENT -->
				<div class="col-sm-10 col-sm-offset-1">
					<div class="flexslider testimonials-slider align-center">
						<ul class="slides">
							<li>
								<div class="testimonial align-center clearfix">
									<blockquote>Let us know what you think about us.</blockquote>
								</div>
							</li>
							<li>
								<div class="testimonial align-center clearfix">
									<blockquote>Let us know what you think about us.</blockquote>
								</div>
							</li>
							<li>
								<div class="testimonial align-center clearfix">
									<blockquote>Let us know what you think about us.</blockquote>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<!-- END SLIDER -->
			</div>
		</div>
	</section>

	<section id="feedback-controls" class="section feedback-controls-section align-center light-text animated" data-animation="fadeInDown" data-duration="500">
		<div class="container">
			<div class="col-md-10 col-md-offset-1">
				<!-- BEGIN CONTROLS -->
				<div class="flex-manual">
					<div class="col-xs-12 col-sm-4 wrap">
						<div class="switch flex-active">
							<img alt="client" src="<?php echo e(asset('startuply')); ?>/img/people/profile-1.jpg" class="sm-pic img-circle pull-left" width="69" height="70">
							<p>
								<span class="highlight">YOUR NAME</span><br/>Position at <span class="highlight">Company.</span>
							</p>
						</div>
					</div>

					<div class="col-xs-12 col-sm-4 wrap">
						<div class="switch pull-left">
							<img alt="client" src="<?php echo e(asset('startuply')); ?>/img/people/profile-2.jpg" class="sm-pic img-circle pull-left" width="69" height="70">
							<p>
								<span class="highlight">YOUR NAME</span><br/>Position at <span class="highlight">Company.</span>
							</p>
						</div>
					</div>

					<div class="col-xs-12 col-sm-4 wrap">
						<div class="switch">
							<img alt="client" src="<?php echo e(asset('startuply')); ?>/img/people/profile-3.jpg" class="sm-pic img-circle pull-left" width="68" height="69">
							<p>
								<span class="highlight">YOUR NAME</span><br/>Position at <span class="highlight">Company.</span>
							</p>
						</div>
					</div>
				</div>
				<!-- END CONTROLS -->
			</div>
		</div>
	</section>

	<section id="process" class="section team-section align-center dark-text">
		<div class="container">
			<div class="section-header">
				<h2>BEHIND <span class="highlight">THE</span> PROCESS</h2>
				<p class="sub-header">
					Simultaneously Record, Recognize Video, Reanalysis Campaign and Create Report.
				</p>
				<p>Vislog logging records the content 24/7 from Nation-Wide TV. Recognize it and enables simultaneous users to monitor the live or archived content. A simple user interface allows the users to monitor the content, search, create and reanalysis campaign. Generates report for your next steps.</p>
			</div>
			<div class="section-content row">
				<div class="col-md-3 col-sm-3 col-xs-6 animated" data-animation="fadeInDown" data-duration="500">
					<div class="team-member">
						<div class="photo-wrapper">
							<div class="overlay-wrapper">
								<img width="262px" src="<?php echo e(asset('images/process')); ?>/record.jpg" alt="">
								<div class="overlay-content">
									<div class="text-wrapper">
										<div class="text-container">
											<p></p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<h5 class="name">RECORD</h5>
					</div>
				</div>

				<div class="col-md-3 col-sm-3 col-xs-6 animated" data-animation="fadeInUp" data-delay="200" data-duration="500">
					<div class="team-member">
						<div class="photo-wrapper">
							<div class="overlay-wrapper">
								<img width="262px" src="<?php echo e(asset('images/process')); ?>/recognize.jpg" alt="">
								<div class="overlay-content">
									<div class="text-wrapper">
										<div class="text-container">
										</div>
									</div>
								</div>
							</div>
						</div>
						<h5 class="name">RECOGNIZE</h5>
					</div>
				</div>

				<div class="col-md-3 col-sm-3 col-xs-6 animated" data-animation="fadeInDown" data-delay="400" data-duration="500">
					<div class="team-member">
						<div class="photo-wrapper">
							<div class="overlay-wrapper">
								<img width="262px" src="<?php echo e(asset('images/process')); ?>/reanalysis.jpg" alt="">
								<div class="overlay-content">
									<div class="text-wrapper">
										<div class="text-container">
										</div>
									</div>
								</div>
							</div>
						</div>
						<h5 class="name">REANALYSIS</h5>
					</div>
				</div>

				<div class="col-md-3 col-sm-3 col-xs-6 animated" data-animation="fadeInUp" data-delay="600" data-duration="500">
					<div class="team-member">
						<div class="photo-wrapper">
							<div class="overlay-wrapper">
								<img width="262px" src="<?php echo e(asset('images/process')); ?>/report.jpg" alt="">
								<div class="overlay-content">
									<div class="text-wrapper">
										<div class="text-container">
										</div>
									</div>
								</div>
							</div>
						</div>
						<h5 class="name">REPORT</h5>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="contact" class="long-block light-text guarantee-section">
		<div class="container">
			<div class="col-md-9 col-lg-9">
				<i class="icon icon-seo-icons-24 pull-left"></i>
				<article class="pull-left">
					<h2>MONITOR YOUR AD NOW!</h2>
					<p class="thin">manage and monitor your ad, easy, fast & effectively.</p>
				</article>
			</div>
			<div class="col-md-3 col-lg-3">
				<a href="#newsletter" class="btn btn-outline">GET LIVE DEMO</a>
			</div>
		</div>
	</section>

	<footer id="footer" class="footer light-text">
		<div class="container">
			<div class="footer-content row">
				<div class="col-sm-4 col-xs-12">
					<div class="logo-wrapper">
						<img height="31" src="<?php echo e(asset('images')); ?>/vislog-logo-white.png" alt="logo" /> <span style="font-size: 25px;vertical-align: bottom;margin-left: 4px;">VISLOG</span>
					</div>
					<p>HITEK presents one of the products for your daily TV monitoring needs. For everyone who is active in media business, Advertisers, Advertising Agencies and TV Station, can use all the features in this tools. Your support and advice will always makes us grow better.</p>
					<p><strong>Yudhi PS, Founder</strong>.</p>
				</div>
				<div class="col-sm-5 social-wrap col-xs-12">
					
				</div>
				<div class="col-sm-3 col-xs-12">
					<strong class="heading">Our Contacts</strong>
					<ul class="list-unstyled">
						<li><span class="icon icon-chat-messages-14"></span><a href="mailto:info.vislog@hitek.co.id">info.vislog@hitek.co.id</a></li>
						<li><span class="icon icon-seo-icons-34"></span>Jl. Lebak Bulus Raya No.Z8 <br>Jakarta Selatan 12440</li>
						<li><span class="icon icon-seo-icons-17"></span>62-21-22763619</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="copyright">Vislog 2020. All rights reserved.</div>
	</footer>

	<div class="back-to-top"><i class="fa fa-angle-up fa-3x"></i></div>

	<!--[if lt IE 9]>
		<script type="text/javascript" src="<?php echo e(asset('startuply')); ?>/js/jquery-1.11.3.min.js?ver=1"></script>
	<![endif]-->
	<!--[if (gte IE 9) | (!IE)]><!-->
		<script type="text/javascript" src="<?php echo e(asset('startuply')); ?>/js/jquery-2.1.4.min.js?ver=1"></script>
	<!--<![endif]-->

	<script type="text/javascript" src="<?php echo e(asset('startuply')); ?>/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo e(asset('startuply')); ?>/js/jquery.flexslider-min.js"></script>
	<script type="text/javascript" src="<?php echo e(asset('startuply')); ?>/js/jquery.appear.js"></script>
	<script type="text/javascript" src="<?php echo e(asset('startuply')); ?>/js/jquery.plugin.js"></script>
	<script type="text/javascript" src="<?php echo e(asset('startuply')); ?>/js/jquery.countdown.js"></script>
	<script type="text/javascript" src="<?php echo e(asset('startuply')); ?>/js/jquery.waypoints.min.js"></script>
	<script type="text/javascript" src="<?php echo e(asset('startuply')); ?>/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="<?php echo e(asset('startuply')); ?>/js/jquery.mb.YTPlayer.min.js"></script>
	<script type="text/javascript" src="<?php echo e(asset('startuply')); ?>/js/jquery-ui-slider.min.js"></script>
	<script type="text/javascript" src="<?php echo e(asset('startuply')); ?>/js/toastr.min.js"></script>
	<script type="text/javascript" src="<?php echo e(asset('startuply')); ?>/js/startuply.js?v=1"></script>
</body>
</html><?php /**PATH C:\xampp\htdocs\vislog\resources\views/home.blade.php ENDPATH**/ ?>