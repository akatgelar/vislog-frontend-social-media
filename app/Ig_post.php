<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Ig_post extends Eloquent
{
    protected $guarded = ['id'];
    protected $table = 'ig_post';

}
