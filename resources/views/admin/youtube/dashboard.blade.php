@extends('admin.layouts.app')

@section('pagetitle')
    <title>Youtube Dashboard</title>
@endsection

@section('content')
    <!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-wrapper">


            <div class="row">
                <div class="col-2">
                    <img src="{{ $item['profile']['thumbnails_medium'] }}" style="margin-bottom: 5px; width: 90%"/>

                    @if($item['profile']['privacyStatus'] === false)<span class="badge" style="background-color: #6768fc; color: #FFFFFF;">Public</span>@else<span class="badge badge-danger">Private</span>@endif
                </div>
                <div class="col-10">
                    <table width="100%">
                        <tr>
                            <th width="100%">
                                <h2 style="font-weight: bold">{{$item['profile']['title']}}</h2>
                                <h6 style="display: contents;" id="text_show"></h6>
                                <a style="display: contents;" id="text_show_link" href="#" onClick="textShow();">&nbsp;&nbsp;More >></a>
                            </th>
                        </tr>
                        <tr>
                            <th width="100%">
                                <h6 style="margin-top: 5px; margin-bottom: 5px;">
                                    <span class="badge" style="background-color: #f70000; color: #FFFFFF;">youtube</span>
                                    / {{$item['profile']['customUrl']}}
                                </h6>
                            </th>
                        </tr>
                        <tr>
                            <th width="100%">
                                <h6 style="margin-top: 5px; margin-bottom: 5px;">
                                    <span style="font-weight: bold">{{number_format($item['profile']['subscriberCount'], 0, ',', '.')}}</span> Subscribers,
                                    <span style="font-weight: bold">{{number_format($item['profile']['videoCount'], 0, ',', '.')}}</span> Videos,
                                    <span style="font-weight: bold">{{number_format($item['profile']['viewCount'], 0, ',', '.')}}</span> Views
                                    (update: {{$item['profile']['date']}})
                                </h6>
                            </th>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Periode</h4>
                </div>
                <div class="col-sm-4">
                    <label class="label-control" for="start_date">Start Date: </label>
                    <input type="date" id="start_date" name="start_date" class="form-control" value="{{$item['date']['post']['first']}}">
                </div>
                <div class="col-sm-4">
                    <label class="label-control" for="end_date">End Date: </label>
                    <input type="date" id="end_date" name="end_date" class="form-control" value="{{$item['date']['post']['last']}}">
                </div>
                <div class="col-sm-4">
                    <label class="label-control" for="end_date">&nbsp;</label>
                    <button class="btn search-result btn-primary m-1" style="width: 100%" onclick="filterDate()"><i class="ft-filter"></i> Filter</button>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-3">
                    <div class="card gradient-blackberry">
                        <div class="card-content">
                          <div class="card-body pt-2 pb-2">
                                <div class="media">
                                    <div class="media-body white text-left">
                                        <h5 class="font-medium-4 mb-0" id="stat_subscribers"></h5>
                                        <span>Subscribers</span>
                                    </div>
                                    <div class="media-right white text-right">
                                        <i class="ft-user-plus font-medium-4"></i>
                                    </div>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card gradient-ibiza-sunset">
                        <div class="card-content">
                          <div class="card-body pt-2 pb-2">
                                <div class="media">
                                    <div class="media-body white text-left">
                                        <h5 class="font-medium-4 mb-0" id="stat_videos"></h5>
                                        <span>Videos</span>
                                    </div>
                                    <div class="media-right white text-right">
                                        <i class="ft-video font-medium-4"></i>
                                    </div>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card gradient-green-tea">
                        <div class="card-content">
                          <div class="card-body pt-2 pb-2">
                                <div class="media">
                                    <div class="media-body white text-left">
                                        <h5 class="font-medium-4 mb-0" id="stat_views"></h5>
                                        <span>Views</span>
                                    </div>
                                    <div class="media-right white text-right">
                                        <i class="ft-eye font-medium-4"></i>
                                    </div>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card gradient-pomegranate">
                        <div class="card-content">
                          <div class="card-body pt-2 pb-2">
                                <div class="media">
                                    <div class="media-body white text-left">
                                        <h5 class="font-medium-4 mb-0" id="stat_likes"></h5>
                                        <span>Likes</span>
                                    </div>
                                    <div class="media-right white text-right">
                                        <i class="ft-thumbs-up font-medium-4"></i>
                                    </div>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Total Subscribers</h4>
                </div>
                <div class="col-sm-12 mt-2 p-2">
                    <div class="card-content" style="text-align: center;">
                        <canvas id="chart-total-followers" height="150"></canvas>
                    </div>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Growth of Total Subscribers</h4>
                </div>
                <div class="col-sm-9 mt-2 p-2">
                    <canvas id="chart-growth-followers" height="150"></canvas>
                </div>
                <div class="col-sm-3 mt-2 p-2">
                    <h5 style="font-weight: bolder;">Total Subscribers</h5>
                    <h3 style="color: #f70000;" id="growth_followers_total"></h3>
                    <h5 style="font-weight: bolder;" class="mt-3">Total Change in Subscribers</h5>
                    <h3 style="color: #f70000;" id="growth_followers_change"></h3>
                    <h5 style="font-weight: bolder;" class="mt-3">Max Change of Subscribers on</h5>
                    <h3 style="color: #f70000;" id="growth_followers_max"></h3>
                    <span id="growth_followers_max_date"></span>
                    <h5 style="font-weight: bolder;" class="mt-3">Average Change per Day</h5>
                    <h3 style="color: #f70000;" id="growth_followers_average"></h3>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Total Videos</h4>
                </div>
                <div class="col-sm-12 mt-2 p-2">
                    <canvas id="chart-number-total-posts" height="150"></canvas>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Number of New Videos</h4>
                </div>
                <div class="col-sm-9 mt-2 p-2">
                    <canvas id="chart-number-posts" height="150"></canvas>
                </div>
                <div class="col-sm-3 mt-2 p-2">
                    <h5 style="font-weight: bolder;">Sum of New Videos</h5>
                    <h3 style="color: #f70000;" id="number_post_total"></h3>
                    <h5 style="font-weight: bolder;" class="mt-3">Average New Videos per Day</h5>
                    <h3 style="color: #f70000;" id="number_post_average"></h3>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Distribution of Interactions</h4>
                </div>
                <div class="col-sm-4 mt-2 p-2">
                    <canvas id="chart-distribution-interactions" height="200"></canvas>
                </div>
                <div class="col-sm-8 mt-2 p-2">
                    <table class="table" style="border: solid 2px darkgray;">
                        <tr style="background-color: lightgrey;">
                            <th></th>
                            <th>Count</th>
                            <th>Share</th>
                        </tr>
                        <tr>
                            <td><span style="background-color: #f70000;">&nbsp;&nbsp;&nbsp;</span> <i class="ft-thumbs-up mr-2 ml-2"></i> Likes</td>
                            <td id="distribution_interactions_likes_number">0</td>
                            <td id="distribution_interactions_likes_percen">0%</td>
                        </tr>
                        <tr>
                            <td><span style="background-color: #b50000;">&nbsp;&nbsp;&nbsp;</span> <i class="ft-thumbs-up mr-2 ml-2"></i> Dislikes</td>
                            <td id="distribution_interactions_dislikes_number">0</td>
                            <td id="distribution_interactions_dislikes_percen">0%</td>
                        </tr>
                        <tr>
                            <td><span style="background-color: #820000;">&nbsp;&nbsp;&nbsp;</span> <i class="ft-message-square mr-2 ml-2"></i> Comments</td>
                            <td id="distribution_interactions_comments_number">0</td>
                            <td id="distribution_interactions_comments_percen">0%</td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Evolution of Interactions</h4>
                </div>
                <div class="col-sm-9 mt-2 p-2">
                    <canvas id="chart-evolution-interactions" height="200"></canvas>
                </div>
                <div class="col-sm-3 mt-2 p-2">
                    <h5 style="font-weight: bolder;" class="mt-3">Max Interactions on</h5>
                    <h3 style="color: #f70000;" id="evolution_interaction_max"></h3>
                    <span id="evolution_interaction_max_date"></span>
                    <h5 style="font-weight: bolder;" class="mt-3">Average Interactions per Day</h5>
                    <h3 style="color: #f70000;" id="evolution_interaction_average"></h3>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Total Views</h4>
                </div>
                <div class="col-sm-12 mt-2 p-2">
                    <canvas id="chart-number-total-views" height="150"></canvas>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Number of Views</h4>
                </div>
                <div class="col-sm-9 mt-2 p-2">
                    <canvas id="chart-number-views" height="150"></canvas>
                </div>
                <div class="col-sm-3 mt-2 p-2">
                    <h5 style="font-weight: bolder;">Sum of New Views</h5>
                    <h3 style="color: #f70000;" id="number_view_total"></h3>
                    <h5 style="font-weight: bolder;" class="mt-3">Average Views per Day</h5>
                    <h3 style="color: #f70000;" id="number_view_average"></h3>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-sm-12">
                    <h4 style="font-weight: bold">Most Engaging Videos Overview</h4>
                </div>
                <div class="col-sm-12 mt-2 p-2">
                    <div class="row m-0 p-0" id="limitpost">
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('pagecss')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.3/css/bootstrap-select.min.css">
@endsection
@section('pagejs')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script>

    var is_show = true;
    var element_total_followers = document.getElementById('chart-total-followers').getContext('2d');
    var element_growth_followers = document.getElementById('chart-growth-followers').getContext('2d');
    var element_number_total_posts = document.getElementById('chart-number-total-posts').getContext('2d');
    var element_number_posts = document.getElementById('chart-number-posts').getContext('2d');
    var element_distribution_interactions = document.getElementById('chart-distribution-interactions').getContext('2d');
    var element_evolution_interactions = document.getElementById('chart-evolution-interactions').getContext('2d');
    var element_number_total_views = document.getElementById('chart-number-total-views').getContext('2d');
    var element_number_views = document.getElementById('chart-number-views').getContext('2d');

    var ctx_total_followers = new Chart(element_total_followers, {});
    var ctx_growth_followers = new Chart(element_growth_followers, {});
    var ctx_number_total_posts = new Chart(element_number_total_posts, {});
    var ctx_number_posts = new Chart(element_number_posts, {});
    var ctx_distribution_interactions = new Chart(element_distribution_interactions, {});
    var ctx_evolution_interactions = new Chart(element_evolution_interactions, {});
    var ctx_number_total_views = new Chart(element_number_total_views, {});
    var ctx_number_views = new Chart(element_number_views, {});

    $(document).ready(function() {
        get_stat();
        get_total_followers();
        get_growth_followers();
        get_number_posts();
        get_distribution_interactions();
        get_evolution_interactions();
        get_number_views();
        get_limit_post();
        textShow();
    });

    function textShow() {
        is_show = !is_show;
        if(is_show) {
            document.getElementById("text_show").innerHTML = `<?php echo $item['profile']['description']; ?>`;
            document.getElementById("text_show_link").innerHTML = "&nbsp;&nbsp;<< Less";
        } else {
            document.getElementById("text_show").innerHTML = `<?php echo substr($item['profile']['description'], 0, 100); ?>...`;
            document.getElementById("text_show_link").innerHTML = "&nbsp;&nbsp;More >>";
        }
    }

    function filterDate() {
        var start_date = document.getElementById("start_date").value;
        var end_date = document.getElementById("end_date").value;
        ctx_total_followers.destroy();
        ctx_growth_followers.destroy();
        ctx_number_total_posts.destroy();
        ctx_number_posts.destroy();
        ctx_distribution_interactions.destroy();
        ctx_evolution_interactions.destroy();
        ctx_number_total_views.destroy();
        ctx_number_views.destroy();
        get_stat(start_date, end_date);
        get_total_followers(start_date, end_date);
        get_growth_followers(start_date, end_date);
        get_number_posts(start_date, end_date);
        get_distribution_interactions(start_date, end_date);
        get_evolution_interactions(start_date, end_date);
        get_number_views(start_date, end_date);
        get_limit_post(start_date, end_date);
    }

    function get_stat(start_date="{{ $item['date']['profile']['first'] }}", end_date="{{ $item['date']['profile']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/youtube/get_stat/'.$item['profile']['id'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                if (result['subscriber_growth'] > 0) {
                    document.getElementById("stat_subscribers").innerHTML = result['subscriber_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(+" + result['subscriber_growth'].toLocaleString('id') + ")" +"</span>";
                } else {
                    document.getElementById("stat_subscribers").innerHTML = result['subscriber_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(" + result['subscriber_growth'].toLocaleString('id') + ")" +"</span>";
                }
                if (result['video_growth'] > 0) {
                    document.getElementById("stat_videos").innerHTML = result['video_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(+" + result['video_growth'].toLocaleString('id') + ")" +"</span>";
                } else {
                    document.getElementById("stat_videos").innerHTML = result['video_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(" + result['video_growth'].toLocaleString('id') + ")" +"</span>";
                }
                if (result['view_growth'] > 0) {
                    document.getElementById("stat_views").innerHTML = result['view_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(+" + result['view_growth'].toLocaleString('id') + ")" +"</span>";
                } else {
                    document.getElementById("stat_views").innerHTML = result['view_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(" + result['view_growth'].toLocaleString('id') + ")" +"</span>";
                }
                if (result['like_growth'] > 0) {
                    document.getElementById("stat_likes").innerHTML = result['like_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(+" + result['like_growth'].toLocaleString('id') + ")" +"</span>";
                } else {
                    document.getElementById("stat_likes").innerHTML = result['like_total'].toLocaleString('id') + "<span style=\"font-size: small;\">"+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(" + result['like_growth'].toLocaleString('id') + ")" +"</span>";
                }
            }
        });
    }

    function get_total_followers(start_date="{{ $item['date']['profile']['first'] }}", end_date="{{ $item['date']['profile']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/youtube/get_total_followers/'.$item['profile']['id'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_total_followers = [];
                var data_total_followers = [];
                result.forEach(element => {
                    label_total_followers.push(element.date)
                    data_total_followers.push(element.followed_by)
                });

                ctx_total_followers = new Chart(element_total_followers, {
                    type: 'line',
                    data: {
                        labels: label_total_followers,
                        datasets: [
                            {
                                label: 'Subscribers',
                                backgroundColor: '#b50000',
                                borderColor: '#f70000',
                                fill: false,
                                data: data_total_followers,
                            },
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: false
                        },
                        bezierCurve : false,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: false
                                }
                            }]
                        },
                        elements: {
                            line: {
                                tension: 0
                            }
                        }
                    }
                });
            }
        });
    }

    function get_growth_followers(start_date="{{ $item['date']['profile']['first'] }}", end_date="{{ $item['date']['profile']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/youtube/get_growth_followers/'.$item['profile']['id'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_growth_followers = [];
                var data_growth_followers = [];
                result['data'].forEach(element => {
                    label_growth_followers.push(element.date)
                    data_growth_followers.push(element.followed_by)
                });

                document.getElementById("growth_followers_total").innerHTML = result['total'].toLocaleString('id');
                document.getElementById("growth_followers_change").innerHTML = result['change'].toLocaleString('id');
                document.getElementById("growth_followers_max").innerHTML = result['max'].toLocaleString('id');
                document.getElementById("growth_followers_max_date").innerHTML = result['max_date'];
                document.getElementById("growth_followers_average").innerHTML = result['average'].toLocaleString('id');

                ctx_growth_followers = new Chart(element_growth_followers, {
                    type: 'bar',
                    data: {
                        labels: label_growth_followers,
                        datasets: [
                            {
                                label: 'Subscribers',
                                backgroundColor: '#f70000',
                                borderColor: '#b50000',
                                fill: false,
                                data: data_growth_followers,
                            },
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: false
                        },
                        bezierCurve : false,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: false
                                }
                            }]
                        },
                        elements: {
                            line: {
                                tension: 0
                            }
                        }
                    }
                });
            }
        });
    }

    function get_number_posts(start_date="{{ $item['date']['post']['first'] }}", end_date="{{ $item['date']['post']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/youtube/get_number_posts/'.$item['profile']['id'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_number_posts = [];
                var data_number_posts = [];
                var data_number_posts_total = [];
                result['data'].forEach(element => {
                    label_number_posts.push(element.date)
                    data_number_posts.push(element.number_post)
                    data_number_posts_total.push(element.number_post_total)
                });

                document.getElementById("number_post_total").innerHTML = result['total'].toLocaleString('id');
                document.getElementById("number_post_average").innerHTML = result['average'].toLocaleString('id');

                ctx_number_posts = new Chart(element_number_posts, {
                    type: 'bar',
                    data: {
                        labels: label_number_posts,
                        datasets: [
                            {
                                label: 'Videos',
                                backgroundColor: '#f70000',
                                data: data_number_posts,
                            },
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: false
                        },
                        bezierCurve : false
                    }
                });

                ctx_number_total_posts = new Chart(element_number_total_posts, {
                    type: 'line',
                    data: {
                        labels: label_number_posts,
                        datasets: [
                            {
                                label: 'Videos',
                                backgroundColor: '#f70000',
                                borderColor: '#b50000',
                                fill: false,
                                data: data_number_posts_total,
                            },
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: false
                        },
                        bezierCurve : false,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: false
                                }
                            }]
                        },
                        elements: {
                            line: {
                                tension: 0
                            }
                        }
                    }
                });
            }
        });
    }

    function get_distribution_interactions(start_date="{{ $item['date']['post']['first'] }}", end_date="{{ $item['date']['post']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/youtube/get_distribution_interactions/'.$item['profile']['id'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_distribution_interactions = [];
                label_distribution_interactions.push('Likes');
                label_distribution_interactions.push('Dislikes');
                label_distribution_interactions.push('Comments');

                var data_distribution_interactions = [];
                data_distribution_interactions.push(result['like'])
                data_distribution_interactions.push(result['dislike'])
                data_distribution_interactions.push(result['comment'])
                var interaction_total = result['like'] + result['dislike'] + result['comment'];

                document.getElementById("distribution_interactions_likes_number").innerHTML = result['like'].toLocaleString('id');
                document.getElementById("distribution_interactions_likes_percen").innerHTML = ((result['like'] / interaction_total) * 100).toLocaleString('id') + "%";
                document.getElementById("distribution_interactions_dislikes_number").innerHTML = result['dislike'].toLocaleString('id');
                document.getElementById("distribution_interactions_dislikes_percen").innerHTML = ((result['dislike'] / interaction_total) * 100).toLocaleString('id') + "%";
                document.getElementById("distribution_interactions_comments_number").innerHTML = result['comment'].toLocaleString('id');
                document.getElementById("distribution_interactions_comments_percen").innerHTML = ((result['comment'] / interaction_total) * 100).toLocaleString('id') + "%";

                var ctx = document.getElementById('chart-distribution-interactions').getContext('2d');
                var chart = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: label_distribution_interactions,
                        datasets: [{
                            label: 'Distribution Interactions',
                            backgroundColor: [
                                '#f70000',
                                '#b50000',
                                '#820000'
                            ],
                            data: data_distribution_interactions,
                            name: label_distribution_interactions,
                        }],
                    },
                    options: {
                        responsive: true,
                        events: false,
                        legend: {
                            display: false,
                            position: 'right'
                        },
                        animation: {
                            // duration: 500,
                            // easing: "easeOutQuart",
                            onComplete: function () {
                            var ctx = this.chart.ctx;
                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function (dataset) {

                                for (var i = 0; i < dataset.data.length; i++) {
                                var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                    total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                    mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
                                    start_angle = model.startAngle,
                                    end_angle = model.endAngle,
                                    mid_angle = start_angle + (end_angle - start_angle)/2;

                                var x = mid_radius * Math.cos(mid_angle);
                                var y = mid_radius * Math.sin(mid_angle);

                                ctx.fillStyle = '#fff';
                                if (i == 3){
                                    ctx.fillStyle = '#444';
                                }
                                var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
                                // if(dataset.data[i] != 0 && dataset._meta[0].data[i].hidden != true) {
                                    ctx.fillText(dataset.name[i] + ' (' + dataset.data[i] + ')', model.x + x, model.y + y);
                                    ctx.fillText(percent, model.x + x, model.y + y + 15);
                                // }
                                }
                            });
                            }
                        }
                    }
                });
            }
        });
    }

    function get_evolution_interactions(start_date="{{ $item['date']['post']['first'] }}", end_date="{{ $item['date']['post']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/youtube/get_evolution_interactions/'.$item['profile']['id'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_evolution_interactions = [];
                var data_evolution_interactions = [];
                data_evolution_interactions.push([]);
                data_evolution_interactions.push([]);
                data_evolution_interactions.push([]);

                result['data'].forEach(element => {
                    label_evolution_interactions.push(element.date)
                    data_evolution_interactions[0].push(element.like)
                    data_evolution_interactions[1].push(element.comment)
                    data_evolution_interactions[2].push(element.comment)
                });

                document.getElementById("evolution_interaction_max").innerHTML = result['max'].toLocaleString('id');
                document.getElementById("evolution_interaction_max_date").innerHTML = result['max_date'];
                document.getElementById("evolution_interaction_average").innerHTML = result['average'].toLocaleString('id');

                ctx_evolution_interactions = new Chart(element_evolution_interactions, {
                    type: 'bar',
                    data: {
                        labels: label_evolution_interactions,
                        datasets: [
                            {
                                label: 'Like',
                                backgroundColor: '#f70000',
                                data: data_evolution_interactions[0],
                            },
                            {
                                label: 'Dislike',
                                backgroundColor: '#b50000',
                                data: data_evolution_interactions[1],
                            },
                            {
                                label: 'Comment',
                                backgroundColor: '#820000',
                                data: data_evolution_interactions[2],
                            },
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: true,
                            position: 'bottom'
                        },
                        scales: {
                            xAxes: [{
                                stacked: true,
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        },
                        bezierCurve : false
                    }
                });
            }
        });
    }

    function get_number_views(start_date="{{ $item['date']['post']['first'] }}", end_date="{{ $item['date']['post']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/youtube/get_number_views/'.$item['profile']['id'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){
                var label_number_views = [];
                var data_number_views = [];
                var data_number_views_total = [];
                result['data'].forEach(element => {
                    label_number_views.push(element.date)
                    data_number_views.push(element.number_view)
                    data_number_views_total.push(element.number_view_total)
                });

                document.getElementById("number_view_total").innerHTML = result['total'].toLocaleString('id');
                document.getElementById("number_view_average").innerHTML = result['average'].toLocaleString('id');

                ctx_number_views = new Chart(element_number_views, {
                    type: 'bar',
                    data: {
                        labels: label_number_views,
                        datasets: [
                            {
                                label: 'Views',
                                backgroundColor: '#f70000',
                                data: data_number_views,
                            },
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: false
                        },
                        bezierCurve : false
                    }
                });

                ctx_number_total_views = new Chart(element_number_total_views, {
                    type: 'line',
                    data: {
                        labels: label_number_views,
                        datasets: [
                            {
                                label: 'Views',
                                backgroundColor: '#f70000',
                                borderColor: '#b50000',
                                fill: false,
                                data: data_number_views_total,
                            },
                        ],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: false
                        },
                        bezierCurve : false,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: false
                                }
                            }]
                        },
                        elements: {
                            line: {
                                tension: 0
                            }
                        }
                    }
                });
            }
        });
    }

    function get_limit_post(start_date="{{ $item['date']['post']['first'] }}", end_date="{{ $item['date']['post']['last'] }}") {
        $.ajax({
            url: "{{ url('/admin/youtube/get_limit_post/'.$item['profile']['id'])}}" + ";" + start_date + ";" + end_date,
            data: {},
            success: function(result){

                var html = ``;
                document.getElementById("limitpost").innerHTML = html;
                result.forEach(element => {
                    html += `
                    <div class="col-sm-5 ml-4 mb-2" style="border: solid 2px lightgrey; border-radius: 15px; height: fit-content;">
                        <div class="row" style="margin-top: 5px; margin-bottom: 5px; border-bottom: solid 2px lightgrey;">
                            <div class="col-2">
                                <img src="{{ $item['profile']['thumbnails_medium'] }}" style="margin-bottom: 5px; width: 100%;"/>
                            </div>
                            <div class="col-10">
                                <h6 style="font-weight: bold">
                                    {{$item['profile']['title']}}
                                </h6>
                                <h6>
                                    <span class="badge" style="background-color: #f70000; color: #FFFFFF; padding: 3px;">youtube</span>
                                    / {{$item['profile']['title']}}
                                    <i class="ml-4">` + element['datetimes'] + `</i>
                                </h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 p-2" style="display: grid;">
                                <iframe style="width: 100%; height: 300px;" src="https://www.youtube.com/embed/` + element['resourceId_videoId'] + `"> </iframe>
                                <span class="ml-2">
                                </span>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 5px; margin-bottom: 5px; border-top: solid 2px lightgrey;">
                            <div class="col-sm-3 p-1" style="border-right: solid 1px lightgrey;">
                                <b>` + (element['likeCount'] + element['dislikeCount'] + element['commentCount']).toLocaleString('id') + `</b>
                                <br>Interactions
                            </div>
                            <div class="col-sm-2 p-1" style="border-right: solid 1px lightgrey;">
                                <b>` + element['likeCount'].toLocaleString('id') + `</b>
                                <br>Likes
                            </div>
                            <div class="col-sm-2 p-1" style="border-right: solid 1px lightgrey;">
                                <b>` + element['dislikeCount'].toLocaleString('id') + `</b>
                                <br>Comments
                            </div>
                            <div class="col-sm-2 p-1" style="border-right: solid 1px lightgrey;">
                                <b>` + element['commentCount'].toLocaleString('id') + `</b>
                                <br>Comments
                            </div>
                            <div class="col-sm-3 p-1">
                                <b>` + (((element['likeCount'] + element['likeCount'] + element['commentCount']) / + `<?php echo $item['profile']['subscriberCount']; ?> `) * 1000).toLocaleString('id') + `</b>
                                <br>Interactions per 1k Followers
                            </div>
                        </div>
                    </div>
                    `;
                });

                document.getElementById("limitpost").innerHTML = html;
            }
        });
    }

</script>
@endsection
