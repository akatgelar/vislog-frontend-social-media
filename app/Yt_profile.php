<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Yt_profile extends Eloquent
{
    protected $guarded = ['id'];
    protected $table = 'yt_profile';

    protected $casts = [
        'date' => 'datetime',
    ];
}
