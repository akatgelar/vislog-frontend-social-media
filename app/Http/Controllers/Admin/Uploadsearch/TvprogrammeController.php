<?php

namespace App\Http\Controllers\Admin\Uploadsearch;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Tvprogrammesearch;
use \Carbon\Carbon;
use Rap2hpoutre\FastExcel\FastExcel;
use App\Imports\TvprogrammesImport;

use DB;

class TvprogrammeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.uploadsearch.tvprogramme');
    }

    public function indexjson(Request $request)
    {
        $draw = $request->draw;
        $start = (isset($request->start) ? (int)$request->start : 0);
        $length = (isset($request->length) ? (int)$request->length : 10);

        $search = (isset($request->search['value']))? $request->search['value'] : false;

        $orderColumn = (isset($request->order[0]['column']))? $request->order[0]['column'] : 0;
        $order = (isset($request->order[0]['dir']))? $request->order[0]['dir'] : 'asc';

        $mappingColumn = [
            'id', 'nlevel_1', 'nlevel_2', 'nprogramme', 'ilevel_1', 'ilevel_2', 'iprogramme'
        ];

        #start
        $mainQuery = [];
        $allQuery = [];

        #search
        if($search){
            array_push($mainQuery, [
                '$match' => [
                    '$or' => [
                        [
                            'id' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ],
                        [
                            'nlevel_1' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ],
                        [
                            'nlevel_2' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ],
                        [
                            'nprogramme' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ],
                        [
                            'ilevel_1' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ],
                        [
                            'ilevel_2' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ],
                        [
                            'iprogramme' => [
                                '$regex' => $search,
                                '$options' => 'i'
                            ]
                        ]
                    ]
                ]
            ]);
        }
        $allQuery = $mainQuery; #all data

        #orderby
        if($order == 'asc'){
            $order = 1;
        }else{
            $order = -1;
        }
        array_push($mainQuery, [
            '$sort' => [
                $mappingColumn[$orderColumn] => $order
            ]
        ]);

        #limit offset
        if(isset($start)) array_push($mainQuery, ['$skip' => $start]);
        if(isset($length)) array_push($mainQuery, ['$limit' => $length]);

        #query data
        // DB::connection('mongodb')->enableQueryLog();
        $query = Tvprogrammesearch::raw(function($collection) use($mainQuery)
        {
            // dd($mainQuery);
            return $collection->aggregate(array_merge(
                [], $mainQuery), ['allowDiskUse' => true, 'maxTimeMS' => 600000] #max 5 minutes
            );
        });
        // dd(DB::connection('mongodb')->getQueryLog());
        // dd($query);
        $response['draw'] = $draw;
        $response['data'] = $query;
        $response['recordsTotal'] = $response['data']->count();

        #query all data
        array_push($allQuery, ['$count' => 'count']);
        $queryAll = Tvprogrammesearch::raw(function($collection) use($allQuery)
        {
            // dd($allQuery);
            return $collection->aggregate(array_merge(
                [], $allQuery), ['allowDiskUse' => true, 'maxTimeMS' => 600000] #max 5 minutes
            );
        });
        // dd($queryAll);
        $response['recordsFiltered'] = (isset($queryAll[0]['count']) ? $queryAll[0]['count'] : 0);

        return response()->json($response);
    }

    public function upload(Request $request)
    {             
        $upload_path = 'uploads/temp';
        if ($request->file('file')->isValid()) {
            $file = $request->file('file');
            // File Details 
            $filename = $file->getClientOriginalName();
            $upload_filename = Carbon::now()->format('Ymd').$filename;
            $extension = $file->getClientOriginalExtension();
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize();
            $mimeType = $file->getMimeType();

            $data['filename'] = $filename;
            $data['mimeType'] = $mimeType;
            $data['fileSize'] = $fileSize;

            // store file to temp folder
            $file->move($upload_path,$upload_filename);

            // import to database
            $imp = (new FastExcel)->configureCsv(';', '}', '\n', 'gbk')->import($upload_path.'/'.$upload_filename, function ($line) {
                $insertData = [];
                foreach($line as $key=>$val){
                    $colname = strtolower($key);
                    $colname = str_replace(' ','_',$colname);
                    $colname = str_replace('.','',$colname);
                    $insertData[$colname] = str_replace('�',' ',$val);
                }
                return Tvprogrammesearch::create($insertData);
            });

            $data['rowCount'] = $imp->count();
            
            unlink($upload_path.'/'.$upload_filename);

            $return = [
                'message' => 'Success',
                'data' => $data
            ];
            return response($return,200);
        }else{
            return response(['message'=>'Upload failed'],400);
        }
    }

    public function csvall()
    {
        $export = Tvprogrammesearch::all();
        $filename = 'vislog-tvprogramme-search-uploaded.csv';
        $temp = 'uploads/temp/'.$filename;
        (new FastExcel($export))->export($temp);
        $headers = [
            'Content-Type: text/csv',
            ];
        return response()->download($temp, $filename, $headers)->deleteFileAfterSend(true);
    }
}
