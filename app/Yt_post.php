<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Yt_post extends Eloquent
{
    protected $guarded = ['id'];
    protected $table = 'yt_post';

    protected $casts = [
        'publishedAt' => 'datetime',
        'viewCount' => 'integer',
        'likeCount' => 'integer',
        'dislikeCount' => 'integer',
        'commentCount' => 'integer',
    ];
}
